﻿#region

using System;
using System.Windows.Forms;
using TP2_Pékéman.Vue;

#endregion

namespace TP2_Pékéman
{
    internal static class Program
    {
        /// <summary>
        ///     Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmJeu());
            //Application.Run(new FrmRegenererVies());
        }
    }
}