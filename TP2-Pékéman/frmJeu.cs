﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using TP2_Pékéman.Logique;

namespace TP2_Pékéman
{
    public partial class FrmJeu : Form
    {
        // variable qui sert a demarrer le jeu
        private const String DEFAULT_NAME = "Gold";
        public static Boolean demarrer = false;
        public static Boolean surMenu = false;
        public static Boolean surEcranNomJoueur = false;
        private static SoundPlayer _player;
        public static Boolean estPret = false;
        private Joueur _joueur;
        /// <summary>
        /// Constructeur de la classe frmJeu qui a comme paramettre le _joueur
        /// </summary>
        //public frmJeu(Joueur _joueur)
        public FrmJeu()
        {
            _joueur = new Joueur(DEFAULT_NAME);
            InitializeComponent();
           // map.Joueur = _joueur;
            map.listePekeman = creerListePekeman();
            Thread threadLogo = new Thread(AfficherLogo);
            threadLogo.Start();
            tmrMenu.Start();
            tmrNomJoueur.Start();
            tmrDemarrer.Start();
            btnDown.Visible = false;
            btnUp.Visible = false;
            btnLeft.Visible = false;
            btnRight.Visible = false;
            btnPekedex.Visible = false;
            lblNomJoueur.Visible = false;
           // this._joueur = _joueur;
        }
        /// <summary>
        /// methode qui affiche l'image du debut du jeu
        /// </summary>
        private void AfficherLogo()
        {
            Thread.Sleep(2000);
            this.BeginInvoke(
            (MethodInvoker)delegate()
            {
                ctlAccueil.Visible = true;
                JouerMusique();
                logoNentindo1.Dispose();
            });
        }
        /// <summary>
        /// methode qui initialise le map et change l'etat des boutons
        /// </summary>
        public void DemarrerJeu()
        {
            //_player.Stop();
            btnPekedex.Visible = true;
            map.ConstruireMap();
            map.Joueur = _joueur;
            ctlAccueil.Visible = false;
            ctlMenu.Visible = false;
            map.Visible = true;
            lblNomJoueur.Visible = true;
            lblNomText.Text = _joueur.nomJoueur;
            estPret = true;
        }
       
        /// <summary>
        /// Evenement qui verifie si le bouton fermer est pressé
        /// </summary>
        private void frmJeu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                if (!QuitterApplication())
                {
                    e.Cancel = true;
                }
            }       
        }
        /// <summary>
        /// methode qui cree la liste de Pekeman
        /// </summary>
        private List<Pekeman> creerListePekeman()
        {
            List<Pekeman> listPekeman = new List<Pekeman>();
            listPekeman.Add(new Pekeman("CHARMANDER",39,TypePekeman.FEU,52,43,45,Properties.Resources.charm, 65));
            listPekeman.Add(new Pekeman("SQUIRTLE", 44, TypePekeman.EAU, 48, 65, 45, Properties.Resources.squir, 43));
            listPekeman.Add(new Pekeman("CHIKORITA", 45, TypePekeman.PLANTE, 49, 65, 45, Properties.Resources.chik, 45));
            listPekeman.Add(new Pekeman("BULBASAUR", 45, TypePekeman.PLANTE, 49, 49, 45, Properties.Resources.bulb, 45));
            listPekeman.Add(new Pekeman("PIDGEY", 40, TypePekeman.NORMALE, 45, 40, 255, Properties.Resources.pid, 56));
            return listPekeman;
        }
        /// <summary>
        /// methode qui demande si on veut quitter le jeu
        /// </summary>
        public static Boolean QuitterApplication()
        {
            DialogResult resultat = MessageBox.Show("Est-ce que vous voulez quitter?", "Quitter", MessageBoxButtons.YesNo);
            if (resultat == DialogResult.Yes)
            {
                Application.Exit();
                return true;
            }
            return false;
        }
        /// <summary>
        /// Evenement lorsque on presse les boutons du clavier pour deplacer le _joueur
        /// </summary>
        private void frmJeu_KeyPress(object sender, KeyPressEventArgs e)
        {           
            if (surMenu)
            {
                DeplaceCurseurMenu(e.KeyChar);   
            }
            else if (surEcranNomJoueur)
            {
                DeplaceCurseurNomJoueur(e.KeyChar);
            }
            if (demarrer && estPret)
            {
                map.DeplacerPerso(e.KeyChar);
            }
        }
        /// <summary>
        /// Evenement du bouton UP (flèche)
        /// </summary>
        private void btnUp_Click(object sender, EventArgs e)
        {
            map.DeplacerPerso('w');
        }
        /// <summary>
        /// Evenement du bouton left (flèche)
        /// </summary>
        private void btnLeft_Click(object sender, EventArgs e)
        {
            map.DeplacerPerso('a');
        }
        /// <summary>
        /// Evenement du bouton right (flèche)
        /// </summary>
        private void btnRight_Click(object sender, EventArgs e)
        {
            map.DeplacerPerso('d');
        }
        /// <summary>
        /// Evenement du bouton down (flèche)
        /// </summary>
        private void btnDown_Click(object sender, EventArgs e)
        {
            map.DeplacerPerso('s');
        }
        /// <summary>
        /// Evenement du bouton pekedex (ouvre la fenetre pekedex)
        /// </summary>
        private void btnPekedex_Click(object sender, EventArgs e)
        {
            FrmPékédex fp = new FrmPékédex(_joueur);
            fp.ShowDialog();
        }
        
        /// <summary>
        /// Methode qui deplace le curseur selon le choix de l'utilisateur et detecte quel option il choisit s'il clique sur la touche espace.
        /// </summary>
        /// <param name="key"></param>
        private void DeplaceCurseurMenu(char key)
        {
            switch (key)
            {
                case 'w':
                    if((this.ctlMenu.ctlCursor.Location.Y-26) >= this.ctlMenu.ctlNewGame.Location.Y)
                        this.ctlMenu.ctlCursor.Top -= 26;
                    break;
                case 's':
                    if ((this.ctlMenu.ctlCursor.Location.Y + 26) <= this.ctlMenu.ctlQuitGame.Location.Y)
                    this.ctlMenu.ctlCursor.Top += 26;
                    break;
                case (char)Keys.Space:
                    if (this.ctlMenu.ctlCursor.Location.Y == this.ctlMenu.ctlNewGame.Location.Y)
                    {
                        ctlMenu.Dispose();
                        surMenu = false;
                        surEcranNomJoueur = true;
                    }
                    else if (this.ctlMenu.ctlCursor.Location.Y == this.ctlMenu.ctlLoadGame.Location.Y)
                    {
                        //NYI
                    }
                    else if (this.ctlMenu.ctlCursor.Location.Y == this.ctlMenu.ctlQuitGame.Location.Y)
                    {
                        QuitterApplication();
                    }
                    break;
            }
            this.Refresh();
        }
        /// <summary>
        /// Methode qui deplace le curseur de l'ecran de choix de nom du joueur.
        /// </summary>
        /// <param name="key"></param>
        public void DeplaceCurseurNomJoueur(char key)
        {
            switch (key)
            {
                case 'w':
                    if ((this.ctlNomJoueur.ctlCursor.Location.Y - 25) >= this.ctlNomJoueur.txtName.Location.Y+5)
                        this.ctlNomJoueur.ctlCursor.Top -= 25;
                    break;
                case 's':
                    if ((this.ctlNomJoueur.ctlCursor.Location.Y + 25) <= this.ctlNomJoueur.ctlEndButton.Location.Y)
                        this.ctlNomJoueur.ctlCursor.Top += 25;
                    break;
                case (char)Keys.Space:
                    if (ctlNomJoueur.ctlCursor.Location.Y == this.ctlNomJoueur.ctlEndButton.Location.Y)
                    {                        
                        //this.ctlNomJoueur.Dispose();
                        surEcranNomJoueur = false;
                        demarrer = true;
                    }
                    break;
            }
            this.Refresh();
        }
        public static void JouerMusique()
        {
           
            Stream str;
            if (surMenu)
            {
                _player.Stop();
                str = Properties.Resources.menu_screen;
            }
            else
            {
                str = Properties.Resources.title_screen;
            }
            _player = new SoundPlayer(str);
            _player.PlayLooping();
        }
       
        private void tmrMenu_Tick(object sender, EventArgs e)
        {
            if (surMenu)
            {
                btnDown.Visible = true;
                btnUp.Visible = true;
                btnLeft.Visible = true;
                btnRight.Visible = true;
                btnPekedex.Visible = false;
                tmrMenu.Stop();
                ctlAccueil.Dispose();
                ctlMenu.Visible = true;
                tmrNomJoueur.Start();
            }
        }
        private void tmrNomJoueur_Tick(object sender, EventArgs e)
        {
            if (surEcranNomJoueur)
            {
                tmrMenu.Stop();
                ctlMenu.Dispose();
                ctlNomJoueur.Visible = true;
                tmrDemarrer.Start();
            }
        }
        /// <summary>
        /// timer qui attend que le bouton demarrer soit pressé
        /// </summary>
        private void tmrDemarrer_Tick(object sender, EventArgs e)
        {
            if (demarrer)
            {
                if (ctlNomJoueur.txtName.Text != "")
                    _joueur = new Joueur(this.ctlNomJoueur.txtName.Text);
                else
                    _joueur = new Joueur(DEFAULT_NAME);//met un nom par defaut si le joueur ne rentre rien.
                map.Joueur = _joueur;
                ctlNomJoueur.Dispose();
                tmrNomJoueur.Stop();
                _player.Stop();
                tmrDemarrer.Stop();
                DemarrerJeu();
                
            }
        }
    }
}
