﻿namespace TP2_Pékéman.Vue
{
    partial class CtlAccueil
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pctBoxAccueil = new System.Windows.Forms.PictureBox();
            this.btnNouvellePartie = new System.Windows.Forms.Button();
            this.btnChargerPartie = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.pnlMenu = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pctBoxAccueil)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // pctBoxAccueil
            // 
            this.pctBoxAccueil.BackColor = System.Drawing.Color.Transparent;
            this.pctBoxAccueil.Image = global::TP2_Pékéman.Properties.Resources.hooh_backgroud_flying;
            this.pctBoxAccueil.Location = new System.Drawing.Point(0, 0);
            this.pctBoxAccueil.Name = "pctBoxAccueil";
            this.pctBoxAccueil.Size = new System.Drawing.Size(700, 571);
            this.pctBoxAccueil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctBoxAccueil.TabIndex = 3;
            this.pctBoxAccueil.TabStop = false;
            // 
            // btnNouvellePartie
            // 
            this.btnNouvellePartie.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNouvellePartie.Location = new System.Drawing.Point(13, 15);
            this.btnNouvellePartie.Name = "btnNouvellePartie";
            this.btnNouvellePartie.Size = new System.Drawing.Size(223, 39);
            this.btnNouvellePartie.TabIndex = 0;
            this.btnNouvellePartie.Text = "NOUVELLE PARTIE";
            this.btnNouvellePartie.UseVisualStyleBackColor = true;
            this.btnNouvellePartie.Click += new System.EventHandler(this.btnNouvellePartie_Click);
            // 
            // btnChargerPartie
            // 
            this.btnChargerPartie.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChargerPartie.Location = new System.Drawing.Point(13, 60);
            this.btnChargerPartie.Name = "btnChargerPartie";
            this.btnChargerPartie.Size = new System.Drawing.Size(223, 39);
            this.btnChargerPartie.TabIndex = 1;
            this.btnChargerPartie.Text = "CHARGER PARTIE";
            this.btnChargerPartie.UseVisualStyleBackColor = true;
            this.btnChargerPartie.Click += new System.EventHandler(this.btnChargerPartie_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.Location = new System.Drawing.Point(13, 105);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(223, 39);
            this.btnQuitter.TabIndex = 2;
            this.btnQuitter.Text = "QUITTER";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.Transparent;
            this.pnlMenu.BackgroundImage = global::TP2_Pékéman.Properties.Resources.loadScreen;
            this.pnlMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMenu.Controls.Add(this.btnNouvellePartie);
            this.pnlMenu.Controls.Add(this.btnQuitter);
            this.pnlMenu.Controls.Add(this.btnChargerPartie);
            this.pnlMenu.Location = new System.Drawing.Point(226, 369);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(249, 158);
            this.pnlMenu.TabIndex = 10;
            // 
            // CtlAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.pctBoxAccueil);
            this.DoubleBuffered = true;
            this.Name = "CtlAccueil";
            this.Size = new System.Drawing.Size(700, 571);
            ((System.ComponentModel.ISupportInitialize)(this.pctBoxAccueil)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pctBoxAccueil;
        private System.Windows.Forms.Button btnNouvellePartie;
        private System.Windows.Forms.Button btnChargerPartie;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Panel pnlMenu;
    }
}
