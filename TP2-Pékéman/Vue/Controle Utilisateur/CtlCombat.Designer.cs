﻿namespace TP2_Pékéman.Vue
{
    partial class CtlCombat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CtlCombat));
            this._ctnBarreVieEnnemi = new CtnBarreVie(10,10);
            this._ctnBarreViePekeman = new CtnBarreVie(10, 10);
            this.tmrSlideOutEnnemi = new System.Windows.Forms.Timer(this.components);
            this.pnlChoixEquipe = new System.Windows.Forms.Panel();
            this.btnPnlEquipeCancel = new System.Windows.Forms.Button();
            this.btnOkPnlEquipe = new System.Windows.Forms.Button();
            this.picPekeman6 = new System.Windows.Forms.PictureBox();
            this.optPokemon6 = new System.Windows.Forms.RadioButton();
            this.lblHPPokemon6 = new System.Windows.Forms.Label();
            this.lblViePokemon6 = new System.Windows.Forms.Label();
            this.picPekeman4 = new System.Windows.Forms.PictureBox();
            this.optPokemon4 = new System.Windows.Forms.RadioButton();
            this.lblHPPokemon4 = new System.Windows.Forms.Label();
            this.lblViePokemon4 = new System.Windows.Forms.Label();
            this.picPekeman2 = new System.Windows.Forms.PictureBox();
            this.optPokemon2 = new System.Windows.Forms.RadioButton();
            this.lblHPPokemon2 = new System.Windows.Forms.Label();
            this.lblViePokemon2 = new System.Windows.Forms.Label();
            this.picPekeman5 = new System.Windows.Forms.PictureBox();
            this.optPokemon5 = new System.Windows.Forms.RadioButton();
            this.lblHPPokemon5 = new System.Windows.Forms.Label();
            this.lblViePokemon5 = new System.Windows.Forms.Label();
            this.picPekeman3 = new System.Windows.Forms.PictureBox();
            this.optPokemon3 = new System.Windows.Forms.RadioButton();
            this.lblHPPokemon3 = new System.Windows.Forms.Label();
            this.lblViePokemon3 = new System.Windows.Forms.Label();
            this.picPekeman1 = new System.Windows.Forms.PictureBox();
            this.optPokemon1 = new System.Windows.Forms.RadioButton();
            this.lblHPPokemon1 = new System.Windows.Forms.Label();
            this.lblViePokemon1 = new System.Windows.Forms.Label();
            this.picFireBallGauche = new System.Windows.Forms.PictureBox();
            this.picFireBallDroite = new System.Windows.Forms.PictureBox();
            this.tmrSlideOutCompagnon = new System.Windows.Forms.Timer(this.components);
            this.tmrSlideInCompagnon = new System.Windows.Forms.Timer(this.components);
            this.tmrBouleFeu = new System.Windows.Forms.Timer(this.components);
            this.pnlCombat = new System.Windows.Forms.Panel();
            this.picCompagnon = new System.Windows.Forms.PictureBox();
            this.picEnnemi = new System.Windows.Forms.PictureBox();
            this.pnlHpYourPekeman = new System.Windows.Forms.Panel();
            this.lblNomPokemon = new System.Windows.Forms.Label();
            this.lblViePokemon = new System.Windows.Forms.Label();
            this.pnlHpEnnemi = new System.Windows.Forms.Panel();
            this.lblNomEnnemi = new System.Windows.Forms.Label();
            this.lblVieEnnemi = new System.Windows.Forms.Label();
            this.pnlMenuCombat = new System.Windows.Forms.Panel();
            this.btnSoigner = new System.Windows.Forms.Button();
            this.btnPokemon = new System.Windows.Forms.Button();
            this.lblQtePotion = new System.Windows.Forms.Label();
            this.lblQtePokeball = new System.Windows.Forms.Label();
            this.picPokeball = new System.Windows.Forms.PictureBox();
            this.picPotion = new System.Windows.Forms.PictureBox();
            this.btnAttraper = new System.Windows.Forms.Button();
            this.btnFuir = new System.Windows.Forms.Button();
            this.btnAttaquer = new System.Windows.Forms.Button();
            this.txtHistorique = new System.Windows.Forms.TextBox();
            this.pnlAttaques = new System.Windows.Forms.Panel();
            this.lblNbBasePower = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblBasePower = new System.Windows.Forms.Label();
            this.lblNbPP = new System.Windows.Forms.Label();
            this.lblPP = new System.Windows.Forms.Label();
            this.lblAttaque = new System.Windows.Forms.Label();
            this.lstAttaques = new System.Windows.Forms.ListBox();
            this.ctlAttraperBouton1 = new TP2_Pékéman.Vue.Composants_jeu.CtlAttraperBouton();
            this.pnlChoixEquipe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFireBallGauche)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFireBallDroite)).BeginInit();
            this.pnlCombat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCompagnon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEnnemi)).BeginInit();
            this.pnlHpYourPekeman.SuspendLayout();
            this.pnlHpEnnemi.SuspendLayout();
            this.pnlMenuCombat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPokeball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPotion)).BeginInit();
            this.pnlAttaques.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrSlideOutEnnemi
            // 
            this.tmrSlideOutEnnemi.Tick += new System.EventHandler(this.tmrSlideOutEnnemi_Tick);
            // 
            // _ctnBarreVieEnnemi
            // 
            this._ctnBarreVieEnnemi.BackColor = System.Drawing.SystemColors.Control;
            this._ctnBarreVieEnnemi.Location = new System.Drawing.Point(15, 61);
            this._ctnBarreVieEnnemi.Name = "_ctnBarreVieEnnemi";
            this._ctnBarreVieEnnemi.Size = new System.Drawing.Size(204, 18);
            this._ctnBarreVieEnnemi.TabIndex = 3;
            // 
            // _ctnBarreViePekeman
            // 
            this._ctnBarreViePekeman.BackColor = System.Drawing.SystemColors.Control;
            this._ctnBarreViePekeman.Location = new System.Drawing.Point(17, 60);
            this._ctnBarreViePekeman.Name = "_ctnBarreViePekeman";
            this._ctnBarreViePekeman.Size = new System.Drawing.Size(204, 18);
            this._ctnBarreViePekeman.TabIndex = 2;
            // 
            // pnlChoixEquipe
            // 
            this.pnlChoixEquipe.BackColor = System.Drawing.Color.White;
            this.pnlChoixEquipe.BackgroundImage = global::TP2_Pékéman.Properties.Resources.pokemon_menu;
            this.pnlChoixEquipe.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlChoixEquipe.Controls.Add(this.btnPnlEquipeCancel);
            this.pnlChoixEquipe.Controls.Add(this.btnOkPnlEquipe);
            this.pnlChoixEquipe.Controls.Add(this.picPekeman6);
            this.pnlChoixEquipe.Controls.Add(this.optPokemon6);
            this.pnlChoixEquipe.Controls.Add(this.lblHPPokemon6);
            this.pnlChoixEquipe.Controls.Add(this.lblViePokemon6);
            this.pnlChoixEquipe.Controls.Add(this.picPekeman4);
            this.pnlChoixEquipe.Controls.Add(this.optPokemon4);
            this.pnlChoixEquipe.Controls.Add(this.lblHPPokemon4);
            this.pnlChoixEquipe.Controls.Add(this.lblViePokemon4);
            this.pnlChoixEquipe.Controls.Add(this.picPekeman2);
            this.pnlChoixEquipe.Controls.Add(this.optPokemon2);
            this.pnlChoixEquipe.Controls.Add(this.lblHPPokemon2);
            this.pnlChoixEquipe.Controls.Add(this.lblViePokemon2);
            this.pnlChoixEquipe.Controls.Add(this.picPekeman5);
            this.pnlChoixEquipe.Controls.Add(this.optPokemon5);
            this.pnlChoixEquipe.Controls.Add(this.lblHPPokemon5);
            this.pnlChoixEquipe.Controls.Add(this.lblViePokemon5);
            this.pnlChoixEquipe.Controls.Add(this.picPekeman3);
            this.pnlChoixEquipe.Controls.Add(this.optPokemon3);
            this.pnlChoixEquipe.Controls.Add(this.lblHPPokemon3);
            this.pnlChoixEquipe.Controls.Add(this.lblViePokemon3);
            this.pnlChoixEquipe.Controls.Add(this.picPekeman1);
            this.pnlChoixEquipe.Controls.Add(this.optPokemon1);
            this.pnlChoixEquipe.Controls.Add(this.lblHPPokemon1);
            this.pnlChoixEquipe.Controls.Add(this.lblViePokemon1);
            this.pnlChoixEquipe.Location = new System.Drawing.Point(0, 0);
            this.pnlChoixEquipe.Name = "pnlChoixEquipe";
            this.pnlChoixEquipe.Size = new System.Drawing.Size(700, 571);
            this.pnlChoixEquipe.TabIndex = 6;
            this.pnlChoixEquipe.Visible = false;
            // 
            // btnPnlEquipeCancel
            // 
            this.btnPnlEquipeCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPnlEquipeCancel.Location = new System.Drawing.Point(505, 490);
            this.btnPnlEquipeCancel.Name = "btnPnlEquipeCancel";
            this.btnPnlEquipeCancel.Size = new System.Drawing.Size(135, 28);
            this.btnPnlEquipeCancel.TabIndex = 27;
            this.btnPnlEquipeCancel.Text = "CANCEL";
            this.btnPnlEquipeCancel.UseVisualStyleBackColor = true;
            this.btnPnlEquipeCancel.Click += new System.EventHandler(this.btnPnlEquipeCancel_Click);
            // 
            // btnOkPnlEquipe
            // 
            this.btnOkPnlEquipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkPnlEquipe.Location = new System.Drawing.Point(364, 490);
            this.btnOkPnlEquipe.Name = "btnOkPnlEquipe";
            this.btnOkPnlEquipe.Size = new System.Drawing.Size(135, 28);
            this.btnOkPnlEquipe.TabIndex = 26;
            this.btnOkPnlEquipe.Text = "OK";
            this.btnOkPnlEquipe.UseVisualStyleBackColor = true;
            this.btnOkPnlEquipe.Click += new System.EventHandler(this.btnOkPnlEquipe_Click);
            // 
            // picPekeman6
            // 
            this.picPekeman6.Image = ((System.Drawing.Image)(resources.GetObject("picPekeman6.Image")));
            this.picPekeman6.Location = new System.Drawing.Point(517, 362);
            this.picPekeman6.Name = "picPekeman6";
            this.picPekeman6.Size = new System.Drawing.Size(123, 113);
            this.picPekeman6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPekeman6.TabIndex = 25;
            this.picPekeman6.TabStop = false;
            // 
            // optPokemon6
            // 
            this.optPokemon6.AutoSize = true;
            this.optPokemon6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.optPokemon6.Location = new System.Drawing.Point(378, 362);
            this.optPokemon6.Name = "optPokemon6";
            this.optPokemon6.Size = new System.Drawing.Size(128, 28);
            this.optPokemon6.TabIndex = 24;
            this.optPokemon6.Text = "SHIBA INU";
            this.optPokemon6.UseVisualStyleBackColor = true;
            this.optPokemon6.CheckedChanged += new System.EventHandler(this.optPokemon6_CheckedChanged);
            // 
            // lblHPPokemon6
            // 
            this.lblHPPokemon6.AutoSize = true;
            this.lblHPPokemon6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblHPPokemon6.Location = new System.Drawing.Point(417, 400);
            this.lblHPPokemon6.Name = "lblHPPokemon6";
            this.lblHPPokemon6.Size = new System.Drawing.Size(60, 24);
            this.lblHPPokemon6.TabIndex = 23;
            this.lblHPPokemon6.Text = "35/35";
            // 
            // lblViePokemon6
            // 
            this.lblViePokemon6.AutoSize = true;
            this.lblViePokemon6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblViePokemon6.Location = new System.Drawing.Point(378, 400);
            this.lblViePokemon6.Name = "lblViePokemon6";
            this.lblViePokemon6.Size = new System.Drawing.Size(38, 24);
            this.lblViePokemon6.TabIndex = 22;
            this.lblViePokemon6.Text = "HP";
            // 
            // picPekeman4
            // 
            this.picPekeman4.Image = ((System.Drawing.Image)(resources.GetObject("picPekeman4.Image")));
            this.picPekeman4.Location = new System.Drawing.Point(517, 210);
            this.picPekeman4.Name = "picPekeman4";
            this.picPekeman4.Size = new System.Drawing.Size(123, 113);
            this.picPekeman4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPekeman4.TabIndex = 21;
            this.picPekeman4.TabStop = false;
            // 
            // optPokemon4
            // 
            this.optPokemon4.AutoSize = true;
            this.optPokemon4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.optPokemon4.Location = new System.Drawing.Point(378, 210);
            this.optPokemon4.Name = "optPokemon4";
            this.optPokemon4.Size = new System.Drawing.Size(128, 28);
            this.optPokemon4.TabIndex = 20;
            this.optPokemon4.Text = "SHIBA INU";
            this.optPokemon4.UseVisualStyleBackColor = true;
            this.optPokemon4.CheckedChanged += new System.EventHandler(this.optPokemon4_CheckedChanged);
            // 
            // lblHPPokemon4
            // 
            this.lblHPPokemon4.AutoSize = true;
            this.lblHPPokemon4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblHPPokemon4.Location = new System.Drawing.Point(417, 248);
            this.lblHPPokemon4.Name = "lblHPPokemon4";
            this.lblHPPokemon4.Size = new System.Drawing.Size(60, 24);
            this.lblHPPokemon4.TabIndex = 19;
            this.lblHPPokemon4.Text = "35/35";
            // 
            // lblViePokemon4
            // 
            this.lblViePokemon4.AutoSize = true;
            this.lblViePokemon4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblViePokemon4.Location = new System.Drawing.Point(376, 248);
            this.lblViePokemon4.Name = "lblViePokemon4";
            this.lblViePokemon4.Size = new System.Drawing.Size(38, 24);
            this.lblViePokemon4.TabIndex = 18;
            this.lblViePokemon4.Text = "HP";
            // 
            // picPekeman2
            // 
            this.picPekeman2.Image = ((System.Drawing.Image)(resources.GetObject("picPekeman2.Image")));
            this.picPekeman2.Location = new System.Drawing.Point(517, 58);
            this.picPekeman2.Name = "picPekeman2";
            this.picPekeman2.Size = new System.Drawing.Size(123, 113);
            this.picPekeman2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPekeman2.TabIndex = 17;
            this.picPekeman2.TabStop = false;
            // 
            // optPokemon2
            // 
            this.optPokemon2.AutoSize = true;
            this.optPokemon2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.optPokemon2.Location = new System.Drawing.Point(378, 58);
            this.optPokemon2.Name = "optPokemon2";
            this.optPokemon2.Size = new System.Drawing.Size(128, 28);
            this.optPokemon2.TabIndex = 16;
            this.optPokemon2.Text = "SHIBA INU";
            this.optPokemon2.UseVisualStyleBackColor = true;
            this.optPokemon2.CheckedChanged += new System.EventHandler(this.optPokemon2_CheckedChanged);
            // 
            // lblHPPokemon2
            // 
            this.lblHPPokemon2.AutoSize = true;
            this.lblHPPokemon2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblHPPokemon2.Location = new System.Drawing.Point(419, 96);
            this.lblHPPokemon2.Name = "lblHPPokemon2";
            this.lblHPPokemon2.Size = new System.Drawing.Size(60, 24);
            this.lblHPPokemon2.TabIndex = 15;
            this.lblHPPokemon2.Text = "35/35";
            // 
            // lblViePokemon2
            // 
            this.lblViePokemon2.AutoSize = true;
            this.lblViePokemon2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblViePokemon2.Location = new System.Drawing.Point(378, 96);
            this.lblViePokemon2.Name = "lblViePokemon2";
            this.lblViePokemon2.Size = new System.Drawing.Size(38, 24);
            this.lblViePokemon2.TabIndex = 14;
            this.lblViePokemon2.Text = "HP";
            // 
            // picPekeman5
            // 
            this.picPekeman5.Image = ((System.Drawing.Image)(resources.GetObject("picPekeman5.Image")));
            this.picPekeman5.Location = new System.Drawing.Point(197, 362);
            this.picPekeman5.Name = "picPekeman5";
            this.picPekeman5.Size = new System.Drawing.Size(123, 113);
            this.picPekeman5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPekeman5.TabIndex = 13;
            this.picPekeman5.TabStop = false;
            // 
            // optPokemon5
            // 
            this.optPokemon5.AutoSize = true;
            this.optPokemon5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.optPokemon5.Location = new System.Drawing.Point(61, 362);
            this.optPokemon5.Name = "optPokemon5";
            this.optPokemon5.Size = new System.Drawing.Size(128, 28);
            this.optPokemon5.TabIndex = 12;
            this.optPokemon5.Text = "SHIBA INU";
            this.optPokemon5.UseVisualStyleBackColor = true;
            this.optPokemon5.CheckedChanged += new System.EventHandler(this.optPokemon5_CheckedChanged);
            // 
            // lblHPPokemon5
            // 
            this.lblHPPokemon5.AutoSize = true;
            this.lblHPPokemon5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblHPPokemon5.Location = new System.Drawing.Point(102, 400);
            this.lblHPPokemon5.Name = "lblHPPokemon5";
            this.lblHPPokemon5.Size = new System.Drawing.Size(60, 24);
            this.lblHPPokemon5.TabIndex = 11;
            this.lblHPPokemon5.Text = "35/35";
            // 
            // lblViePokemon5
            // 
            this.lblViePokemon5.AutoSize = true;
            this.lblViePokemon5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblViePokemon5.Location = new System.Drawing.Point(61, 400);
            this.lblViePokemon5.Name = "lblViePokemon5";
            this.lblViePokemon5.Size = new System.Drawing.Size(38, 24);
            this.lblViePokemon5.TabIndex = 10;
            this.lblViePokemon5.Text = "HP";
            // 
            // picPekeman3
            // 
            this.picPekeman3.Image = ((System.Drawing.Image)(resources.GetObject("picPekeman3.Image")));
            this.picPekeman3.Location = new System.Drawing.Point(197, 210);
            this.picPekeman3.Name = "picPekeman3";
            this.picPekeman3.Size = new System.Drawing.Size(123, 113);
            this.picPekeman3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPekeman3.TabIndex = 9;
            this.picPekeman3.TabStop = false;
            // 
            // optPokemon3
            // 
            this.optPokemon3.AutoSize = true;
            this.optPokemon3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.optPokemon3.Location = new System.Drawing.Point(61, 210);
            this.optPokemon3.Name = "optPokemon3";
            this.optPokemon3.Size = new System.Drawing.Size(128, 28);
            this.optPokemon3.TabIndex = 8;
            this.optPokemon3.Text = "SHIBA INU";
            this.optPokemon3.UseVisualStyleBackColor = true;
            this.optPokemon3.CheckedChanged += new System.EventHandler(this.optPokemon3_CheckedChanged);
            // 
            // lblHPPokemon3
            // 
            this.lblHPPokemon3.AutoSize = true;
            this.lblHPPokemon3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblHPPokemon3.Location = new System.Drawing.Point(102, 248);
            this.lblHPPokemon3.Name = "lblHPPokemon3";
            this.lblHPPokemon3.Size = new System.Drawing.Size(60, 24);
            this.lblHPPokemon3.TabIndex = 7;
            this.lblHPPokemon3.Text = "35/35";
            // 
            // lblViePokemon3
            // 
            this.lblViePokemon3.AutoSize = true;
            this.lblViePokemon3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblViePokemon3.Location = new System.Drawing.Point(61, 248);
            this.lblViePokemon3.Name = "lblViePokemon3";
            this.lblViePokemon3.Size = new System.Drawing.Size(38, 24);
            this.lblViePokemon3.TabIndex = 6;
            this.lblViePokemon3.Text = "HP";
            // 
            // picPekeman1
            // 
            this.picPekeman1.Image = ((System.Drawing.Image)(resources.GetObject("picPekeman1.Image")));
            this.picPekeman1.Location = new System.Drawing.Point(197, 58);
            this.picPekeman1.Name = "picPekeman1";
            this.picPekeman1.Size = new System.Drawing.Size(123, 113);
            this.picPekeman1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPekeman1.TabIndex = 5;
            this.picPekeman1.TabStop = false;
            // 
            // optPokemon1
            // 
            this.optPokemon1.AutoSize = true;
            this.optPokemon1.Checked = true;
            this.optPokemon1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.optPokemon1.Location = new System.Drawing.Point(61, 58);
            this.optPokemon1.Name = "optPokemon1";
            this.optPokemon1.Size = new System.Drawing.Size(128, 28);
            this.optPokemon1.TabIndex = 4;
            this.optPokemon1.TabStop = true;
            this.optPokemon1.Text = "SHIBA INU";
            this.optPokemon1.UseVisualStyleBackColor = true;
            this.optPokemon1.CheckedChanged += new System.EventHandler(this.optPokemon1_CheckedChanged);
            // 
            // lblHPPokemon1
            // 
            this.lblHPPokemon1.AutoSize = true;
            this.lblHPPokemon1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblHPPokemon1.Location = new System.Drawing.Point(102, 96);
            this.lblHPPokemon1.Name = "lblHPPokemon1";
            this.lblHPPokemon1.Size = new System.Drawing.Size(60, 24);
            this.lblHPPokemon1.TabIndex = 3;
            this.lblHPPokemon1.Text = "35/35";
            // 
            // lblViePokemon1
            // 
            this.lblViePokemon1.AutoSize = true;
            this.lblViePokemon1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblViePokemon1.Location = new System.Drawing.Point(61, 96);
            this.lblViePokemon1.Name = "lblViePokemon1";
            this.lblViePokemon1.Size = new System.Drawing.Size(38, 24);
            this.lblViePokemon1.TabIndex = 2;
            this.lblViePokemon1.Text = "HP";
            // 
            // picFireBallGauche
            // 
            this.picFireBallGauche.BackColor = System.Drawing.Color.White;
            this.picFireBallGauche.Image = global::TP2_Pékéman.Properties.Resources.fireball_gauche;
            this.picFireBallGauche.Location = new System.Drawing.Point(381, 128);
            this.picFireBallGauche.Name = "picFireBallGauche";
            this.picFireBallGauche.Size = new System.Drawing.Size(75, 75);
            this.picFireBallGauche.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFireBallGauche.TabIndex = 29;
            this.picFireBallGauche.TabStop = false;
            // 
            // picFireBallDroite
            // 
            this.picFireBallDroite.BackColor = System.Drawing.Color.White;
            this.picFireBallDroite.Image = global::TP2_Pékéman.Properties.Resources.fireball_droite;
            this.picFireBallDroite.Location = new System.Drawing.Point(245, 333);
            this.picFireBallDroite.Name = "picFireBallDroite";
            this.picFireBallDroite.Size = new System.Drawing.Size(75, 75);
            this.picFireBallDroite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFireBallDroite.TabIndex = 28;
            this.picFireBallDroite.TabStop = false;
            // 
            // tmrSlideOutCompagnon
            // 
            this.tmrSlideOutCompagnon.Enabled = true;
            this.tmrSlideOutCompagnon.Tick += new System.EventHandler(this.tmrSlideOutCompagnon_Tick);
            // 
            // tmrSlideInCompagnon
            // 
            this.tmrSlideInCompagnon.Enabled = true;
            this.tmrSlideInCompagnon.Tick += new System.EventHandler(this.tmrSlideInCompagnon_Tick);
            // 
            // tmrBouleFeu
            // 
            this.tmrBouleFeu.Enabled = true;
            this.tmrBouleFeu.Tick += new System.EventHandler(this.tmrBouleFeu_Tick);
            // 
            // pnlCombat
            // 
            this.pnlCombat.BackColor = System.Drawing.Color.White;
            this.pnlCombat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlCombat.Controls.Add(this.picFireBallDroite);
            this.pnlCombat.Controls.Add(this.picFireBallGauche);
            this.pnlCombat.Controls.Add(this.picCompagnon);
            this.pnlCombat.Controls.Add(this.picEnnemi);
            this.pnlCombat.Controls.Add(this.pnlHpYourPekeman);
            this.pnlCombat.Controls.Add(this.pnlHpEnnemi);
            this.pnlCombat.Controls.Add(this.pnlMenuCombat);
            this.pnlCombat.Controls.Add(this.pnlAttaques);
            this.pnlCombat.Location = new System.Drawing.Point(0, 0);
            this.pnlCombat.Name = "pnlCombat";
            this.pnlCombat.Size = new System.Drawing.Size(700, 571);
            this.pnlCombat.TabIndex = 7;
            // 
            // picCompagnon
            // 
            this.picCompagnon.BackColor = System.Drawing.Color.White;
            this.picCompagnon.Location = new System.Drawing.Point(47, 208);
            this.picCompagnon.Name = "picCompagnon";
            this.picCompagnon.Size = new System.Drawing.Size(200, 200);
            this.picCompagnon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCompagnon.TabIndex = 10;
            this.picCompagnon.TabStop = false;
            // 
            // picEnnemi
            // 
            this.picEnnemi.BackColor = System.Drawing.Color.White;
            this.picEnnemi.InitialImage = global::TP2_Pékéman.Properties.Resources.pokeball;
            this.picEnnemi.Location = new System.Drawing.Point(455, 3);
            this.picEnnemi.Name = "picEnnemi";
            this.picEnnemi.Size = new System.Drawing.Size(200, 200);
            this.picEnnemi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picEnnemi.TabIndex = 9;
            this.picEnnemi.TabStop = false;
            // 
            // pnlHpYourPekeman
            // 
            this.pnlHpYourPekeman.BackgroundImage = global::TP2_Pékéman.Properties.Resources.healthbar;
            this.pnlHpYourPekeman.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlHpYourPekeman.Controls.Add(this._ctnBarreViePekeman);
            this.pnlHpYourPekeman.Controls.Add(this.lblNomPokemon);
            this.pnlHpYourPekeman.Controls.Add(this.lblViePokemon);
            this.pnlHpYourPekeman.Location = new System.Drawing.Point(455, 306);
            this.pnlHpYourPekeman.Name = "pnlHpYourPekeman";
            this.pnlHpYourPekeman.Size = new System.Drawing.Size(220, 102);
            this.pnlHpYourPekeman.TabIndex = 7;
            // 
            // lblNomPokemon
            // 
            this.lblNomPokemon.AutoSize = true;
            this.lblNomPokemon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblNomPokemon.Location = new System.Drawing.Point(61, 3);
            this.lblNomPokemon.Name = "lblNomPokemon";
            this.lblNomPokemon.Size = new System.Drawing.Size(24, 24);
            this.lblNomPokemon.TabIndex = 1;
            this.lblNomPokemon.Text = "--";
            // 
            // lblViePokemon
            // 
            this.lblViePokemon.AutoSize = true;
            this.lblViePokemon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblViePokemon.Location = new System.Drawing.Point(98, 33);
            this.lblViePokemon.Name = "lblViePokemon";
            this.lblViePokemon.Size = new System.Drawing.Size(44, 24);
            this.lblViePokemon.TabIndex = 0;
            this.lblViePokemon.Text = "--/--";
            // 
            // pnlHpEnnemi
            // 
            this.pnlHpEnnemi.BackgroundImage = global::TP2_Pékéman.Properties.Resources.healthbar2;
            this.pnlHpEnnemi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlHpEnnemi.Controls.Add(this._ctnBarreVieEnnemi);
            this.pnlHpEnnemi.Controls.Add(this.lblNomEnnemi);
            this.pnlHpEnnemi.Controls.Add(this.lblVieEnnemi);
            this.pnlHpEnnemi.Location = new System.Drawing.Point(3, 3);
            this.pnlHpEnnemi.Name = "pnlHpEnnemi";
            this.pnlHpEnnemi.Size = new System.Drawing.Size(220, 102);
            this.pnlHpEnnemi.TabIndex = 6;
            // 
            // lblNomEnnemi
            // 
            this.lblNomEnnemi.AutoSize = true;
            this.lblNomEnnemi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblNomEnnemi.Location = new System.Drawing.Point(64, 5);
            this.lblNomEnnemi.Name = "lblNomEnnemi";
            this.lblNomEnnemi.Size = new System.Drawing.Size(24, 24);
            this.lblNomEnnemi.TabIndex = 3;
            this.lblNomEnnemi.Text = "--";
            // 
            // lblVieEnnemi
            // 
            this.lblVieEnnemi.AutoSize = true;
            this.lblVieEnnemi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblVieEnnemi.Location = new System.Drawing.Point(101, 35);
            this.lblVieEnnemi.Name = "lblVieEnnemi";
            this.lblVieEnnemi.Size = new System.Drawing.Size(44, 24);
            this.lblVieEnnemi.TabIndex = 2;
            this.lblVieEnnemi.Text = "--/--";
            // 
            // pnlMenuCombat
            // 
            this.pnlMenuCombat.BackgroundImage = global::TP2_Pékéman.Properties.Resources.combat_menu;
            this.pnlMenuCombat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMenuCombat.Controls.Add(this.btnSoigner);
            this.pnlMenuCombat.Controls.Add(this.btnPokemon);
            this.pnlMenuCombat.Controls.Add(this.lblQtePotion);
            this.pnlMenuCombat.Controls.Add(this.lblQtePokeball);
            this.pnlMenuCombat.Controls.Add(this.picPokeball);
            this.pnlMenuCombat.Controls.Add(this.picPotion);
            this.pnlMenuCombat.Controls.Add(this.btnAttraper);
            this.pnlMenuCombat.Controls.Add(this.btnFuir);
            this.pnlMenuCombat.Controls.Add(this.btnAttaquer);
            this.pnlMenuCombat.Controls.Add(this.txtHistorique);
            this.pnlMenuCombat.Location = new System.Drawing.Point(0, 429);
            this.pnlMenuCombat.Name = "pnlMenuCombat";
            this.pnlMenuCombat.Size = new System.Drawing.Size(700, 139);
            this.pnlMenuCombat.TabIndex = 8;
            // 
            // btnSoigner
            // 
            this.btnSoigner.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSoigner.Location = new System.Drawing.Point(321, 55);
            this.btnSoigner.Name = "btnSoigner";
            this.btnSoigner.Size = new System.Drawing.Size(135, 28);
            this.btnSoigner.TabIndex = 2;
            this.btnSoigner.Text = "SOIGNER";
            this.btnSoigner.UseVisualStyleBackColor = true;
            this.btnSoigner.Click += new System.EventHandler(this.btnSoigner_Click);
            // 
            // btnPokemon
            // 
            this.btnPokemon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPokemon.Location = new System.Drawing.Point(321, 91);
            this.btnPokemon.Name = "btnPokemon";
            this.btnPokemon.Size = new System.Drawing.Size(276, 28);
            this.btnPokemon.TabIndex = 4;
            this.btnPokemon.Text = "PEKEMAN";
            this.btnPokemon.UseVisualStyleBackColor = true;
            this.btnPokemon.Click += new System.EventHandler(this.btnPokemon_Click);
            // 
            // lblQtePotion
            // 
            this.lblQtePotion.AutoSize = true;
            this.lblQtePotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblQtePotion.Location = new System.Drawing.Point(641, 59);
            this.lblQtePotion.Name = "lblQtePotion";
            this.lblQtePotion.Size = new System.Drawing.Size(24, 24);
            this.lblQtePotion.TabIndex = 10;
            this.lblQtePotion.Text = "--";
            // 
            // lblQtePokeball
            // 
            this.lblQtePokeball.AutoSize = true;
            this.lblQtePokeball.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblQtePokeball.Location = new System.Drawing.Point(641, 21);
            this.lblQtePokeball.Name = "lblQtePokeball";
            this.lblQtePokeball.Size = new System.Drawing.Size(24, 24);
            this.lblQtePokeball.TabIndex = 2;
            this.lblQtePokeball.Text = "--";
            // 
            // picPokeball
            // 
            this.picPokeball.Image = global::TP2_Pékéman.Properties.Resources.pokeball;
            this.picPokeball.Location = new System.Drawing.Point(610, 19);
            this.picPokeball.Name = "picPokeball";
            this.picPokeball.Size = new System.Drawing.Size(25, 25);
            this.picPokeball.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPokeball.TabIndex = 9;
            this.picPokeball.TabStop = false;
            // 
            // picPotion
            // 
            this.picPotion.Image = global::TP2_Pékéman.Properties.Resources.potion;
            this.picPotion.Location = new System.Drawing.Point(610, 57);
            this.picPotion.Name = "picPotion";
            this.picPotion.Size = new System.Drawing.Size(25, 25);
            this.picPotion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPotion.TabIndex = 8;
            this.picPotion.TabStop = false;
            // 
            // btnAttraper
            // 
            this.btnAttraper.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAttraper.Location = new System.Drawing.Point(462, 19);
            this.btnAttraper.Name = "btnAttraper";
            this.btnAttraper.Size = new System.Drawing.Size(135, 28);
            this.btnAttraper.TabIndex = 1;
            this.btnAttraper.Text = "ATTRAPER";
            this.btnAttraper.UseVisualStyleBackColor = true;
            this.btnAttraper.Click += new System.EventHandler(this.btnAttraper_Click);
            // 
            // btnFuir
            // 
            this.btnFuir.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuir.Location = new System.Drawing.Point(462, 55);
            this.btnFuir.Name = "btnFuir";
            this.btnFuir.Size = new System.Drawing.Size(135, 28);
            this.btnFuir.TabIndex = 3;
            this.btnFuir.Text = "FUIR";
            this.btnFuir.UseVisualStyleBackColor = true;
            this.btnFuir.Click += new System.EventHandler(this.btnFuir_Click);
            // 
            // btnAttaquer
            // 
            this.btnAttaquer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAttaquer.Location = new System.Drawing.Point(321, 19);
            this.btnAttaquer.Name = "btnAttaquer";
            this.btnAttaquer.Size = new System.Drawing.Size(135, 28);
            this.btnAttaquer.TabIndex = 0;
            this.btnAttaquer.Text = "ATTAQUER";
            this.btnAttaquer.UseVisualStyleBackColor = true;
            this.btnAttaquer.Click += new System.EventHandler(this.btnAttaquer_Click);
            // 
            // txtHistorique
            // 
            this.txtHistorique.BackColor = System.Drawing.Color.White;
            this.txtHistorique.Location = new System.Drawing.Point(20, 21);
            this.txtHistorique.Multiline = true;
            this.txtHistorique.Name = "txtHistorique";
            this.txtHistorique.ReadOnly = true;
            this.txtHistorique.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtHistorique.Size = new System.Drawing.Size(258, 100);
            this.txtHistorique.TabIndex = 3;
            // 
            // pnlAttaques
            // 
            this.pnlAttaques.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlAttaques.BackgroundImage")));
            this.pnlAttaques.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAttaques.Controls.Add(this.lblNbBasePower);
            this.pnlAttaques.Controls.Add(this.btnCancel);
            this.pnlAttaques.Controls.Add(this.btnOk);
            this.pnlAttaques.Controls.Add(this.lblBasePower);
            this.pnlAttaques.Controls.Add(this.lblNbPP);
            this.pnlAttaques.Controls.Add(this.lblPP);
            this.pnlAttaques.Controls.Add(this.lblAttaque);
            this.pnlAttaques.Controls.Add(this.lstAttaques);
            this.pnlAttaques.Location = new System.Drawing.Point(289, 429);
            this.pnlAttaques.Name = "pnlAttaques";
            this.pnlAttaques.Size = new System.Drawing.Size(411, 139);
            this.pnlAttaques.TabIndex = 11;
            // 
            // lblNbBasePower
            // 
            this.lblNbBasePower.AutoSize = true;
            this.lblNbBasePower.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblNbBasePower.Location = new System.Drawing.Point(336, 93);
            this.lblNbBasePower.Name = "lblNbBasePower";
            this.lblNbBasePower.Size = new System.Drawing.Size(24, 24);
            this.lblNbBasePower.TabIndex = 13;
            this.lblNbBasePower.Text = "--";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(107, 96);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(18, 96);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblBasePower
            // 
            this.lblBasePower.AutoSize = true;
            this.lblBasePower.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblBasePower.Location = new System.Drawing.Point(197, 92);
            this.lblBasePower.Name = "lblBasePower";
            this.lblBasePower.Size = new System.Drawing.Size(137, 24);
            this.lblBasePower.TabIndex = 10;
            this.lblBasePower.Text = "PUISSANCE :";
            // 
            // lblNbPP
            // 
            this.lblNbPP.AutoSize = true;
            this.lblNbPP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblNbPP.Location = new System.Drawing.Point(243, 60);
            this.lblNbPP.Name = "lblNbPP";
            this.lblNbPP.Size = new System.Drawing.Size(44, 24);
            this.lblNbPP.TabIndex = 9;
            this.lblNbPP.Text = "--/--";
            // 
            // lblPP
            // 
            this.lblPP.AutoSize = true;
            this.lblPP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblPP.Location = new System.Drawing.Point(197, 60);
            this.lblPP.Name = "lblPP";
            this.lblPP.Size = new System.Drawing.Size(48, 24);
            this.lblPP.TabIndex = 8;
            this.lblPP.Text = "PP :";
            // 
            // lblAttaque
            // 
            this.lblAttaque.AutoSize = true;
            this.lblAttaque.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblAttaque.Location = new System.Drawing.Point(197, 22);
            this.lblAttaque.Name = "lblAttaque";
            this.lblAttaque.Size = new System.Drawing.Size(24, 24);
            this.lblAttaque.TabIndex = 7;
            this.lblAttaque.Text = "--";
            // 
            // lstAttaques
            // 
            this.lstAttaques.FormattingEnabled = true;
            this.lstAttaques.Location = new System.Drawing.Point(18, 21);
            this.lstAttaques.Name = "lstAttaques";
            this.lstAttaques.Size = new System.Drawing.Size(164, 69);
            this.lstAttaques.TabIndex = 6;
            this.lstAttaques.SelectedIndexChanged += new System.EventHandler(this.lstAttaques_SelectedIndexChanged);
            // 
            // ctlAttraperBouton1
            // 
            this.ctlAttraperBouton1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ctlAttraperBouton1.BackgroundImage")));
            this.ctlAttraperBouton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ctlAttraperBouton1.Location = new System.Drawing.Point(0, 0);
            this.ctlAttraperBouton1.Name = "ctlAttraperBouton1";
            this.ctlAttraperBouton1.Size = new System.Drawing.Size(0, 0);
            this.ctlAttraperBouton1.TabIndex = 0;
            // 
            // CtlCombat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlCombat);
            this.Controls.Add(this.pnlChoixEquipe);
            this.Name = "CtlCombat";
            this.Size = new System.Drawing.Size(700, 571);
            this.pnlChoixEquipe.ResumeLayout(false);
            this.pnlChoixEquipe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFireBallGauche)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFireBallDroite)).EndInit();
            this.pnlCombat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCompagnon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEnnemi)).EndInit();
            this.pnlHpYourPekeman.ResumeLayout(false);
            this.pnlHpYourPekeman.PerformLayout();
            this.pnlHpEnnemi.ResumeLayout(false);
            this.pnlHpEnnemi.PerformLayout();
            this.pnlMenuCombat.ResumeLayout(false);
            this.pnlMenuCombat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPokeball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPotion)).EndInit();
            this.pnlAttaques.ResumeLayout(false);
            this.pnlAttaques.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Composants_jeu.CtlAttaquerBouton ctlAttaquerBouton1;
        private Composants_jeu.CtlAttraperBouton ctlAttraperBouton1;
        private System.Windows.Forms.Timer tmrSlideOutEnnemi;
        private System.Windows.Forms.Panel pnlChoixEquipe;
        private System.Windows.Forms.PictureBox picPekeman1;
        private System.Windows.Forms.RadioButton optPokemon1;
        private System.Windows.Forms.Label lblHPPokemon1;
        private System.Windows.Forms.Label lblViePokemon1;
        private System.Windows.Forms.PictureBox picPekeman6;
        private System.Windows.Forms.RadioButton optPokemon6;
        private System.Windows.Forms.Label lblHPPokemon6;
        private System.Windows.Forms.Label lblViePokemon6;
        private System.Windows.Forms.PictureBox picPekeman4;
        private System.Windows.Forms.RadioButton optPokemon4;
        private System.Windows.Forms.Label lblHPPokemon4;
        private System.Windows.Forms.Label lblViePokemon4;
        private System.Windows.Forms.PictureBox picPekeman2;
        private System.Windows.Forms.RadioButton optPokemon2;
        private System.Windows.Forms.Label lblHPPokemon2;
        private System.Windows.Forms.Label lblViePokemon2;
        private System.Windows.Forms.PictureBox picPekeman5;
        private System.Windows.Forms.RadioButton optPokemon5;
        private System.Windows.Forms.Label lblHPPokemon5;
        private System.Windows.Forms.Label lblViePokemon5;
        private System.Windows.Forms.PictureBox picPekeman3;
        private System.Windows.Forms.RadioButton optPokemon3;
        private System.Windows.Forms.Label lblHPPokemon3;
        private System.Windows.Forms.Label lblViePokemon3;
        private System.Windows.Forms.Button btnPnlEquipeCancel;
        private System.Windows.Forms.Button btnOkPnlEquipe;
        private System.Windows.Forms.Timer tmrSlideOutCompagnon;
        private System.Windows.Forms.Timer tmrSlideInCompagnon;
        private System.Windows.Forms.Timer tmrBouleFeu;
        private System.Windows.Forms.PictureBox picFireBallDroite;
        private System.Windows.Forms.PictureBox picFireBallGauche;
        private System.Windows.Forms.Panel pnlCombat;
        private System.Windows.Forms.PictureBox picCompagnon;
        private System.Windows.Forms.PictureBox picEnnemi;
        private System.Windows.Forms.Panel pnlHpYourPekeman;
        private CtnBarreVie _ctnBarreViePekeman;
        private System.Windows.Forms.Label lblNomPokemon;
        private System.Windows.Forms.Label lblViePokemon;
        private System.Windows.Forms.Panel pnlHpEnnemi;
        private CtnBarreVie _ctnBarreVieEnnemi;
        private System.Windows.Forms.Label lblNomEnnemi;
        private System.Windows.Forms.Label lblVieEnnemi;
        private System.Windows.Forms.Panel pnlMenuCombat;
        private System.Windows.Forms.Button btnSoigner;
        private System.Windows.Forms.Button btnPokemon;
        private System.Windows.Forms.Label lblQtePotion;
        private System.Windows.Forms.Label lblQtePokeball;
        private System.Windows.Forms.PictureBox picPokeball;
        private System.Windows.Forms.PictureBox picPotion;
        private System.Windows.Forms.Button btnAttraper;
        private System.Windows.Forms.Button btnFuir;
        private System.Windows.Forms.Button btnAttaquer;
        private System.Windows.Forms.TextBox txtHistorique;
        private System.Windows.Forms.Panel pnlAttaques;
        private System.Windows.Forms.Label lblNbBasePower;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblBasePower;
        private System.Windows.Forms.Label lblNbPP;
        private System.Windows.Forms.Label lblPP;
        private System.Windows.Forms.Label lblAttaque;
        private System.Windows.Forms.ListBox lstAttaques;
    }
}
