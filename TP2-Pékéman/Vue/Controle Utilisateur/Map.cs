﻿#region

using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;
using TP2_Pékéman.Vue.Tuiles;

#endregion

namespace TP2_Pékéman.Vue
{
    public sealed partial class Map : UserControl
    {
        private static readonly Random Random = new Random();
        
        // tableau de tuiles de la map
        private readonly Tuile[,] _tuiles;
        private CtlPekecenter _ctlCenter;
        private CtlPekemart _ctlMart;
        private Boolean _mapConstruite;
        // position x du coin gauche de la map
        private int _posX;
        // position y du coin gauche de la map
        private int _posY; 
        private readonly int _dogePosition = 9;       
        //private static Boolean estDansMarket;

        /// <summary>
        ///     Constructeur de la classe Map
        /// </summary>
        public Map()
        {
            InitializeComponent();
            DoubleBuffered = true;
            _tuiles = new Tuile[200, 200];
        }

        /// <summary>
        ///     Fonction qui desine la map avec les tuiles
        /// </summary>
        private void Map_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            //mesure de la tabble: 22 largeur, 18 hauteur
            if (_mapConstruite)
            {
                for (int y = 0; y < 19; y++)
                {
                    for (int x = 0; x < 22; x++)
                    {
                        g.DrawImage(_tuiles[x + _posX, y + _posY].image, x*32, y*32);
                    }
                }
            }
        }

        /// <summary>
        ///     Methode qui construit la map avec des tuiles
        /// </summary>
        public void ConstruireMap()
        {
            string sLine;
            int horizontal = 0;
            int vertical = 0;

            var arbreBottom = new Arbre(Resources.arbre_bottom);
            var arbre1 = new Arbre(Resources.arbre1);
            var arbre2 = new Arbre(Resources.arbre2);
            var arbreTop = new Arbre(Resources.arbre_top);
            var chemin = new Surface(Resources.chemin, true);
            var eau = new Surface(Resources.eau, false);
            var fleur1 = new Plante(Resources.fleur1, true, false);
            var fleur2 = new Plante(Resources.fleur2, true, false);
            var gazon = new Plante(Resources.gazon, true, false);
            var herbe = new Plante(Resources.herbe, true, true);
            var herbe2 = new Plante(Resources.herbe2, true, false);
            var herbe3 = new Plante(Resources.herbe3, true, true);
            var herbe4 = new Plante(Resources.herbe4, false, false);
            var maison1 = new Batiment(Resources.maison1, false);
            var maison2 = new Batiment(Resources.maison2, false);
            var maison3 = new Batiment(Resources.maison3, false);
            var mart = new Pancarte(Resources.mart, true, false);
            var pancarte = new Pancarte(Resources.pancarte, false, false);
            var poke = new Pancarte(Resources.poke, false, true);
            var porte = new Batiment(Resources.porte, true);
            var toit1 = new Batiment(Resources.toit1, false);
            var toit2 = new Batiment(Resources.toit2, false);
            var toit3 = new Batiment(Resources.toit3, false);
            var pekecenter = new Batiment(Resources.poke, true);

            var reader = new StringReader(Resources.map);
            while ((sLine = reader.ReadLine()) != null)
            {
                foreach (char c in sLine)
                {
                    switch (c)
                    {
                        case 'A':
                            _tuiles[horizontal, vertical] = arbreBottom;
                            break;
                        case '@':
                            _tuiles[horizontal, vertical] = arbre1;
                            break;
                        case '&':
                            _tuiles[horizontal, vertical] = arbre2;
                            break;
                        case 'a':
                            _tuiles[horizontal, vertical] = arbreTop;
                            break;
                        case 'C':
                            _tuiles[horizontal, vertical] = chemin;
                            break;
                        case 'E':
                            _tuiles[horizontal, vertical] = eau;
                            break;
                        case 'F':
                            _tuiles[horizontal, vertical] = fleur1;
                            break;
                        case 'f':
                            _tuiles[horizontal, vertical] = fleur2;
                            break;
                        case 'G':
                            _tuiles[horizontal, vertical] = gazon;
                            break;
                        case 'H':
                            _tuiles[horizontal, vertical] = herbe;
                            break;
                        case 'h':
                            _tuiles[horizontal, vertical] = herbe2;
                            break;
                        case '#':
                            _tuiles[horizontal, vertical] = herbe3;
                            break;
                        case 'q':
                            _tuiles[horizontal, vertical] = herbe4;
                            break;
                        case 'd':
                            _tuiles[horizontal, vertical] = maison1;
                            break;
                        case 'c':
                            _tuiles[horizontal, vertical] = maison2;
                            break;
                        case 'g':
                            _tuiles[horizontal, vertical] = maison3;
                            break;
                        case 'm':
                            _tuiles[horizontal, vertical] = mart;
                            break;
                        case 'P':
                            _tuiles[horizontal, vertical] = pancarte;
                            break;
                        case 'k':
                            _tuiles[horizontal, vertical] = poke;
                            break;
                        case 'p':
                            _tuiles[horizontal, vertical] = porte;
                            break;
                        case '<':
                            _tuiles[horizontal, vertical] = toit1;
                            break;
                        case '-':
                            _tuiles[horizontal, vertical] = toit2;
                            break;
                        case '>':
                            _tuiles[horizontal, vertical] = toit3;
                            break;
                    }
                    horizontal++;
                }
                horizontal = 0;
                vertical++;
            }
            //    objReader.Close();
            _mapConstruite = true;
        }

        /// <summary>
        ///     Methode qui verifie le deplacement du heros avec les tuiles et le limites du tableau
        /// </summary>
        private Boolean VerifierDeplacement(int posX, int posY, int joueurX, int joueurY)
        {
            if (posX < 179 && posX >= 0 && posY < 182 && posY >= 0)
            {
                if (_tuiles[joueurX, joueurY].Accessible)
                {
                    DemarrerCombat(joueurX, joueurY);
                    VerifierMarket(joueurX, joueurY);
                    VerifierCenter(joueurX, joueurY);
                    //  estDevantMarket(joueurX, joueurY);

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Methode qui demarre un combat selon les posibilites et sur la tuile plante
        /// </summary>
        private void DemarrerCombat(int posX, int posY)
        {
            if (_tuiles[posX, posY].combatPossible)
            {
                int chanceCombat = trouverPossibiliteCombat();
                if (chanceCombat == 7)
                {
                    var combat = new FrmCombat();
                    combat.InitialiserCombat();
                    combat.ShowDialog();
                }
            }
        }

        private void VerifierCenter(int posX, int posY)
        {
            if (_tuiles[posX - 1, posY].centerTrouve)
            {
                Console.Write(Resources.Msg_Bienvenu_pekecenter);
                FrmJeu.estDevantCenter = true;
                ctnHeros1.Enabled = false;
            }
        }

        /// <summary>
        ///     Methode qui verifie si le Joueur entre sur un market
        /// </summary>
        private void VerifierMarket(int posX, int posY)
        {
            if (_tuiles[posX - 1, posY].marketTrouve)
            {
                FrmJeu.estDevantMart = true;
                ctnHeros1.Enabled = false;
                //Noyau.LeJoueur.posX = _tuil;
            }
        }

        /// <summary>
        ///     Methode qui trouve un random pour demarrer le combat
        /// </summary>
        private int trouverPossibiliteCombat()
        {
            return Random.Next(1, 11);
        }

        /// <summary>
        ///     methode deplace le personnage selon les touches du clavier
        /// </summary>
        public void DeplacerPerso(char key)
        {
            if (FrmJeu.demarrer && FrmJeu.estPret)
            {
                switch (Char.ToUpper(key))
                {
                    case (char) Keys.S:
                        ctnHeros1.Deplacement = Deplacement.BAS;
                        if (VerifierDeplacement(_posX, _posY + 1, Noyau.LeJoueur.posX, Noyau.LeJoueur.posY + 1))
                        {
                            Noyau.LeJoueur.posY++;
                            _posY++;
                        }
                        break;
                    case (char) Keys.W:
                        ctnHeros1.Deplacement = Deplacement.HAUT;
                        if (VerifierDeplacement(_posX, _posY - 1, Noyau.LeJoueur.posX, Noyau.LeJoueur.posY - 1))
                        {
                            Noyau.LeJoueur.posY--;
                            _posY--;
                        }
                        break;
                    case (char) Keys.A:
                        ctnHeros1.Deplacement = Deplacement.GAUCHE;
                        if (VerifierDeplacement(_posX - 1, _posY, Noyau.LeJoueur.posX - 1, Noyau.LeJoueur.posY))
                        {
                            Noyau.LeJoueur.posX--;
                            _posX--;
                        }
                        break;
                    case (char) Keys.D:
                        ctnHeros1.Deplacement = Deplacement.DROITE;
                        if (VerifierDeplacement(_posX + 1, _posY, Noyau.LeJoueur.posX + 1, Noyau.LeJoueur.posY))
                        {
                            Noyau.LeJoueur.posX++;
                            _posX++;
                        }
                        break;
                }

                Refresh();
            }
        }

        public int PosX
        {
            get { return _posX; }
            set { _posX = value; }
        }
        public int PosY
        {
            get { return _posY; }
            set { _posY = value; }
        }

    }
}