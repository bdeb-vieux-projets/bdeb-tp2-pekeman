﻿using System;
using System.Drawing;
using System.Security.AccessControl;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;

namespace TP2_Pékéman.Vue
{
    public class CtlNpc:UserControl
    {
        public Deplacement DeplacementNPC;
        private Image _image = Resources.npc_bas1;
        // chiffre qui represente le mouvement du personnage en horizontal
        private int x = 1;
        // chiffre qui represente le mouvement du personnage en vertical
        private int y = 1;
        private IDPHRASES _phrase = IDPHRASES.ZELDA;
        public Timer TmrAnimationNpc;
        private System.ComponentModel.IContainer components;
        //booleene qui determine si le npc se deplace
        private Boolean _seDeplace;
        private Boolean _directionEstHorizontale;
        private int _distanceParcours;
        private readonly Boolean _accessible = false;

        public CtlNpc()
        {
            
            InitializeComponent();
            _seDeplace = true;
            _directionEstHorizontale = true;
            DoubleBuffered = true;
            DeplacementNPC = Deplacement.BAS;
            
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            TmrAnimationNpc = new Timer(components);
             SuspendLayout();
            // 
            // TmrAnimationNpc
            // 
            TmrAnimationNpc.Interval = 200;
            TmrAnimationNpc.Tick += tmrAnimationNPC_Tick;
            // 
            // CtlNpc
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CtlNpc_Paint);
            BackColor = Color.Transparent;
            Name = "CtlNpc";
            Size = new Size(32, 32);
            ResumeLayout(false);

        }

        private void CtlNpc_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(_image,0,0);
        }

        private void tmrAnimationNPC_Tick(object sender, EventArgs e)
        {
            if (_seDeplace)
            {
                switch (DeplacementNPC)
                {
                    case Deplacement.BAS:
                        switch (y)
                        {
                            case 1:
                                _image = Resources.npc_bas1;
                                y++;
                                break;
                            case 2:
                                _image = Resources.npc_bas2;
                                y++;
                                break;
                            case 3:
                                _image = Resources.npc_bas3;
                                y = 2;
                                break;
                        }
                        break;
                    case Deplacement.HAUT:
                        switch (y)
                        {
                            case 1:
                                _image = Resources.npc_haut1;
                                y++;
                                break;
                            case 2:
                                _image = Resources.npc_haut2;
                                y++;
                                break;
                            case 3:
                                _image = Resources.npc_haut3;
                                y = 2;
                                break;
                        }
                        break;
                }

                switch (DeplacementNPC)
                {
                    case Deplacement.GAUCHE:
                        switch (x)
                        {
                            case 1:
                                _image = Resources.npc_gauche2;
                                x++;
                                break;
                            case 2:
                                _image = Resources.npc_gauche3;
                                x = 1;
                                break;
                        }
                        break;
                    case Deplacement.DROITE:
                        switch (x)
                        {
                            case 1:
                                _image = Resources.npc_droite2;
                                x++;
                                break;
                            case 2:
                                _image = Resources.npc_droite3;
                                x = 1;
                                break;
                        }
                        break;
                }
            }
            Refresh();
        }

        public void InitialiserNPC(IDPHRASES phrase, Boolean seDeplace, Boolean directionEstHorizontale, int distanceDeParcous)
        {
            Phrase = phrase;
            _seDeplace = seDeplace;
            _directionEstHorizontale = directionEstHorizontale;
            DistanceParcours = distanceDeParcous;
        }

        public Boolean accessible
        {
            get { return _accessible; }
        }

        public IDPHRASES Phrase
        {
            get { return _phrase; }
            set { _phrase = value; }
        }

        public int DistanceParcours
        {
            get { return _distanceParcours; }
            set { _distanceParcours = value; }
        }
    }
}
