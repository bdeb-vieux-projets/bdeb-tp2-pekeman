﻿namespace TP2_Pékéman.Vue.Controle_Utilisateur
{
    partial class CtlFin
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.pctFin = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctFin)).BeginInit();
            this.SuspendLayout();
            // 
            // pctFin
            // 
            this.pctFin.Image = global::TP2_Pékéman.Properties.Resources.fin;
            this.pctFin.Location = new System.Drawing.Point(0, 0);
            this.pctFin.Name = "pctFin";
            this.pctFin.Size = new System.Drawing.Size(700, 635);
            this.pctFin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctFin.TabIndex = 0;
            this.pctFin.TabStop = false;
            // 
            // CtlFin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pctFin);
            this.Name = "CtlFin";
            this.Size = new System.Drawing.Size(700, 638);
            ((System.ComponentModel.ISupportInitialize)(this.pctFin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pctFin;
    }
}
