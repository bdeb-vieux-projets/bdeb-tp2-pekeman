﻿#region

using System;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using System.Drawing;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class CtlPokedex : UserControl
    {
        public CtlPokedex()
        {
            InitializeComponent();
            cboListPokemons.BackColor = Color.Red;
        }
        /// <summary>
        /// Methode qui charge la liste de pokemons que le joueur a rencontré
        /// </summary>
        public void ChargerPokedex()
        {
            foreach (var pokemon in Noyau.LeJoueur.pekedexList)
            {
                cboListPokemons.Items.Add(pokemon.Nom);    
            }
            
        }
        /// <summary>
        /// Methode qui charge les infos d'un pokemons sur l'interface
        /// </summary>
        public void ChargeInfoPokemon()
        {
            Pekeman pekemanRecherche = TrouverPokemon();
            picPokemon.Image = pekemanRecherche.Image;
            lblTypeEspece.Text = pekemanRecherche.Type.ToString();
            lblValWeight.Text = pekemanRecherche.Poids;
            lblValHeight.Text = pekemanRecherche.Taille;
            lblValCatchRate.Text = pekemanRecherche.CatchRate.ToString();

            lblAttack1.Text = pekemanRecherche.ListeAttaques[0].NomAttaque;
            lblAttack2.Text = pekemanRecherche.ListeAttaques[1].NomAttaque;
            txtDescription.Text = pekemanRecherche.Description;

        }

        private Pekeman TrouverPokemon()
        {
            Pekeman pekemanRecherche = null;
            foreach (var pekeman in Noyau.LeJoueur.pekedexList)
            {
                if (pekeman.Nom.Equals(cboListPokemons.SelectedItem))
                {
                    pekemanRecherche = pekeman;
                    break;
                }
            }
            return pekemanRecherche;
        }
        private void cboListPokemons_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChargeInfoPokemon();
        }
        
    }
}