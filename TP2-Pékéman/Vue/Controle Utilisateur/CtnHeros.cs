﻿#region

using System;
using System.Drawing;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class CtnHeros : UserControl
    {
        public Deplacement Deplacement;
        private Image _image = Resources.bas1;
        // chiffre qui represente le mouvement du personnage en horizontal
        private int _x = 1;
        // chiffre qui represente le mouvement du personnage en vertical
        private int _y = 1;

        /// <summary>
        ///     Constructeur de la classe CtnHeros
        /// </summary>
        public CtnHeros()
        {
            InitializeComponent();
            DoubleBuffered = true;
            Deplacement = Deplacement.BAS;
            
            tmrHeros.Start();
        }

        /// <summary>
        ///     fontion qui desinne l'image du heros
        /// </summary>
        private void CtnHeros_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(_image, 0, 0);
        }

        /// <summary>
        ///     Timer qui change les images du heros
        /// </summary>
        private void tmrHeros_Tick(object sender, EventArgs e)
        {
            switch (Deplacement)
            {
                case Deplacement.BAS:
                    switch (_y)
                    {
                        case 1:
                            _image = Resources.bas1;
                            _y++;
                            break;
                        case 2:
                            _image = Resources.bas2;
                            _y++;
                            break;
                        case 3:
                            _image = Resources.bas3;
                            _y = 2;
                            break;
                    }
                    break;
                case Deplacement.HAUT:
                    switch (_y)
                    {
                        case 1:
                            _image = Resources.haut1;
                            _y++;
                            break;
                        case 2:
                            _image = Resources.haut2;
                            _y++;
                            break;
                        case 3:
                            _image = Resources.haut3;
                            _y = 2;
                            break;
                    }
                    break;
                case Deplacement.GAUCHE:
                    switch (_x)
                    {
                        case 1:
                            _image = Resources.gauche1;
                            _x++;
                            break;
                        case 2:
                            _image = Resources.gauche2;
                            _x = 1;
                            break;
                    }
                    break;
                case Deplacement.DROITE:
                    switch (_x)
                    {
                        case 1:
                            _image = Resources.droite1;
                            _x++;
                            break;
                        case 2:
                            _image = Resources.droite2;
                            _x = 1;
                            break;
                    }
                    break;
            }
            Refresh();
        }
    }
}