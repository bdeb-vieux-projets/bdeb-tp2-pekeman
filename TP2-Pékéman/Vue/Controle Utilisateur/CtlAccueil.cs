﻿#region

using System;
using System.IO;
using System.Windows.Forms;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class CtlAccueil : UserControl
    {
        public CtlAccueil()
        {
            InitializeComponent();
            pctBoxAccueil.SendToBack();
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        }


        private void btnQuitter_Click(object sender, EventArgs e)
        {
            FrmJeu.QuitterApplication();
        }

        private void btnNouvellePartie_Click(object sender, EventArgs e)
        {
            FrmJeu.surEcranNomJoueur = true;
            FrmJeu.JouerMusique();
        }

        private void btnChargerPartie_Click(object sender, EventArgs e)
        {
            FrmJeu.boutonChargerPartie = true;
        }
    }
}