﻿namespace TP2_Pékéman.Vue
{
    sealed partial class Map
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ctnHeros1 = new TP2_Pékéman.Vue.CtnHeros();
            this.SuspendLayout();
            // 
            // ctnHeros1
            // 
            this.ctnHeros1.BackColor = System.Drawing.Color.Transparent;
            this.ctnHeros1.Location = new System.Drawing.Point(350, 286);
            this.ctnHeros1.Name = "ctnHeros1";
            this.ctnHeros1.Size = new System.Drawing.Size(32, 32);
            this.ctnHeros1.TabIndex = 0;
            // 
            // Map
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ctnHeros1);
            this.Name = "Map";
            this.Size = new System.Drawing.Size(700, 572);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Map_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        public CtnHeros ctnHeros1;
    }
}
