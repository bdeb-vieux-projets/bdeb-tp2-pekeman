﻿namespace TP2_Pékéman.Vue
{
    partial class CtlPokedex
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSpecies = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblCatchRate = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.picPokemon = new System.Windows.Forms.PictureBox();
            this.lblTypeEspece = new System.Windows.Forms.Label();
            this.lblValWeight = new System.Windows.Forms.Label();
            this.lblValHeight = new System.Windows.Forms.Label();
            this.lblValCatchRate = new System.Windows.Forms.Label();
            this.lblAttack1 = new System.Windows.Forms.Label();
            this.lblAttack2 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cboListPokemons = new System.Windows.Forms.ComboBox();
            this.lblAttack4 = new System.Windows.Forms.Label();
            this.lblAttack3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picPokemon)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSpecies
            // 
            this.lblSpecies.AutoSize = true;
            this.lblSpecies.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpecies.Location = new System.Drawing.Point(282, 139);
            this.lblSpecies.Name = "lblSpecies";
            this.lblSpecies.Size = new System.Drawing.Size(93, 24);
            this.lblSpecies.TabIndex = 2;
            this.lblSpecies.Text = "Espece :";
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.Location = new System.Drawing.Point(282, 175);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(80, 24);
            this.lblWeight.TabIndex = 3;
            this.lblWeight.Text = "Poids : ";
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeight.Location = new System.Drawing.Point(282, 211);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(73, 24);
            this.lblHeight.TabIndex = 4;
            this.lblHeight.Text = "Taille :";
            // 
            // lblCatchRate
            // 
            this.lblCatchRate.AutoSize = true;
            this.lblCatchRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCatchRate.Location = new System.Drawing.Point(282, 247);
            this.lblCatchRate.Name = "lblCatchRate";
            this.lblCatchRate.Size = new System.Drawing.Size(175, 24);
            this.lblCatchRate.TabIndex = 5;
            this.lblCatchRate.Text = "Taux de capture :";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(311, 338);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(131, 25);
            this.lblDescription.TabIndex = 6;
            this.lblDescription.Text = "Description";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 338);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "Attaques";
            // 
            // picPokemon
            // 
            this.picPokemon.Image = global::TP2_Pékéman.Properties.Resources.pokeball;
            this.picPokemon.Location = new System.Drawing.Point(44, 133);
            this.picPokemon.Name = "picPokemon";
            this.picPokemon.Size = new System.Drawing.Size(146, 137);
            this.picPokemon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPokemon.TabIndex = 0;
            this.picPokemon.TabStop = false;
            // 
            // lblTypeEspece
            // 
            this.lblTypeEspece.AutoSize = true;
            this.lblTypeEspece.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTypeEspece.Location = new System.Drawing.Point(381, 139);
            this.lblTypeEspece.Name = "lblTypeEspece";
            this.lblTypeEspece.Size = new System.Drawing.Size(24, 24);
            this.lblTypeEspece.TabIndex = 8;
            this.lblTypeEspece.Text = "--";
            // 
            // lblValWeight
            // 
            this.lblValWeight.AutoSize = true;
            this.lblValWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValWeight.Location = new System.Drawing.Point(361, 175);
            this.lblValWeight.Name = "lblValWeight";
            this.lblValWeight.Size = new System.Drawing.Size(24, 24);
            this.lblValWeight.TabIndex = 9;
            this.lblValWeight.Text = "--";
            // 
            // lblValHeight
            // 
            this.lblValHeight.AutoSize = true;
            this.lblValHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValHeight.Location = new System.Drawing.Point(361, 211);
            this.lblValHeight.Name = "lblValHeight";
            this.lblValHeight.Size = new System.Drawing.Size(24, 24);
            this.lblValHeight.TabIndex = 10;
            this.lblValHeight.Text = "--";
            // 
            // lblValCatchRate
            // 
            this.lblValCatchRate.AutoSize = true;
            this.lblValCatchRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValCatchRate.Location = new System.Drawing.Point(463, 247);
            this.lblValCatchRate.Name = "lblValCatchRate";
            this.lblValCatchRate.Size = new System.Drawing.Size(24, 24);
            this.lblValCatchRate.TabIndex = 11;
            this.lblValCatchRate.Text = "--";
            // 
            // lblAttack1
            // 
            this.lblAttack1.AutoSize = true;
            this.lblAttack1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack1.Location = new System.Drawing.Point(32, 391);
            this.lblAttack1.Name = "lblAttack1";
            this.lblAttack1.Size = new System.Drawing.Size(24, 24);
            this.lblAttack1.TabIndex = 12;
            this.lblAttack1.Text = "--";
            // 
            // lblAttack2
            // 
            this.lblAttack2.AutoSize = true;
            this.lblAttack2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack2.Location = new System.Drawing.Point(32, 423);
            this.lblAttack2.Name = "lblAttack2";
            this.lblAttack2.Size = new System.Drawing.Size(24, 24);
            this.lblAttack2.TabIndex = 13;
            this.lblAttack2.Text = "--";
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.Location = new System.Drawing.Point(314, 391);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(353, 134);
            this.txtDescription.TabIndex = 14;
            // 
            // cboListPokemons
            // 
            this.cboListPokemons.BackColor = System.Drawing.Color.Red;
            this.cboListPokemons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboListPokemons.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboListPokemons.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold);
            this.cboListPokemons.ForeColor = System.Drawing.Color.White;
            this.cboListPokemons.FormattingEnabled = true;
            this.cboListPokemons.Location = new System.Drawing.Point(221, 10);
            this.cboListPokemons.Name = "cboListPokemons";
            this.cboListPokemons.Size = new System.Drawing.Size(258, 45);
            this.cboListPokemons.Sorted = true;
            this.cboListPokemons.TabIndex = 0;
            this.cboListPokemons.SelectedIndexChanged += new System.EventHandler(this.cboListPokemons_SelectedIndexChanged);
            // 
            // lblAttack4
            // 
            this.lblAttack4.AutoSize = true;
            this.lblAttack4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack4.Location = new System.Drawing.Point(33, 487);
            this.lblAttack4.Name = "lblAttack4";
            this.lblAttack4.Size = new System.Drawing.Size(24, 24);
            this.lblAttack4.TabIndex = 17;
            this.lblAttack4.Text = "--";
            // 
            // lblAttack3
            // 
            this.lblAttack3.AutoSize = true;
            this.lblAttack3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttack3.Location = new System.Drawing.Point(33, 455);
            this.lblAttack3.Name = "lblAttack3";
            this.lblAttack3.Size = new System.Drawing.Size(24, 24);
            this.lblAttack3.TabIndex = 16;
            this.lblAttack3.Text = "--";
            // 
            // CtlPokedex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TP2_Pékéman.Properties.Resources.pokedexUI_gif;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.lblAttack4);
            this.Controls.Add(this.lblAttack3);
            this.Controls.Add(this.cboListPokemons);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblAttack2);
            this.Controls.Add(this.lblAttack1);
            this.Controls.Add(this.lblValCatchRate);
            this.Controls.Add(this.lblValHeight);
            this.Controls.Add(this.lblValWeight);
            this.Controls.Add(this.lblTypeEspece);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblCatchRate);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblSpecies);
            this.Controls.Add(this.picPokemon);
            this.DoubleBuffered = true;
            this.Name = "CtlPokedex";
            this.Size = new System.Drawing.Size(700, 590);
            ((System.ComponentModel.ISupportInitialize)(this.picPokemon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picPokemon;
        private System.Windows.Forms.Label lblSpecies;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.Label lblCatchRate;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTypeEspece;
        private System.Windows.Forms.Label lblValWeight;
        private System.Windows.Forms.Label lblValHeight;
        private System.Windows.Forms.Label lblValCatchRate;
        private System.Windows.Forms.Label lblAttack1;
        private System.Windows.Forms.Label lblAttack2;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ComboBox cboListPokemons;
        private System.Windows.Forms.Label lblAttack4;
        private System.Windows.Forms.Label lblAttack3;
    }
}
