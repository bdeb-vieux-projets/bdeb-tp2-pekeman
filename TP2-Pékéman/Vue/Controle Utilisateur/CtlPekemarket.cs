﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;
using TP2_Pékéman.Vue.Tuiles;

namespace TP2_Pékéman.Vue
{
    public class CtlPekemart : UserControl
    {
        private const int LONGUEUR = 22;
        private const int LARGEUR = 18;
        private static readonly Random RandomGenerator = new Random();
        private readonly Tuile[,] _tuilesPeke;
        public CtnHeros CtnHerosMart;
        public int PositionTapisX;
        private Boolean _arretThread;
        private Point _coordComptoirDroit = new Point(0, 0);
        private Point _coordComptoirGauche = new Point(0, 0);
        private CtlNpc _ctlNpcClient;
        private CtlNpc _ctlNpcComptoir;
        private bool _herosPlace = false;
        private Boolean _mapConstruite;
        private int _posHerosX;
        private int _posHerosY = LARGEUR - 1;
        private int _positionTapisX2;

        public CtlPekemart()
        {
            InitializeComponent();
            DoubleBuffered = true;
            _tuilesPeke = new Tuile[LONGUEUR, LARGEUR];
            ConstruireMapPeke();
        }

        public void ConstruireMapPeke()
        {
            int horizontal = 0, vertical = 0;

            var bancBottom = new Banc(Resources.banc_bottom);
            var bancTop = new Banc(Resources.banc_top);
            var comptoir1 = new Comptoir(Resources.comptoir_vertical_vide);
            var comptoir2 = new ComptoirOrdi(Resources.comptoir_vertical_ordi, false);
            var comptoir3 = new Comptoir(Resources.comptoir_horizontall_vide);
            var comptoir5 = new Comptoir(Resources.comptoir_coin_bas_droite);
            var comptoir4 = new Comptoir(Resources.comptoir_coin_basgauche);
            var mur1 = new Mur(Resources.mur);
            var tapis1 = new Tapis(Resources.tapis_entree, true);
            var ground = new Surface(Resources.plancher, true);
            var etagereTop = new Etagere(Resources.etagere_top);
            var etagereBottom = new Etagere(Resources.etagere_bottom);

            DessinerSol(ground);
            DessinerMur(mur1);
            DessinerTapis(tapis1);
            DessinerComptoir(comptoir1, comptoir2, comptoir3, comptoir4, comptoir5);
            if (ObtenirItem())
                DessinerBancs(bancTop, bancBottom);
            if (ObtenirItem())
                DessinerEtageres(etagereTop, etagereBottom, ground);

            _mapConstruite = true;
        }

        /// <summary>
        ///     Fonction qui decide si des items seront dessines dans le Pekemart
        ///     et Pekecenter
        /// </summary>
        /// <returns>Retourne true ou false</returns>
        private bool ObtenirItem()
        {
            bool construireItem = Convert.ToBoolean(RandomGenerator.Next()%2);
            return construireItem;
        }

        /// <summary>
        ///     Procedure qui dessine les etageres. Le nombre d'etageres est aleatoire
        /// </summary>
        /// <param name="etagereTop">Image de l'etagere superieure</param>
        /// <param name="etagereBottom">Image de l'etagere inferieure</param>
        /// <param name="ground">Image du sol</param>
        private void DessinerEtageres(Etagere etagereTop, Etagere etagereBottom, Surface ground)
        {
            int nbEtageresAlea = RandomGenerator.Next(1, 5);

            for (int i = 0; i < nbEtageresAlea; i++)
            {
                int positionAleaY = RandomGenerator.Next(LARGEUR - 2);
                int positionAleaX = RandomGenerator.Next(LONGUEUR - 2);
                int positionAleaYPlus = positionAleaY + 1;

                while (positionAleaY <= 4 || !(_tuilesPeke[positionAleaX, positionAleaY].GetType() ==
                                               typeof (Surface) &&
                                               _tuilesPeke[positionAleaX, positionAleaYPlus].GetType() ==
                                               typeof (Surface)))
                {
                    positionAleaY = RandomGenerator.Next(0, LARGEUR - 2);
                }

                _tuilesPeke[positionAleaX, positionAleaY] = etagereTop;
                positionAleaYPlus = positionAleaY + 1;
                _tuilesPeke[positionAleaX, positionAleaYPlus] = etagereBottom;
            }
        }

        /// <summary>
        ///     Procedure qui dessine le comptoir
        /// </summary>
        /// <param name="comptoir1">Comptoir vertical</param>
        /// <param name="comptoir2">Comptoir avec ordi</param>
        /// <param name="comptoir3">Comptoir horizontal</param>
        /// <param name="comptoir4">Comptoir du coin gauche</param>
        private void DessinerComptoir(Comptoir comptoir1, ComptoirOrdi comptoir2, Comptoir comptoir3, Comptoir comptoir5,
            Comptoir comptoir4)
        {
            int positionAleaY = RandomGenerator.Next(0, LARGEUR - 1);
            // pour pouvoir acceder a la caisse
            int longueurComptoir = RandomGenerator.Next(0, ((LONGUEUR - 1) - (positionAleaY + 1)));

            while (longueurComptoir < 3 || (positionAleaY + longueurComptoir) > LONGUEUR)
            {
                positionAleaY = RandomGenerator.Next(0, LARGEUR - 1);
                longueurComptoir = RandomGenerator.Next(0, ((LONGUEUR - 1) - (positionAleaY + 1)));
            }

            _coordComptoirDroit.Y = 3;
            _coordComptoirGauche.Y = 2;
            _coordComptoirDroit.X = positionAleaY;
            _coordComptoirGauche.X = _coordComptoirDroit.X + longueurComptoir - 1;
            //Hauteur du comptoir
            for (int i = 1; i < 4; i++)
            {
                _tuilesPeke[positionAleaY, i] = comptoir1;
            }
            // Dessine le coin bas gauche
            _tuilesPeke[positionAleaY++, 4] = comptoir4;
            //Longueur du comptoir
            for (int i = 0; i < longueurComptoir; i++)
            {
                _tuilesPeke[positionAleaY++, 4] = comptoir3;
            }
            //Dessine le comptoir coin droit 
            _tuilesPeke[positionAleaY, 4] = comptoir5;
            //Dessine le comptoir avec ordi
            _tuilesPeke[positionAleaY, 3] = comptoir2;
            //Dessine le comptoir vide droit
            for (int i = 2; i > 0; i--)
            {
                _tuilesPeke[positionAleaY, i] = comptoir1;
            }
        }

        /// <summary>
        ///     Procedure qui dessine les bancs. Le nombre de bancs est aleatoire
        /// </summary>
        /// <param name="bancTop">Banc inferieur</param>
        /// <param name="bancBottom">Banc superieur</param>
        private void DessinerBancs(Banc bancTop, Banc bancBottom)
        {
            int positionAleaX = RandomGenerator.Next(0, LONGUEUR - 1);
            int positionAleaY = RandomGenerator.Next(0, LARGEUR - 2);
            int posAleaYplus = positionAleaY++;

            while (positionAleaY <= 4 || !(_tuilesPeke[positionAleaX, positionAleaY].GetType() ==
                                           typeof (Surface) && _tuilesPeke[positionAleaX, posAleaYplus].GetType() ==
                                           typeof (Surface)))
            {
                positionAleaY = RandomGenerator.Next(0, LARGEUR - 1);
            }
            _tuilesPeke[positionAleaX, positionAleaY] = bancTop;
            positionAleaY += 1;
            _tuilesPeke[positionAleaX, positionAleaY] = bancBottom;
        }

        /// <summary>
        ///     Procedure qui dessine le sol
        /// </summary>
        /// <param name="ground">Image du sol</param>
        private void DessinerSol(Surface ground)
        {
            for (int x = 0; x < LARGEUR; x++)
            {
                for (int y = 0; y < LONGUEUR; y++)
                {
                    _tuilesPeke[y, x] = ground;
                }
            }
        }

        /// <summary>
        ///     Procedure qui dessine le tapis d'entree
        /// </summary>
        /// <param name="tapis1">Image du tapis</param>
        private void DessinerTapis(Tapis tapis1)
        {
            PositionTapisX = RandomGenerator.Next(0, LONGUEUR - 2);
            _positionTapisX2 = PositionTapisX + 1;

            _tuilesPeke[PositionTapisX, (LARGEUR - 1)] = tapis1;
            _tuilesPeke[_positionTapisX2, (LARGEUR - 1)] = tapis1;
        }

        /// <summary>
        ///     Procedure qui dessine le mur superieur
        /// </summary>
        /// <param name="spriteMur">Image du mur</param>
        private void DessinerMur(Mur spriteMur)
        {
            for (int i = 0; i < LONGUEUR; i++)
            {
                _tuilesPeke[i, 0] = spriteMur;
            }
        }

        private void InitializeComponent()
        {
            _ctlNpcComptoir = new CtlNpc();
            _ctlNpcClient = new CtlNpc();
            SuspendLayout();
            // 
            // ctlNpcComptoir
            // 
            _ctlNpcComptoir.BackColor = Color.Transparent;
            _ctlNpcComptoir.Location = new Point(660, 25);
            _ctlNpcComptoir.Name = "_ctlNpcComptoir";
            _ctlNpcComptoir.Size = new Size(32, 32);
            _ctlNpcComptoir.TabIndex = 0;

            _ctlNpcClient.BackColor = Color.Transparent;
            _ctlNpcClient.Name = "_ctlNpcClient";
            _ctlNpcClient.Size = new Size(32, 32);
            _ctlNpcClient.TabIndex = 0;
            // 
            // CtlPekemart
            // 
            Controls.Add(_ctlNpcComptoir);
            Controls.Add(_ctlNpcClient);
            Name = "CtlPekemart";
            Size = new Size(700, 567);
            Load += CtlPekemart_Load;
            Paint += CtlPekemart_Paint;
            ResumeLayout(false);
        }

        /// <summary>
        ///     Procedure qui se charge de dessiner le Pekemarket au complet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CtlPekemart_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            //mesure de la tabble: 22 largeur, 18 hauteur
            if (_mapConstruite)
            {
                for (int y = 0; y < LARGEUR; y++)
                {
                    for (int x = 0; x < LONGUEUR; x++)
                    {
                        g.DrawImage(_tuilesPeke[x, y].image, x*32, y*32);
                    }
                }
            }
        }

        private void CtlPekemart_Load(object sender, EventArgs e)
        {
            int posTapisX = 0;
            if (FrmJeu.estDevantMart)
            {
                CtnHerosMart = new CtnHeros();
                CtnHerosMart.BackColor = Color.Transparent;

                for (int i = 0; i < LONGUEUR; i++)
                {
                    for (int j = 0; j < LARGEUR; j++)
                    {
                        if (_tuilesPeke[i, j].GetType() == typeof (Tapis))
                        {
                            posTapisX = i - 1;
                        }
                    }
                }

                CtnHerosMart.Location = new Point((posTapisX)*32, (LARGEUR - 1)*32);
                _posHerosX = posTapisX;
                _posHerosY = LARGEUR - 1;
                CtnHerosMart.Name = "ctnHeros1";
                CtnHerosMart.Size = new Size(32, 32);
                CtnHerosMart.TabIndex = 0;
                CtnHerosMart.Deplacement = Deplacement.HAUT;
                Controls.Add(CtnHerosMart);
                PlacerLesNpc();
                DemarrerAnimation();
            }
        }

        public void DeplacerPerso(char key)
        {
            if (FrmJeu.estDevantMart && CtnHerosMart != null)
            {
                switch (Char.ToUpper(key))
                {
                    case (char) Keys.S:
                        CtnHerosMart.Deplacement = Deplacement.BAS;
                        if (VerifierDeplacement(_posHerosX, _posHerosY + 1))
                        {
                            _posHerosY++;
                        }
                        break;
                    case (char) Keys.W:
                        CtnHerosMart.Deplacement = Deplacement.HAUT;
                        if (VerifierDeplacement(_posHerosX, _posHerosY - 1))
                        {
                            _posHerosY--;
                        }
                        break;
                    case (char) Keys.A:
                        CtnHerosMart.Deplacement = Deplacement.GAUCHE;
                        if (VerifierDeplacement(_posHerosX - 1, _posHerosY))
                        {
                            _posHerosX--;
                        }
                        break;
                    case (char) Keys.D:
                        CtnHerosMart.Deplacement = Deplacement.DROITE;
                        if (VerifierDeplacement(_posHerosX + 1, _posHerosY))
                        {
                            _posHerosX++;
                        }
                        break;
                }
                CtnHerosMart.Location = new Point((_posHerosX)*32, (_posHerosY)*32);

                Refresh();
            }
        }

        private Boolean VerifierDeplacement(int posX, int posY)
        {
            if (posX < LONGUEUR && posX >= 0 && posY < LARGEUR && posY >= 0)
            {
                if (_ctlNpcClient.Location == new Point(posX*32, posY*32))
                {
                    MessageBox.Show(Phrases.GetEnumDescription(_ctlNpcClient.Phrase), "NPC",
                        MessageBoxButtons.OK);
                }
                //if (Math.Abs(Math.Abs(_ctlNpcComptoir.Location.X) - posX * 32) == 32
                //    || Math.Abs(Math.Abs(_ctlNpcComptoir.Location.Y) - posY * 32) == 32)
                //{
                //    MessageBox.Show(Phrases.GetEnumDescription(_ctlNpcComptoir.Phrase), "NPC",
                //        MessageBoxButtons.OK);
                //}
                if (_tuilesPeke[posX, posY].Accessible)
                {
                    return true;
                }


                if (_tuilesPeke[posX, posY].GetType() == typeof (Tapis))
                {
                    FrmJeu.estDevantMart = false;
                    Visible = false;
                    _arretThread = true;
                    return true;
                }
                if (_tuilesPeke[posX, posY].GetType() == typeof (ComptoirOrdi))
                {
                    var frmAchat = new FrmAchatPekemart();
                    frmAchat.ShowDialog();
                }
            }
            return false;
        }

        private void PlacerLesNpc()
        {
            PlacerNpcComptoir();
            PlacerNPCClient();
            _ctlNpcClient.TmrAnimationNpc.Start();
            Refresh();
        }

        private void PlacerNpcComptoir()
        {
            _coordComptoirDroit.X += 1;
            _ctlNpcComptoir.InitialiserNPC(IDPHRASES.ZELDA, true, true,
                _coordComptoirGauche.X - _coordComptoirDroit.X);
            _ctlNpcComptoir.Location = new Point(_coordComptoirDroit.X*32, _coordComptoirDroit.Y*32);
        }

        private void PlacerNPCClient()
        {
            _ctlNpcClient.InitialiserNPC(IDPHRASES.MARIO, true, true, 0);
            var randomX = new Random();
            var randomY = new Random();

            var coordNpcClient = new Point();
            coordNpcClient.X = randomX.Next(2, LONGUEUR);
            coordNpcClient.Y = randomY.Next(2, LARGEUR);

            if (!_tuilesPeke[coordNpcClient.X, coordNpcClient.Y].Accessible &&
                coordNpcClient != _ctlNpcComptoir.Location
                && _tuilesPeke[coordNpcClient.X, coordNpcClient.Y].Accessible
                && _tuilesPeke[coordNpcClient.X, coordNpcClient.Y].Accessible
                && _tuilesPeke[coordNpcClient.X, coordNpcClient.Y].Accessible)
            {
                coordNpcClient.X += 1;
                coordNpcClient.Y += 1;
            }

            coordNpcClient.X *= 32;
            coordNpcClient.Y *= 32;
            _ctlNpcClient.Location = coordNpcClient;
        }

        private void DemarrerAnimation()
        {
            var thread = new Thread(AnimerNPC);
            thread.IsBackground = true;
            thread.Name = "Animation des NPC";
            _arretThread = true;
            thread.Start();
        }

        private void AnimerNPC()
        {
            int x = 0;
            while (!_arretThread)
            {
                BeginInvoke(
                    (MethodInvoker) delegate
                    {
                        if (x < _ctlNpcClient.DistanceParcours)
                        {
                            _ctlNpcClient.Left -= 32;
                            --x;
                            _ctlNpcClient.DeplacementNPC = Deplacement.DROITE;
                        }
                        if (x > _ctlNpcClient.DistanceParcours)
                        {
                            _ctlNpcClient.Left += 32;
                            ++x;
                            _ctlNpcClient.DeplacementNPC = Deplacement.GAUCHE;
                        }
                        Refresh();
                    });
                Thread.Sleep(50);
            }
        }
    }
}