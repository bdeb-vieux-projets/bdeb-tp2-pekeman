﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;

namespace TP2_Pékéman.Vue
{
    public partial class CtlCombat : UserControl
    {
        private readonly Combat _combat;
        private readonly int _emplacement;
        private readonly Point _positionBouleDeFeuCompagnon = new Point(245, 333);
        private readonly Point _positionBouleDeFeuEnnemi = new Point(381, 128);
        private Attaque _attaqueChoisi = new Attaque("Test", 0, 0, 0);
        private Boolean _cibleEstCompagnon;
        private Boolean _finSlideOut = false;
        private int _indiceNouveauPokemon;
        private Point _positionBouleDeFeu;
        private Point _positionCible;
        private Thread _thread;
        private Boolean _tourEnnemi;

        public CtlCombat()
        {
            //  DoubleBuffered = true;
            _combat = new Combat();
            InitializeComponent();
            DesactiverToutLesLabelsPnlEquipe();
            InitiliserMenuChoixEquipe();
            InitialiserBarreVies();

            _emplacement = picCompagnon.Location.X;
            picFireBallDroite.Visible = false;
            picFireBallGauche.Visible = false;
            UpdateComposantsVies();
        }

        public void InitialiserBarreVies()
        {
            _ctnBarreVieEnnemi = new CtnBarreVie(_combat.Ennemi.NbVies, _combat.Ennemi.NbViesMax);
            _ctnBarreViePekeman = new CtnBarreVie(Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].NbVies,
                Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].NbViesMax);
        }


        private void AfficherInfoCombat(String attaquant, String nomAttaque)
        {
            txtHistorique.AppendText(attaquant + " utilise " + nomAttaque + "\n");
            txtHistorique.AppendText("======\n");
        }

        private void AfficherInfoEchapper()
        {
            if (_combat.Fuir)
                txtHistorique.AppendText("Vous avez réussi a échapper au pékéman " + _combat.Ennemi.Nom + "\n");
            else
                txtHistorique.AppendText("Vous n'avez pas réussi a échapper au pékéman " + _combat.Ennemi.Nom + "\n");
            txtHistorique.AppendText("======\n");
        }

        private void AfficherPokemonSoigne(Boolean estSoigne)
        {
            if (estSoigne)
                txtHistorique.AppendText("Vous avez soigné " + Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom +
                                         " de 10 pts de vies\n");
            else
                txtHistorique.AppendText("Vous n'avez pas assez de potions!\n");
            txtHistorique.AppendText("======\n");
        }

        private void btnAttaquer_Click(object sender, EventArgs e)
        {
            pnlAttaques.BringToFront();
            lstAttaques.SelectedIndex = 0;
        }

        private void btnAttraper_Click(object sender, EventArgs e)
        {
            Boolean estAttrape = _combat.AttraperPekeman();
            if (estAttrape)
            {
                picEnnemi.Image = Resources.pokeball;
                Refresh();
                txtHistorique.AppendText("Vous avez attrappé le pékéman " + _combat.Ennemi.Nom + "\n");
                txtHistorique.AppendText("======\n");
                Thread.Sleep(1500);
                var topLevelControl = (Form) TopLevelControl;
                if (topLevelControl != null) topLevelControl.Close();
            }
            if (!_combat.PlusDePokeballs)
            {
                txtHistorique.AppendText("Vous n'avez pas plus de pokéballs!\n");
                txtHistorique.AppendText("======\n");
            }
            txtHistorique.AppendText("Vous n'avez pas attrappé le pékéman " + _combat.Ennemi.Nom + "\n");
            txtHistorique.AppendText("======\n");
            UpdatePokeballLabel();
        }

        private void btnSoigner_Click(object sender, EventArgs e)
        {
            Boolean estSoigne = _combat.SoignerPekeman();
            AfficherPokemonSoigne(estSoigne);
            UpdatePotionLabel();
        }

        private void btnFuir_Click(object sender, EventArgs e)
        {
            _combat.ChanceFuirAttaque();
            _combat.RemettreEnVieEnnemi();
            AfficherInfoEchapper();
            if (_combat.Fuir)
            {
                Noyau.CombatEnCours = false;
                tmrSlideOutCompagnon.Start();
            }
        }

        public void InitialiserListeAttaques()
        {
            lstAttaques.Items.Clear();
            foreach (Attaque attaque in Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].ListeAttaques)
            {
                lstAttaques.Items.Add(attaque.NomAttaque);
            }
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            pnlAttaques.SendToBack();
            Refresh();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            _attaqueChoisi = null;
            _attaqueChoisi = TrouverAttaque();
            if (_attaqueChoisi != null)
            {
                _cibleEstCompagnon = false;
                Pekeman pokemonChoisi = Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi];
                _combat.CombattrePokemon(Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi], _combat.Ennemi,
                    _attaqueChoisi);
                UpdateComposantsVies();

                if (_combat.Ennemi.NbVies == 0)
                {
                    tmrSlideOutEnnemi.Enabled = true;
                    tmrSlideOutEnnemi.Start();
                    txtHistorique.AppendText("Le " + _combat.Ennemi.Nom + " est mort.");
                    txtHistorique.AppendText("======\n");
                }
                AfficherInfoCombat(pokemonChoisi.Nom, _attaqueChoisi.NomAttaque);
                tmrBouleFeu.Start();
                pnlAttaques.SendToBack();
                Thread.Sleep(500);
                _tourEnnemi = true;
                AiAttaque();
            }
            else
                MessageBox.Show("Vous devez sélectionner une attaque", "Combat", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
        }

        /// <summary>
        ///     Methode qui retourne l'attaque sélectionnée par le joueur.
        /// </summary>
        /// <param name="nomAttaque"></param>
        /// <returns></returns>
        private Attaque TrouverAttaque()
        {
            Attaque uneAttaque = null;
            foreach (Attaque attaque in Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].ListeAttaques)
            {
                if (attaque.NomAttaque.Equals(lstAttaques.SelectedItem))
                {
                    uneAttaque = attaque;
                    break;
                }
            }
            return uneAttaque;
        }

        public void UpdateComposantsVies()
        {
            UpdateBarreVies();
            UpdateEtiquettesVies();
            Refresh();
        }

        private void UpdateBarreVies()
        {
            _ctnBarreViePekeman.VieMax =
                Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].NbViesMax;
            _ctnBarreViePekeman.Vie =
                Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].NbVies;
            _ctnBarreVieEnnemi.VieMax = _combat.Ennemi.NbViesMax;
            _ctnBarreVieEnnemi.Vie = _combat.Ennemi.NbVies;
            Refresh();
        }

        private void UpdateEtiquettesVies()
        {
            Pekeman compagnon = Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi];
            lblViePokemon.Text = compagnon.NbVies + " / " + compagnon.NbViesMax;
            lblVieEnnemi.Text = _combat.Ennemi.NbVies + " / " + _combat.Ennemi.NbViesMax;
        }

        public void InitialiserNomsPekemans()
        {
            lblNomEnnemi.Text = _combat.Ennemi.Nom;
            lblNomPokemon.Text = Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom;
        }

        public void InitialiserImagesPokemons()
        {
            picEnnemi.Image = _combat.Ennemi.Image;
            picCompagnon.Image = Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].ImageBack;
        }

      


        private void lstAttaques_SelectedIndexChanged(object sender, EventArgs e)
        {
            Attaque attaque = TrouverAttaque();
            lblAttaque.Text = attaque.NomAttaque;
            lblNbPP.Text = attaque.ActualPowerPoints + " / " + attaque.MaxPowerPoints;
            lblNbBasePower.Text = attaque.BasePower.ToString();
        }

        public void UpdatePotionLabel()
        {
            lblQtePotion.Text = Noyau.LeJoueur.NbPotions.ToString(CultureInfo.InvariantCulture);
        }

        public void UpdatePokeballLabel()
        {
            lblQtePokeball.Text = Noyau.LeJoueur.NbPokeballs.ToString(CultureInfo.InvariantCulture);
        }


        private void tmrSlideOutEnnemi_Tick(object sender, EventArgs e)
        {
            if (!Noyau.CombatEnCours)
            {
                while (picEnnemi.Left < Width)
                {
                    picEnnemi.Left += 1;
                    Thread.Sleep(1);
                }
                var topLevelControl = (Form) TopLevelControl;
                if (topLevelControl != null) topLevelControl.Close();
                tmrSlideOutEnnemi.Stop();
                _combat.RemettreEnVieEnnemi();
            }
        }

        /// <summary>
        ///     Methode qui initialise la liste de l'equipe de pokemons du joueur
        /// </summary>
        private void InitiliserMenuChoixEquipe()
        {
            //initialise les radio buttons
            if (Noyau.LeJoueur.Equipe.Any())
            {
                optPokemon1.Text = Noyau.LeJoueur.Equipe[0].Nom;
                optPokemon1.Enabled = true;
                picPekeman1.Image = Noyau.LeJoueur.Equipe[0].Image;
                lblHPPokemon1.Text = Noyau.LeJoueur.Equipe[0].NbVies + "/" + Noyau.LeJoueur.Equipe[0].NbViesMax;
            }
            if (Noyau.LeJoueur.Equipe.Count() >= 2)
            {
                optPokemon2.Text = Noyau.LeJoueur.Equipe[1].Nom;
                optPokemon2.Enabled = true;
                picPekeman2.Image = Noyau.LeJoueur.Equipe[1].Image;
                lblHPPokemon2.Text = Noyau.LeJoueur.Equipe[1].NbVies + "/" + Noyau.LeJoueur.Equipe[1].NbViesMax;
            }
            if (Noyau.LeJoueur.Equipe.Count() >= 3)
            {
                optPokemon3.Text = Noyau.LeJoueur.Equipe[2].Nom;
                optPokemon3.Enabled = true;
                picPekeman3.Image = Noyau.LeJoueur.Equipe[2].Image;
                lblHPPokemon3.Text = Noyau.LeJoueur.Equipe[2].NbVies + "/" + Noyau.LeJoueur.Equipe[2].NbViesMax;
            }
            if (Noyau.LeJoueur.Equipe.Count() >= 4)
            {
                optPokemon4.Text = Noyau.LeJoueur.Equipe[3].Nom;
                optPokemon4.Enabled = true;
                picPekeman4.Image = Noyau.LeJoueur.Equipe[3].Image;
                lblHPPokemon4.Text = Noyau.LeJoueur.Equipe[3].NbVies + "/" + Noyau.LeJoueur.Equipe[3].NbViesMax;
            }
            if (Noyau.LeJoueur.Equipe.Count() >= 5)
            {
                optPokemon5.Text = Noyau.LeJoueur.Equipe[4].Nom;
                optPokemon5.Enabled = true;
                picPekeman5.Image = Noyau.LeJoueur.Equipe[4].Image;
                lblHPPokemon5.Text = Noyau.LeJoueur.Equipe[4].NbVies + "/" + Noyau.LeJoueur.Equipe[4].NbViesMax;
            }
            if (Noyau.LeJoueur.Equipe.Count() == 6)
            {
                optPokemon6.Text = Noyau.LeJoueur.Equipe[5].Nom;
                optPokemon6.Enabled = true;
                picPekeman6.Image = Noyau.LeJoueur.Equipe[5].Image;
                lblHPPokemon6.Text = Noyau.LeJoueur.Equipe[5].NbVies + "/" + Noyau.LeJoueur.Equipe[5].NbViesMax;
            }
            DesactiveOptPokemonActif();
        }

        /// <summary>
        ///     methode qui desactive le radio button du pokemon actif
        /// </summary>
        private void DesactiveOptPokemonActif()
        {
            if (Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom.Equals(optPokemon1.Text))
            {
                optPokemon1.Enabled = false;
            }
            else if (Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom.Equals(optPokemon2.Text))
            {
                optPokemon2.Enabled = false;
            }
            else if (Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom.Equals(optPokemon3.Text))
            {
                optPokemon3.Enabled = false;
            }
            else if (Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom.Equals(optPokemon4.Text))
            {
                optPokemon4.Enabled = false;
            }
            else if (Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom.Equals(optPokemon5.Text))
            {
                optPokemon5.Enabled = false;
            }
            else if (Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom.Equals(optPokemon6.Text))
            {
                optPokemon6.Enabled = false;
            }
        }

        /// <summary>
        ///     Methode qui desactive tout les composants du menu de l'equipe
        /// </summary>
        private void DesactiverToutLesLabelsPnlEquipe()
        {
            optPokemon1.Text = Resources.strNoName;
            optPokemon1.Enabled = false;
            picPekeman1.Image = Resources.pokeball;
            lblHPPokemon1.Text = Resources.strNoHp;

            optPokemon2.Text = Resources.strNoName;
            optPokemon2.Enabled = false;
            picPekeman2.Image = Resources.pokeball;
            lblHPPokemon2.Text = Resources.strNoHp;

            optPokemon3.Text = Resources.strNoName;
            optPokemon3.Enabled = false;
            picPekeman3.Image = Resources.pokeball;
            lblHPPokemon3.Text = Resources.strNoHp;

            optPokemon4.Text = Resources.strNoName;
            optPokemon4.Enabled = false;
            picPekeman4.Image = Resources.pokeball;
            lblHPPokemon4.Text = Resources.strNoHp;

            optPokemon5.Text = Resources.strNoName;
            optPokemon5.Enabled = false;
            picPekeman5.Image = Resources.pokeball;
            lblHPPokemon5.Text = Resources.strNoHp;

            optPokemon6.Text = Resources.strNoName;
            optPokemon6.Enabled = false;
            picPekeman6.Image = Resources.pokeball;
            lblHPPokemon6.Text = Resources.strNoHp;
        }

        private void btnPnlEquipeCancel_Click(object sender, EventArgs e)
        {
            pnlChoixEquipe.Visible = false;
        }

        /// <summary>
        ///     Methode qui affiche l'equipe de pokemons du joueur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPokemon_Click(object sender, EventArgs e)
        {
            pnlChoixEquipe.Visible = true;
            pnlChoixEquipe.BringToFront();
        }

        /// <summary>
        ///     Methode qui detecte le moment où on confirme l'échange de pokémons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOkPnlEquipe_Click(object sender, EventArgs e)
        {
            int indicePokemonPrecedent = _combat.IndicePekemanChoisi;
            if (indicePokemonPrecedent != _indiceNouveauPokemon)
            {
                _combat.EchangerPokemon(_indiceNouveauPokemon);
                if (_combat.EstEchange)
                {
                    txtHistorique.AppendText("Reviens " + Noyau.LeJoueur.Equipe[indicePokemonPrecedent].Nom + "!\n");
                    txtHistorique.AppendText("======\n");
                    pnlChoixEquipe.Visible = false;
                    tmrSlideOutCompagnon.Start();
                }
            }
            else
            {
                MessageBox.Show("Vous ne pouvez pas sélectionner ce pékéman!", "Combat", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void tmrSlideOutCompagnon_Tick(object sender, EventArgs e)
        {
            if (!Noyau.CombatEnCours || _combat.EstEchange)
            {
                while (picCompagnon.Left != -picCompagnon.Width)
                {
                    picCompagnon.Left -= 1;
                    Thread.Sleep(1);
                }
                if (!Noyau.CombatEnCours)
                {
                    Thread.Sleep(500);
                    var topLevelControl = (Form) TopLevelControl;
                    if (topLevelControl != null) topLevelControl.Close();
                }
                else if (_combat.EstEchange)
                {
                    UpdateComposantsVies();
                    ChangerComposantsCompagons();
                    tmrSlideInCompagnon.Start();

                    txtHistorique.AppendText("GO " + Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi].Nom + "!\n");
                    txtHistorique.AppendText("======\n");
                }
                tmrSlideOutCompagnon.Stop();
            }
        }

        private void tmrSlideInCompagnon_Tick(object sender, EventArgs e)
        {
            if (_combat.EstEchange)
            {
                while (picCompagnon.Left != _emplacement)
                {
                    picCompagnon.Left += 1;
                    Thread.Sleep(3);
                }
                tmrSlideInCompagnon.Stop();
            }
        }

        /// <summary>
        ///     Methode qui se chage de changer les composants graphiques du compagnon.
        /// </summary>
        private void ChangerComposantsCompagons()
        {
            InitialiserImagesPokemons();
            InitialiserListeAttaques();
            InitialiserNomsPekemans();
            Refresh();
        }

        private void optPokemon1_CheckedChanged(object sender, EventArgs e)
        {
            _indiceNouveauPokemon = 0;
        }

        private void optPokemon2_CheckedChanged(object sender, EventArgs e)
        {
            _indiceNouveauPokemon = 1;
        }

        private void optPokemon3_CheckedChanged(object sender, EventArgs e)
        {
            _indiceNouveauPokemon = 2;
        }

        private void optPokemon4_CheckedChanged(object sender, EventArgs e)
        {
            _indiceNouveauPokemon = 3;
        }

        private void optPokemon5_CheckedChanged(object sender, EventArgs e)
        {
            _indiceNouveauPokemon = 4;
        }

        private void optPokemon6_CheckedChanged(object sender, EventArgs e)
        {
            _indiceNouveauPokemon = 5;
        }

        private void tmrBouleFeu_Tick(object sender, EventArgs e)
        {
            if (_attaqueChoisi.NomAttaque.Equals("TACKLE"))
            {
                if (!_cibleEstCompagnon)
                {
                    picFireBallDroite.Visible = true;
                    Refresh();
                    picFireBallDroite.BringToFront();
                    _positionBouleDeFeu = _positionBouleDeFeuCompagnon;
                    _positionCible = picEnnemi.Location;
                    while (_positionBouleDeFeu.X <= _positionCible.X &&
                           _positionBouleDeFeu.Y >= _positionCible.Y)
                    {
                        _positionBouleDeFeu.X += 1;
                        _positionBouleDeFeu.Y -= 1;
                        picFireBallDroite.Top = _positionBouleDeFeu.Y;
                        picFireBallDroite.Left = _positionBouleDeFeu.X;
                        Thread.Sleep(3);
                    }
                    picFireBallDroite.Visible = false;
                    Refresh();
                    //Fait clignoter l'ennemi s'il est attaqué
                    picEnnemi.Visible = false;
                    Refresh();
                    Thread.Sleep(5);
                    picEnnemi.Visible = true;
                    Refresh();
                }
                else if (_cibleEstCompagnon && _tourEnnemi)
                {
                    Refresh();
                    picFireBallGauche.Visible = true;
                    _positionBouleDeFeu = _positionBouleDeFeuEnnemi;
                    _positionCible = picCompagnon.Location;
                    while (_positionBouleDeFeu.X >= _positionCible.X &&
                           _positionBouleDeFeu.Y <= _positionCible.Y)
                    {
                        _positionBouleDeFeu.X -= 1;
                        _positionBouleDeFeu.Y += 1;
                        picFireBallGauche.Top = _positionBouleDeFeu.Y;
                        picFireBallGauche.Left = _positionBouleDeFeu.X;
                        Thread.Sleep(3);
                    }
                    picFireBallGauche.Visible = false;
                    Refresh();
                    //Fait clignoter l'ennemi s'il est attaqué
                    picCompagnon.Visible = false;
                    Refresh();
                    Thread.Sleep(5);
                    picCompagnon.Visible = true;
                    Refresh();
                }
                tmrBouleFeu.Stop();
            }
        }

        /// <summary>
        ///     Methode qui permet au ai de jouer.
        /// </summary>
        public void AiAttaque()
        {
            if (_tourEnnemi)
            {
                var randAttaque = new Random();
                String nomAttaque;
                int indiceAttaque = randAttaque.Next(_combat.Ennemi.ListeAttaques.Count);

                nomAttaque = _combat.Ennemi.ListeAttaques[indiceAttaque].NomAttaque;
                _combat.CombattrePokemon(_combat.Ennemi, Noyau.LeJoueur.Equipe[_combat.IndicePekemanChoisi],
                    _combat.Ennemi.ListeAttaques[indiceAttaque]);
                AfficherInfoCombat(_combat.Ennemi.Nom, nomAttaque);
                UpdateComposantsVies();
                _tourEnnemi = false;
            }
        }
    }
}