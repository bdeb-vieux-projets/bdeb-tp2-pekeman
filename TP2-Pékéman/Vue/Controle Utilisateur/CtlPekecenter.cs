﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;
using TP2_Pékéman.Vue.Tuiles;

namespace TP2_Pékéman.Vue
{
    public partial class CtlPekecenter : UserControl
    {
        private const int LONGUEUR = 22;
        private const int LARGEUR = 18;
        private const int HAUTEUR_COMPTOIR = 1;
        private const int MAX_ORDI = 3;
        private const int MIN_ITEM = 1;
        private const int MAX_BANCS = 5;
        private static readonly Random RandomGenerator = new Random();
        private readonly Tuile[,] _tuilesPeke;
        private Boolean _mapConstruite;
        public CtnHeros ctnHeros1;
        private Boolean dessinerItem;
        private int longueurComptoir;
        private int posHerosX;
        private int posHerosY;
        private int posX = 0;
        private int posY = 0;


        public CtlPekecenter()
        {
            InitializeComponent();
            DoubleBuffered = true;
            _tuilesPeke = new Tuile[LONGUEUR, LARGEUR];
            ConstruireMapCenter();
        }

        private void ConstruireMapCenter()
        {
            var ground = new Surface(Resources.plancher1, true);
            var murSup = new Mur(Resources.mur1);
            var plancherOmbre = new Surface(Resources.plancher_ombre, true);
            var ordiTop = new Ordinateur(Resources.ordi_top, false);
            var ordiBottom = new Ordinateur(Resources.ordi_bottom, false);
            var tapis = new Tapis(Resources.tapis_entree, true);
            var compt_coin = new ComptoirOrdi(Resources.coin, false);
            var compt_hori = new Comptoir(Resources.comptoir_horizontal);
            var compt_vert = new Comptoir(Resources.comptoir_vertical);
            var bancs = new Banc(Resources.pouf);
            DessinerSol(ground);
            DessinerMurSuperieur(murSup);
            DessinerPlancherOmbre(plancherOmbre);
            DessinerTapis(tapis);
            DessinerComptoir(compt_hori, compt_coin, compt_vert);
            DessinerBancs(bancs);
            DessinerOrdinateurs(ordiBottom, ordiTop);

            _mapConstruite = true;
        }

        private bool ObtenirItem()
        {
            bool construireItem = Convert.ToBoolean(RandomGenerator.Next()%2);
            return construireItem;
        }

        private void DessinerBancs(Banc bancs)
        {
            int positionAleaX = RandomGenerator.Next(0, LONGUEUR - 2);
            int positionAleaX2 = positionAleaX + 1;
            int nbRangeesBancs = RandomGenerator.Next(MIN_ITEM, MAX_BANCS);
            dessinerItem = ObtenirItem();
            if (dessinerItem)
            {
                for (int i = 0; i < nbRangeesBancs; i++)
                {
                    int positionAleaY = RandomGenerator.Next(0, LARGEUR - 1);

                    while (positionAleaY <= 4 || !(_tuilesPeke[positionAleaX, positionAleaY].GetType() ==
                                                   typeof (Surface) &&
                                                   (_tuilesPeke[positionAleaX2, positionAleaY].GetType() ==
                                                    typeof (Surface))))
                    {
                        positionAleaY = RandomGenerator.Next(0, LARGEUR - 1);
                    }

                    _tuilesPeke[positionAleaX, positionAleaY] = bancs;
                    _tuilesPeke[positionAleaX, positionAleaY] = bancs;
                }
            }
        }

        private void DessinerComptoir(Comptoir compt_hori, ComptoirOrdi compt_coin, Comptoir compt_vert)
        {
            int positionAleaLarg = RandomGenerator.Next(0, LARGEUR - 1);
            longueurComptoir = RandomGenerator.Next(0, (LONGUEUR - positionAleaLarg));

            while (longueurComptoir < 3)
                longueurComptoir = RandomGenerator.Next(0, (LONGUEUR - positionAleaLarg));
            // Dessine la longueur du comptoir
            for (int i = 0; i < longueurComptoir; i++)
            {
                _tuilesPeke[i, 2] = compt_hori;
            }
            // Dessine le coin du comptoir
            _tuilesPeke[longueurComptoir, 2] = compt_coin;
            // Dessine la hauteur du comptoir
            for (int i = 0; i < HAUTEUR_COMPTOIR; i++)
            {
                _tuilesPeke[longueurComptoir, HAUTEUR_COMPTOIR] = compt_vert;
            }
        }

        private void DessinerTapis(Tapis tapis)
        {
            int positionAlea = RandomGenerator.Next(0, LONGUEUR - 1);

            for (int i = 0; i < 2; i++)
            {
                _tuilesPeke[positionAlea, (LARGEUR - 1)] = tapis;
                positionAlea++;
            }
        }

        private void DessinerOrdinateurs(Ordinateur ordiBottom, Ordinateur ordiTop)
        {
            int nbOrdi = RandomGenerator.Next(MIN_ITEM, MAX_ORDI);
            _tuilesPeke[LONGUEUR - 1, 1] = ordiTop;
            _tuilesPeke[LONGUEUR - 1, 2] = ordiBottom;

            for (int i = 0; i < nbOrdi; i++)
            {
                int positionAleaLong = RandomGenerator.Next(0, LONGUEUR - 1);
                while (positionAleaLong <= longueurComptoir)
                {
                    positionAleaLong = RandomGenerator.Next(0, LONGUEUR - 1);
                }
                _tuilesPeke[positionAleaLong, 1] = ordiTop;
                _tuilesPeke[positionAleaLong, 2] = ordiBottom;
            }
        }

        private void DessinerPlancherOmbre(Surface plancherOmbre)
        {
            for (int i = 0; i < LONGUEUR; i++)
            {
                _tuilesPeke[i, 1] = plancherOmbre;
            }
        }

        private void DessinerMurSuperieur(Mur murSup)
        {
            for (int i = 0; i < LONGUEUR; i++)
            {
                _tuilesPeke[i, 0] = murSup;
            }
        }

        private void DessinerSol(Surface ground)
        {
            for (int x = 0; x < LARGEUR; x++)
            {
                for (int y = 0; y < LONGUEUR; y++)
                {
                    _tuilesPeke[y, x] = ground;
                }
            }
        }

        private void CtlPekecenter_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            //mesure de la tabble: 22 largeur, 18 hauteur
            if (_mapConstruite)
            {
                for (int y = 0; y < LARGEUR; y++)
                {
                    for (int x = 0; x < LONGUEUR; x++)
                    {
                        g.DrawImage(_tuilesPeke[x + posX, y + posY].image, x*32, y*32);
                    }
                }
            }
        }

        public void DeplacerPerso(char key)
        {
            if (FrmJeu.estDevantCenter && ctnHeros1 != null)
            {
                switch (Char.ToUpper(key))
                {
                    case (char) Keys.S:
                        ctnHeros1.Deplacement = Deplacement.BAS;
                        if (VerifierDeplacement(posHerosX, posHerosY + 1))
                        {
                            posHerosY++;
                        }
                        break;
                    case (char) Keys.W:
                        ctnHeros1.Deplacement = Deplacement.HAUT;
                        if (VerifierDeplacement(posHerosX, posHerosY - 1))
                        {
                            posHerosY--;
                        }
                        break;
                    case (char) Keys.A:
                        ctnHeros1.Deplacement = Deplacement.GAUCHE;
                        if (VerifierDeplacement(posHerosX - 1, posHerosY))
                        {
                            posHerosX--;
                        }
                        break;
                    case (char) Keys.D:
                        ctnHeros1.Deplacement = Deplacement.DROITE;
                        if (VerifierDeplacement(posHerosX + 1, posHerosY))
                        {
                            posHerosX++;
                        }
                        break;
                }
                ctnHeros1.Location = new Point((posHerosX)*32, (posHerosY)*32);
                Refresh();
            }
        }

        private Boolean VerifierDeplacement(int PosX, int PosY)
        {
            if (PosX < LONGUEUR && PosX >= 0 && PosY < LARGEUR && PosY >= 0)
            {
                if (_tuilesPeke[PosX, PosY].Accessible)
                {
                    return true;
                }
                if (_tuilesPeke[PosX, PosY].GetType() == typeof (Tapis))
                {
                    Visible = false;
                    FrmJeu.estDevantCenter = false;
                }
                else if (_tuilesPeke[PosX, PosY].GetType() == typeof (ComptoirOrdi))
                {
                    int count = verifierViesPekemans();
                    count = 6; // test
                    if (count > 0)
                    {
                        var frmCenter = new FrmRegenererVies();
                        frmCenter.ShowDialog();
                        frmCenter.StartPosition = FormStartPosition.CenterScreen;
                    }
                    else
                    {
                        MessageBox.Show("Vos pekemans possedent tous le nombre de vie maximale", "",
                            MessageBoxButtons.OK, MessageBoxIcon.None);
                    }
                }
            }
            return false;
        }

        private int verifierViesPekemans()
        {
            int count = 0;
            foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
            {
                if (pekeman.NbVies < pekeman.NbViesMax)
                {
                    count++;
                }
            }
            return count;
        }

        private void CtlPekecenter_Load(object sender, EventArgs e)
        {
            int posTapisX = 0;
            if (FrmJeu.estDevantCenter)
            {
                ctnHeros1 = new CtnHeros();
                ctnHeros1.BackColor = Color.Transparent;

                for (int i = 0; i < LONGUEUR; i++)
                {
                    for (int j = 0; j < LARGEUR; j++)
                    {
                        if (_tuilesPeke[i, j].GetType() == typeof (Tapis))
                        {
                            posTapisX = i - 1;
                        }
                    }
                }

                ctnHeros1.Location = new Point((posTapisX)*32, (LARGEUR - 1)*32);
                posHerosX = posTapisX;
                posHerosY = LARGEUR - 1;
                ctnHeros1.Name = "ctnHeros1";
                ctnHeros1.Size = new Size(32, 32);
                ctnHeros1.TabIndex = 0;
                ctnHeros1.Deplacement = Deplacement.HAUT;
                Controls.Add(ctnHeros1);
            }
        }

        private void tmrAnimerNPC_Tick(object sender, EventArgs e)
        {
        }
    }
}