﻿namespace TP2_Pékéman.Vue
{
    partial class CtlNomJoueur
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CtlNomJoueur));
            this.picHero = new System.Windows.Forms.PictureBox();
            this.ctlOakMessage = new TP2_Pékéman.Vue.CtlOakMessage();
            this.ctlNameBox = new TP2_Pékéman.Vue.CtlNameBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.picHero)).BeginInit();
            this.ctlNameBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // picHero
            // 
            this.picHero.Image = global::TP2_Pékéman.Properties.Resources.gold_personnage;
            this.picHero.Location = new System.Drawing.Point(449, 79);
            this.picHero.Name = "picHero";
            this.picHero.Size = new System.Drawing.Size(182, 267);
            this.picHero.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHero.TabIndex = 2;
            this.picHero.TabStop = false;
            // 
            // ctlOakMessage
            // 
            this.ctlOakMessage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ctlOakMessage.BackgroundImage")));
            this.ctlOakMessage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ctlOakMessage.Location = new System.Drawing.Point(2, 392);
            this.ctlOakMessage.Name = "ctlOakMessage";
            this.ctlOakMessage.Size = new System.Drawing.Size(697, 156);
            this.ctlOakMessage.TabIndex = 1;
            // 
            // ctlNameBox
            // 
            this.ctlNameBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ctlNameBox.BackgroundImage")));
            this.ctlNameBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ctlNameBox.Controls.Add(this.btnOk);
            this.ctlNameBox.Controls.Add(this.txtName);
            this.ctlNameBox.Location = new System.Drawing.Point(3, 3);
            this.ctlNameBox.Name = "ctlNameBox";
            this.ctlNameBox.Size = new System.Drawing.Size(192, 103);
            this.ctlNameBox.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(95, 63);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(22, 37);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(148, 20);
            this.txtName.TabIndex = 0;
            // 
            // CtlNomJoueur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picHero);
            this.Controls.Add(this.ctlOakMessage);
            this.Controls.Add(this.ctlNameBox);
            this.Name = "CtlNomJoueur";
            this.Size = new System.Drawing.Size(700, 700);
            ((System.ComponentModel.ISupportInitialize)(this.picHero)).EndInit();
            this.ctlNameBox.ResumeLayout(false);
            this.ctlNameBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public CtlNameBox ctlNameBox;
        public CtlOakMessage ctlOakMessage;
        private System.Windows.Forms.PictureBox picHero;
        public System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnOk;

    }
}
