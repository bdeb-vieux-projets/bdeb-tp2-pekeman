﻿#region

using System.Drawing;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class CtnBarreVie : UserControl
    {
        private readonly Image barre;
        private int _vie ;
        private int _vieMax ;
        private int maxBarre = 144; // équivaut à 100%
         

        public int VieMax
        {
          set { _vieMax = value; }
        }

        public int Vie
        {
            set { _vie = value; }
        }

        /// <summary>
        ///     Constructeur de la classe CtnBarreVie
        /// </summary>
        public CtnBarreVie(int vie, int vieMax)
        {
            _vie = vie;
            _vieMax = vieMax;
               InitializeComponent();
               barre = Resources.hp_vide;
        }
       
        /// <summary>
        ///     Fontion qui design le control
        /// </summary>
        private void ctnBarreVie_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(barre, 0, 0);
            double longueurPixel = RechercherPourcentage();
            longueurPixel *= maxBarre;
            g.FillRectangle(Brushes.Green, 48, 0, (float) longueurPixel, 15);
            g.Dispose();
        }

        /// <summary>
        ///     Procedure qui cherche la vie restant pour la designer sur le control et retourne un double
        /// </summary>
        private double RechercherPourcentage()
        {
            double vie = _vie,
                vieMax = _vieMax;
            return _vie / _vieMax; ;
        }
    }
}