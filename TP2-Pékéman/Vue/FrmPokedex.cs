﻿
using System.Windows.Forms;

namespace TP2_Pékéman.Vue
{
    public partial class FrmPokedex : Form
    {
        public FrmPokedex()
        {
            InitializeComponent();
        }

        public void ChargerPokedex()
        {
            ctlPokedex.ChargerPokedex();
        }

        private void FrmPokedex_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }     
    }
}
