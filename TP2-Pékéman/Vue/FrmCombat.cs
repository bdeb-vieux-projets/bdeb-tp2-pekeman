﻿using System;
using System.Windows.Forms;
using TP2_Pékéman.Modele;

namespace TP2_Pékéman.Vue
{
    public partial class FrmCombat : Form
    {
        public FrmCombat()
        {
            InitializeComponent();
            tmrCombat.Start();
        }

        public void InitialiserCombat()
        {
            ctlCombat.InitialiserNomsPekemans();
            ctlCombat.InitialiserImagesPokemons();
            ctlCombat.InitialiserListeAttaques();
            ctlCombat.UpdateComposantsVies();
            ctlCombat.UpdatePokeballLabel();
            ctlCombat.UpdatePotionLabel();
        }
    }
}
