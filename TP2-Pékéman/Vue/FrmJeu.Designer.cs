﻿namespace TP2_Pékéman.Vue
{
    partial class FrmJeu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmJeu));
            this.tmrDemarrer = new System.Windows.Forms.Timer(this.components);
            this.tmrMenu = new System.Windows.Forms.Timer(this.components);
            this.tmrNomJoueur = new System.Windows.Forms.Timer(this.components);
            this.tmrPekemart = new System.Windows.Forms.Timer(this.components);
            this.mnuPrincipal = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouvellePartieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirPartie = new System.Windows.Forms.ToolStripMenuItem();
            this.sauvegardeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.pékédexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.contrôleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.àProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrPekecenter = new System.Windows.Forms.Timer(this.components);
            this.tmrUpdateLabels = new System.Windows.Forms.Timer(this.components);
            this.pnlControlesJeu = new System.Windows.Forms.Panel();
            this.lblPotion = new System.Windows.Forms.Label();
            this.lblPokeball = new System.Windows.Forms.Label();
            this.lblArgent = new System.Windows.Forms.Label();
            this.picPokeball = new System.Windows.Forms.PictureBox();
            this.picPotion = new System.Windows.Forms.PictureBox();
            this.picArgent = new System.Windows.Forms.PictureBox();
            this.lblNomJoueur = new System.Windows.Forms.Label();
            this.lblNomText = new System.Windows.Forms.Label();
            this.btnPekedex = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.ctlNomJoueur = new TP2_Pékéman.Vue.CtlNomJoueur();
            this.ctlPekecenter1 = new TP2_Pékéman.Vue.CtlPekecenter();
            this.ctlPekemart = new TP2_Pékéman.Vue.CtlPekemart();
            this.logoNentindo1 = new TP2_Pékéman.Vue.LogoNentindo();
            this.ctlAccueil = new TP2_Pékéman.Vue.CtlAccueil();
            this.map = new TP2_Pékéman.Vue.Map();
            this.ctlFinDuJeu = new TP2_Pékéman.Vue.Controle_Utilisateur.CtlFin();
            this.mnuPrincipal.SuspendLayout();
            this.pnlControlesJeu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPokeball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPotion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picArgent)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrDemarrer
            // 
            this.tmrDemarrer.Tick += new System.EventHandler(this.tmrDemarrer_Tick);
            // 
            // tmrMenu
            // 
            this.tmrMenu.Tick += new System.EventHandler(this.tmrMenu_Tick);
            // 
            // tmrNomJoueur
            // 
            this.tmrNomJoueur.Tick += new System.EventHandler(this.tmrNomJoueur_Tick);
            // 
            // tmrPekemart
            // 
            this.tmrPekemart.Enabled = true;
            this.tmrPekemart.Tick += new System.EventHandler(this.tmrPekemart_Tick);
            // 
            // mnuPrincipal
            // 
            this.mnuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.toolStripMenuItem3});
            this.mnuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.mnuPrincipal.Name = "mnuPrincipal";
            this.mnuPrincipal.Size = new System.Drawing.Size(699, 24);
            this.mnuPrincipal.TabIndex = 23;
            this.mnuPrincipal.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouvellePartieToolStripMenuItem,
            this.ouvrirPartie,
            this.sauvegardeToolStripMenuItem,
            this.toolStripMenuItem1,
            this.pékédexToolStripMenuItem,
            this.toolStripMenuItem2,
            this.quitterToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // nouvellePartieToolStripMenuItem
            // 
            this.nouvellePartieToolStripMenuItem.Name = "nouvellePartieToolStripMenuItem";
            this.nouvellePartieToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.nouvellePartieToolStripMenuItem.Text = "Nouvelle partie";
            this.nouvellePartieToolStripMenuItem.Click += new System.EventHandler(this.nouvellePartieToolStripMenuItem_Click);
            // 
            // ouvrirPartie
            // 
            this.ouvrirPartie.Name = "ouvrirPartie";
            this.ouvrirPartie.Size = new System.Drawing.Size(154, 22);
            this.ouvrirPartie.Text = "Ouvrir...";
            this.ouvrirPartie.Click += new System.EventHandler(this.ouvrirPartie_Click_1);
            // 
            // sauvegardeToolStripMenuItem
            // 
            this.sauvegardeToolStripMenuItem.Name = "sauvegardeToolStripMenuItem";
            this.sauvegardeToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.sauvegardeToolStripMenuItem.Text = "Enregistrer...";
            this.sauvegardeToolStripMenuItem.Click += new System.EventHandler(this.sauvegardeToolStripMenuItem_Click_1);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(151, 6);
            // 
            // pékédexToolStripMenuItem
            // 
            this.pékédexToolStripMenuItem.Name = "pékédexToolStripMenuItem";
            this.pékédexToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.pékédexToolStripMenuItem.Text = "Pékédex...";
            this.pékédexToolStripMenuItem.Click += new System.EventHandler(this.pékédexToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(151, 6);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contrôleToolStripMenuItem,
            this.toolStripMenuItem4,
            this.àProposToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(24, 20);
            this.toolStripMenuItem3.Text = "?";
            // 
            // contrôleToolStripMenuItem
            // 
            this.contrôleToolStripMenuItem.Name = "contrôleToolStripMenuItem";
            this.contrôleToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.contrôleToolStripMenuItem.Text = "Contrôle...";
            this.contrôleToolStripMenuItem.Click += new System.EventHandler(this.contrôleToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(128, 6);
            // 
            // àProposToolStripMenuItem
            // 
            this.àProposToolStripMenuItem.Name = "àProposToolStripMenuItem";
            this.àProposToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.àProposToolStripMenuItem.Text = "À propos...";
            this.àProposToolStripMenuItem.Click += new System.EventHandler(this.àProposToolStripMenuItem_Click);
            // 
            // tmrPekecenter
            // 
            this.tmrPekecenter.Enabled = true;
            this.tmrPekecenter.Tick += new System.EventHandler(this.tmrPekecenter_Tick);
            // 
            // tmrUpdateLabels
            // 
            this.tmrUpdateLabels.Enabled = true;
            this.tmrUpdateLabels.Tick += new System.EventHandler(this.tmrUpdateLabels_Tick);
            // 
            // pnlControlesJeu
            // 
            this.pnlControlesJeu.BackgroundImage = global::TP2_Pékéman.Properties.Resources.background_controls;
            this.pnlControlesJeu.Controls.Add(this.lblPotion);
            this.pnlControlesJeu.Controls.Add(this.lblPokeball);
            this.pnlControlesJeu.Controls.Add(this.lblArgent);
            this.pnlControlesJeu.Controls.Add(this.picPokeball);
            this.pnlControlesJeu.Controls.Add(this.picPotion);
            this.pnlControlesJeu.Controls.Add(this.picArgent);
            this.pnlControlesJeu.Controls.Add(this.lblNomJoueur);
            this.pnlControlesJeu.Controls.Add(this.lblNomText);
            this.pnlControlesJeu.Controls.Add(this.btnPekedex);
            this.pnlControlesJeu.Controls.Add(this.btnDown);
            this.pnlControlesJeu.Controls.Add(this.btnRight);
            this.pnlControlesJeu.Controls.Add(this.btnLeft);
            this.pnlControlesJeu.Controls.Add(this.btnUp);
            this.pnlControlesJeu.Location = new System.Drawing.Point(0, 598);
            this.pnlControlesJeu.Name = "pnlControlesJeu";
            this.pnlControlesJeu.Size = new System.Drawing.Size(699, 144);
            this.pnlControlesJeu.TabIndex = 25;
            this.pnlControlesJeu.Visible = false;
            // 
            // lblPotion
            // 
            this.lblPotion.AutoSize = true;
            this.lblPotion.BackColor = System.Drawing.Color.Transparent;
            this.lblPotion.Location = new System.Drawing.Point(52, 78);
            this.lblPotion.Name = "lblPotion";
            this.lblPotion.Size = new System.Drawing.Size(36, 13);
            this.lblPotion.TabIndex = 44;
            this.lblPotion.Text = "potion";
            this.lblPotion.Visible = false;
            // 
            // lblPokeball
            // 
            this.lblPokeball.AutoSize = true;
            this.lblPokeball.BackColor = System.Drawing.Color.Transparent;
            this.lblPokeball.Location = new System.Drawing.Point(52, 102);
            this.lblPokeball.Name = "lblPokeball";
            this.lblPokeball.Size = new System.Drawing.Size(47, 13);
            this.lblPokeball.TabIndex = 43;
            this.lblPokeball.Text = "pokeball";
            this.lblPokeball.Visible = false;
            // 
            // lblArgent
            // 
            this.lblArgent.AutoSize = true;
            this.lblArgent.BackColor = System.Drawing.Color.Transparent;
            this.lblArgent.Location = new System.Drawing.Point(52, 53);
            this.lblArgent.Name = "lblArgent";
            this.lblArgent.Size = new System.Drawing.Size(37, 13);
            this.lblArgent.TabIndex = 41;
            this.lblArgent.Text = "argent";
            this.lblArgent.Visible = false;
            // 
            // picPokeball
            // 
            this.picPokeball.BackColor = System.Drawing.Color.Transparent;
            this.picPokeball.Image = global::TP2_Pékéman.Properties.Resources.pokeball;
            this.picPokeball.Location = new System.Drawing.Point(28, 98);
            this.picPokeball.Name = "picPokeball";
            this.picPokeball.Size = new System.Drawing.Size(20, 20);
            this.picPokeball.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPokeball.TabIndex = 40;
            this.picPokeball.TabStop = false;
            this.picPokeball.Visible = false;
            // 
            // picPotion
            // 
            this.picPotion.BackColor = System.Drawing.Color.Transparent;
            this.picPotion.Image = global::TP2_Pékéman.Properties.Resources.potion;
            this.picPotion.Location = new System.Drawing.Point(28, 74);
            this.picPotion.Name = "picPotion";
            this.picPotion.Size = new System.Drawing.Size(20, 20);
            this.picPotion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPotion.TabIndex = 39;
            this.picPotion.TabStop = false;
            this.picPotion.Visible = false;
            // 
            // picArgent
            // 
            this.picArgent.BackColor = System.Drawing.Color.Transparent;
            this.picArgent.Image = global::TP2_Pékéman.Properties.Resources.argent;
            this.picArgent.Location = new System.Drawing.Point(28, 49);
            this.picArgent.Name = "picArgent";
            this.picArgent.Size = new System.Drawing.Size(20, 20);
            this.picArgent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picArgent.TabIndex = 38;
            this.picArgent.TabStop = false;
            this.picArgent.Visible = false;
            // 
            // lblNomJoueur
            // 
            this.lblNomJoueur.AutoSize = true;
            this.lblNomJoueur.BackColor = System.Drawing.Color.Transparent;
            this.lblNomJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomJoueur.Location = new System.Drawing.Point(28, 22);
            this.lblNomJoueur.Name = "lblNomJoueur";
            this.lblNomJoueur.Size = new System.Drawing.Size(130, 20);
            this.lblNomJoueur.TabIndex = 31;
            this.lblNomJoueur.Text = "Nom du joueur:";
            // 
            // lblNomText
            // 
            this.lblNomText.AutoSize = true;
            this.lblNomText.BackColor = System.Drawing.Color.Transparent;
            this.lblNomText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomText.Location = new System.Drawing.Point(164, 22);
            this.lblNomText.Name = "lblNomText";
            this.lblNomText.Size = new System.Drawing.Size(40, 20);
            this.lblNomText.TabIndex = 32;
            this.lblNomText.Text = "nom";
            this.lblNomText.Visible = false;
            // 
            // btnPekedex
            // 
            this.btnPekedex.BackColor = System.Drawing.Color.Transparent;
            this.btnPekedex.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnPekedex.Image = global::TP2_Pékéman.Properties.Resources.pokedex_icon;
            this.btnPekedex.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPekedex.Location = new System.Drawing.Point(376, 50);
            this.btnPekedex.Name = "btnPekedex";
            this.btnPekedex.Size = new System.Drawing.Size(121, 49);
            this.btnPekedex.TabIndex = 0;
            this.btnPekedex.Text = "PEKEDEX";
            this.btnPekedex.UseVisualStyleBackColor = false;
            this.btnPekedex.Click += new System.EventHandler(this.btnPekedex_Click);
            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.Color.Transparent;
            this.btnDown.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDown.BackgroundImage")));
            this.btnDown.Location = new System.Drawing.Point(587, 80);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(42, 42);
            this.btnDown.TabIndex = 4;
            this.btnDown.UseVisualStyleBackColor = false;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnRight
            // 
            this.btnRight.BackColor = System.Drawing.Color.Transparent;
            this.btnRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRight.BackgroundImage")));
            this.btnRight.Location = new System.Drawing.Point(628, 57);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(42, 42);
            this.btnRight.TabIndex = 3;
            this.btnRight.UseVisualStyleBackColor = false;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.BackColor = System.Drawing.Color.Transparent;
            this.btnLeft.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLeft.BackgroundImage")));
            this.btnLeft.Location = new System.Drawing.Point(546, 57);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(42, 42);
            this.btnLeft.TabIndex = 1;
            this.btnLeft.UseVisualStyleBackColor = false;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnUp
            // 
            this.btnUp.BackColor = System.Drawing.Color.Transparent;
            this.btnUp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUp.BackgroundImage")));
            this.btnUp.Location = new System.Drawing.Point(587, 34);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(42, 42);
            this.btnUp.TabIndex = 2;
            this.btnUp.UseVisualStyleBackColor = false;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // ctlNomJoueur
            // 
            this.ctlNomJoueur.Location = new System.Drawing.Point(-1, 25);
            this.ctlNomJoueur.Name = "ctlNomJoueur";
            this.ctlNomJoueur.Size = new System.Drawing.Size(700, 615);
            this.ctlNomJoueur.TabIndex = 17;
            this.ctlNomJoueur.Visible = false;
            // 
            // ctlPekecenter1
            // 
            this.ctlPekecenter1.Location = new System.Drawing.Point(-1, 25);
            this.ctlPekecenter1.Name = "ctlPekecenter1";
            this.ctlPekecenter1.Size = new System.Drawing.Size(700, 572);
            this.ctlPekecenter1.TabIndex = 24;
            this.ctlPekecenter1.Visible = false;
            // 
            // ctlPekemart
            // 
            this.ctlPekemart.Location = new System.Drawing.Point(-1, 25);
            this.ctlPekemart.Name = "ctlPekemart";
            this.ctlPekemart.Size = new System.Drawing.Size(701, 572);
            this.ctlPekemart.TabIndex = 22;
            this.ctlPekemart.Visible = false;
            // 
            // logoNentindo1
            // 
            this.logoNentindo1.BackColor = System.Drawing.Color.Transparent;
            this.logoNentindo1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("logoNentindo1.BackgroundImage")));
            this.logoNentindo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoNentindo1.Location = new System.Drawing.Point(0, 25);
            this.logoNentindo1.Name = "logoNentindo1";
            this.logoNentindo1.Size = new System.Drawing.Size(700, 589);
            this.logoNentindo1.TabIndex = 18;
            // 
            // ctlAccueil
            // 
            this.ctlAccueil.BackColor = System.Drawing.Color.White;
            this.ctlAccueil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ctlAccueil.Location = new System.Drawing.Point(-1, 25);
            this.ctlAccueil.Name = "ctlAccueil";
            this.ctlAccueil.Size = new System.Drawing.Size(701, 615);
            this.ctlAccueil.TabIndex = 19;
            this.ctlAccueil.Visible = false;
            // 
            // map
            // 
            this.map.Location = new System.Drawing.Point(-1, 25);
            this.map.Name = "map";
            this.map.PosX = 0;
            this.map.PosY = 0;
            this.map.Size = new System.Drawing.Size(701, 607);
            this.map.TabIndex = 20;
            this.map.Visible = false;
            // 
            // ctlFinDuJeu
            // 
            this.ctlFinDuJeu.Location = new System.Drawing.Point(-1, 0);
            this.ctlFinDuJeu.Name = "ctlFinDuJeu";
            this.ctlFinDuJeu.Size = new System.Drawing.Size(700, 571);
            this.ctlFinDuJeu.TabIndex = 26;
            this.ctlFinDuJeu.Visible = false;
            // 
            // FrmJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(699, 743);
            this.Controls.Add(this.pnlControlesJeu);
            this.Controls.Add(this.mnuPrincipal);
            this.Controls.Add(this.ctlNomJoueur);
            this.Controls.Add(this.ctlPekecenter1);
            this.Controls.Add(this.ctlPekemart);
            this.Controls.Add(this.logoNentindo1);
            this.Controls.Add(this.ctlAccueil);
            this.Controls.Add(this.map);
            this.Controls.Add(this.ctlFinDuJeu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.mnuPrincipal;
            this.MaximizeBox = false;
            this.Name = "FrmJeu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pékéman";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmJeu_FormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmJeu_KeyPress);
            this.mnuPrincipal.ResumeLayout(false);
            this.mnuPrincipal.PerformLayout();
            this.pnlControlesJeu.ResumeLayout(false);
            this.pnlControlesJeu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPokeball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPotion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picArgent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Map map;
        private CtlAccueil ctlAccueil;
        private LogoNentindo logoNentindo1;
        private System.Windows.Forms.Timer tmrDemarrer;
        private System.Windows.Forms.Timer tmrMenu;
        private System.Windows.Forms.Timer tmrNomJoueur;
        public CtlNomJoueur ctlNomJoueur;
        private System.Windows.Forms.Timer tmrPekemart;
        
        private System.Windows.Forms.MenuStrip mnuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouvellePartieToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sauvegardeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrirPartie;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem contrôleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem àProposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pékédexToolStripMenuItem;
        private System.Windows.Forms.Timer tmrPekecenter;
        private CtlPekecenter ctlPekecenter1;
        private System.Windows.Forms.Timer tmrUpdateLabels;
        private System.Windows.Forms.Panel pnlControlesJeu;
        private System.Windows.Forms.Label lblPokeball;
        private System.Windows.Forms.Label lblArgent;
        private System.Windows.Forms.PictureBox picPokeball;
        private System.Windows.Forms.PictureBox picPotion;
        private System.Windows.Forms.PictureBox picArgent;
        private System.Windows.Forms.Label lblNomJoueur;
        private System.Windows.Forms.Label lblNomText;
        private System.Windows.Forms.Button btnPekedex;
        internal System.Windows.Forms.Button btnDown;
        internal System.Windows.Forms.Button btnRight;
        internal System.Windows.Forms.Button btnLeft;
        internal System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Label lblPotion;
        private Controle_Utilisateur.CtlFin ctlFinDuJeu;
    }
}

