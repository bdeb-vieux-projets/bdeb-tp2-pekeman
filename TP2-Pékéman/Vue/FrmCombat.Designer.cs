﻿namespace TP2_Pékéman.Vue
{
    partial class FrmCombat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCombat));
            this.ctlCombat = new CtlCombat();
            this.tmrCombat = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // ctlCombat
            // 
            this.ctlCombat.Location = new System.Drawing.Point(1, 1);
            this.ctlCombat.Name = "ctlCombat";
            this.ctlCombat.Size = new System.Drawing.Size(700, 571);
            this.ctlCombat.TabIndex = 0;
            // 
            // tmrCombat
            // 
            this.tmrCombat.Enabled = true;
            // 
            // FrmCombat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 572);
            this.ControlBox = false;
            this.Controls.Add(this.ctlCombat);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCombat";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Combat";
            this.TopMost = true;
            this.ResumeLayout(false);

        }
     
        private System.ComponentModel.IContainer components = null;
        private CtlCombat ctlCombat;
        private System.Windows.Forms.Timer tmrCombat;
      
    }
}