﻿using System;
using System.IO;
using System.Media;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;

namespace TP2_Pékéman.Vue
{
    public partial class FrmJeu : Form
    {
        public static Noyau jeu = new Noyau();
        private static SoundPlayer _player;
        // TODO ça devrait être un enum EtatPartie
        private static Boolean surMenu = false;
        public static Boolean boutonChargerPartie;
        public static Boolean surEcranNomJoueur;
        public static Boolean estPret;
        public static Boolean demarrer;
        public static Boolean estDevantMart;
        public static Boolean estDevantCenter;
        private readonly SauvegardeRestauration save = new SauvegardeRestauration();
        private CtlPekemart ctlPekemart;
        private FrmPokedex pokedex;

        /// <summary>
        ///     Constructeur de la classe frmJeu qui a comme paramettre le jeu.Joueur
        /// </summary>
        public FrmJeu()
        {
            InitializeComponent();

            pékédexToolStripMenuItem.Enabled = false;
            sauvegardeToolStripMenuItem.Enabled = false;
            ouvrirPartie.Enabled = false;

            var threadLogo = new Thread(AfficherLogo);
            threadLogo.Start();
            tmrMenu.Start();
            tmrNomJoueur.Start();
            tmrDemarrer.Start();
            tmrUpdateLabels.Start();

            btnDown.Visible = false;
            btnUp.Visible = false;
            btnLeft.Visible = false;
            btnRight.Visible = false;
            btnPekedex.Visible = false;
            lblNomJoueur.Visible = false;
            lblNomText.Visible = false;
        }

        /// <summary>
        ///     Procédure qui affiche l'image du debut du jeu
        /// </summary>
        private void AfficherLogo()
        {
            Thread.Sleep(2000);
            BeginInvoke(
                (MethodInvoker) delegate
                {
                    ctlAccueil.Visible = true;
                    JouerMusique();
                    logoNentindo1.Dispose();
                });
        }

        /// <summary>
        ///     Procédure qui initialise le map et change l'etat des boutons
        /// </summary>
        public void DemarrerJeu()
        {
            pékédexToolStripMenuItem.Enabled = true;
            sauvegardeToolStripMenuItem.Enabled = true;
            ouvrirPartie.Enabled = true;
            
            map.ConstruireMap();    
            map.Visible = true;

                 
            ctlAccueil.Visible = false;
            ctlPekemart.Visible = false;
           
            lblNomText.Text = Noyau.LeJoueur.nomJoueur;        
            ActiverControleJeu();
            estPret = true;
        }

        /// <summary>
        ///     Evenement qui verifie si le bouton fermer est pressé
        /// </summary>
        private void frmJeu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                if (!QuitterApplication())
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        ///     Fonction qui demande si on veut quitter le jeu
        /// </summary>
        public static Boolean QuitterApplication()
        {
            DialogResult resultat = MessageBox.Show("Est-ce que vous voulez quitter?", "Quitter",
                MessageBoxButtons.YesNo);
            if (resultat == DialogResult.Yes)
            {
                Application.Exit();
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Evenement lorsque on presse les boutons du clavier pour deplacer le jeu.Joueur
        /// </summary>
        private void frmJeu_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (map.Visible)
            {
                map.DeplacerPerso(e.KeyChar);
            }
            if (estDevantMart)
            {
                ctlPekemart.DeplacerPerso(e.KeyChar);
            }
            if (estDevantCenter)
            {
                ctlPekecenter1.DeplacerPerso(e.KeyChar);
            }
        }

        /// <summary>
        ///     Evenement du bouton UP (flèche)
        /// </summary>
        private void btnUp_Click(object sender, EventArgs e)
        {
            map.DeplacerPerso('w');
        }

        /// <summary>
        ///     Evenement du bouton left (flèche)
        /// </summary>
        private void btnLeft_Click(object sender, EventArgs e)
        {
            map.DeplacerPerso('a');
        }

        /// <summary>
        ///     Evenement du bouton right (flèche)
        /// </summary>
        private void btnRight_Click(object sender, EventArgs e)
        {
            map.DeplacerPerso('d');
        }

        /// <summary>
        ///     Evenement du bouton down (flèche)
        /// </summary>
        private void btnDown_Click(object sender, EventArgs e)
        {
            map.DeplacerPerso('s');
        }

        /// <summary>
        ///     Evenement du bouton pekedex (ouvre la fenetre pekedex)
        /// </summary>
        private void btnPekedex_Click(object sender, EventArgs e)
        {
            pokedex = new FrmPokedex();
            pokedex.ChargerPokedex();
            pokedex.ShowDialog();
        }

        /// <summary>
        ///     Methode qui fait jouer la musique du jeu
        /// </summary>
        public static void JouerMusique()
        {
            Stream str;
            if (surMenu)
            {
                _player.Stop();
                str = Resources.menu_screen;
            }
            else
            {
                str = Resources.title_screen;
            }
            _player = new SoundPlayer(str);
            _player.PlayLooping();
        }

        private void tmrMenu_Tick(object sender, EventArgs e)
        {
            if (surMenu)
            {
                pnlControlesJeu.Visible = true;
                ctlAccueil.Visible = false;
                tmrMenu.Stop();
                tmrNomJoueur.Start();
            }
        }

        private void tmrNomJoueur_Tick(object sender, EventArgs e)
        {
            if (surEcranNomJoueur)
            {
                ctlNomJoueur.Visible = true;
                tmrNomJoueur.Stop();
                tmrDemarrer.Start();
            }
            else if (boutonChargerPartie)
            {
                tmrNomJoueur.Stop();

                if (ChargerPartie())
                {
                    ctlNomJoueur.Dispose();
                    ctlAccueil.Visible = false;
                    tmrDemarrer.Start();
                    demarrer = true;
                }
                else
                {
                    boutonChargerPartie = false;
                    tmrNomJoueur.Start();
                }
            }
        }

        /// <summary>
        ///     timer qui attend que le bouton demarrer soit pressé
        /// </summary>
        private void tmrDemarrer_Tick(object sender, EventArgs e)
        {
            if (demarrer)
            {
                if (ctlNomJoueur.txtName.Text != "")
                    Noyau.LeJoueur.nomJoueur = ctlNomJoueur.txtName.Text;
                ActiverControleJeu();
                ctlNomJoueur.Visible = false;
                _player.Stop();
                tmrDemarrer.Stop();
                tmrPekemart.Start();
                DemarrerJeu();
            }
        }

        private void ActiverControleJeu()
        {
            btnDown.Visible = true;
            btnLeft.Visible = true;
            btnUp.Visible = true;
            btnRight.Visible = true;
            btnPekedex.Visible = true;
            pnlControlesJeu.Visible = true;
            lblArgent.Visible = true;
            lblPokeball.Visible = true;
            lblPotion.Visible = true;
            lblNomJoueur.Visible = true;
            lblNomText.Visible = true;
            picArgent.Visible = true;
            picPokeball.Visible = true;
            picPotion.Visible = true;
        }

        private void DesactiverControleJeu()
        {
            btnDown.Visible = false;
            btnLeft.Visible = false;
            btnUp.Visible = false;
            btnRight.Visible = false;
            btnPekedex.Visible = false;
            pnlControlesJeu.Visible = false;
            lblArgent.Visible = false;
            lblPokeball.Visible = false;
            lblPotion.Visible = false;
            lblNomJoueur.Visible = false;
            lblNomText.Visible = false;
            picArgent.Visible = false;
            picPokeball.Visible = false;
            picPotion.Visible = false;
        }

        private void tmrPekemart_Tick(object sender, EventArgs e)
        {
            if (estDevantMart)
            {
                sauvegardeToolStripMenuItem.Enabled = false;
                ouvrirPartie.Enabled = false;

                map.Visible = false;
                ctlPekemart.Visible = true;
                //ctlPekemart.ctnHeros1.Enabled = false;
                foreach (UserControl c in map.Controls)
                {
                    c.Enabled = false;
                }
            }
            else
            {
                map.Visible = true;
                ctlPekemart.Visible = false;
            }
        }

        private void tmrPekecenter_Tick(object sender, EventArgs e)
        {
            if (estDevantCenter)
            {
                sauvegardeToolStripMenuItem.Enabled = false;
                ouvrirPartie.Enabled = false;

                map.Visible = false;
                ctlPekecenter1.Visible = true;
                map.ctnHeros1.Enabled = false;
            }
        }

        private void tmrUpdateLabels_Tick(object sender, EventArgs e)
        {
            UpdateItemsQty();
            // Vérifie si le jeux est terminé
            if (Noyau.LeJoueur.FinDuJeu())
            {
                sauvegardeToolStripMenuItem.Enabled = false;
                ouvrirPartie.Enabled = false;
                _player.Play();
                tmrUpdateLabels.Stop();
                map.Visible = false;
                DesactiverControleJeu();
                ctlFinDuJeu.Visible = true;
            }
        }

        public void UpdateItemsQty()
        {
            lblArgent.Text = Noyau.LeJoueur.Argent.ToString();
            lblPokeball.Text = Noyau.LeJoueur.NbPokeballs.ToString();
            lblPotion.Text = Noyau.LeJoueur.NbPotions.ToString();
        }

        /// <summary>
        ///     Lorsque l'utilisateur clique sur l'option Quitter du menu
        ///     Ouvre une fenêtre de confirmation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuitterApplication();
        }

        /// <summary>
        ///     Lorsque l'utilisateur clique sur l'option Pékédex du menu
        ///     Ouvre une nouvelle fenêtre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pékédexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pokedex = new FrmPokedex();
            pokedex.ChargerPokedex();
            pokedex.ShowDialog();
        }

        private void nouvellePartieToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        ///     Lorsque l'utilisateur clique sur l'option Ouvrir du menu
        ///     Ouvre une fenêtre de dialog qui permet d'ouvrir un fichier de sauvegarde *.pkm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ouvrirPartie_Click_1(object sender, EventArgs e)
        {
            ChargerPartie();
        }

        private Boolean ChargerPartie()
        {
            var ouvrirPartie = new OpenFileDialog();

            ouvrirPartie.Filter = "Fichier Pékéman (*.pkm)|*pkm";
            ouvrirPartie.Title = "Ouvrir un fichier de sauvegarde";
            ouvrirPartie.ShowDialog();

            if (ouvrirPartie.FileName != "")
            {
                var fs = (FileStream) ouvrirPartie.OpenFile();
                save.Restauration(jeu, fs);
                RestaurationMiseAJour();
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Lorsque l'utilisateur clique sur l'option sauvegarde du menu
        ///     Ouvre une fenêtre SaveFileDialog qui permet à l'utilisateur de nommer
        ///     et de choisir l'emplacement de sa sauvegarde
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sauvegardeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            var savePartie = new SaveFileDialog();

            savePartie.Filter = "Fichier de sauvegarde Pékéman (*.pkm)|*pkm";
            savePartie.Title = "Enregistrer la partie";
            savePartie.ShowDialog();

            // Sauvegarde la partie dans le fichier choisi par l'utilisateur
            if (savePartie.FileName != "")
            {
                // Vérifie l'extension du fichier
                if (!ValiderExtensionFichier(savePartie.FileName))
                {
                    savePartie.FileName += ".pkm";
                }

                // Ouvre le fichier et l'envoie à la sauvegarde
                var fs = (FileStream) savePartie.OpenFile();
                save.Sauvegarde(jeu, fs);
            }
        }

        /// <summary>
        ///     Lorsque l'utilisateur clique sur le menu À propos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void àProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult resultat = MessageBox.Show(
                "Travail réalisé dans le cadre du cours Développement graphique \n" +
                "Dominique Cright, Christelle Sissoko, Jean Joshua Ghali\n" +
                "Version 0.2 - 2014",
                "À propos",
                MessageBoxButtons.OK);
        }

        /// <summary>
        ///     Lorsque l'utilisateur clique sur le menu Contrôle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void contrôleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult resultat = MessageBox.Show(
                "Pour faire déplacer le personnage utilise les touches : \n" +
                "\t W pour monter\n" +
                "\t S pour descendre\n" +
                "\t D pour aller à droite\n" +
                "\t A pour aller à gauche\n" +
                "De plus, les flèches du clavier et à l'écran fonctionnent",
                "Contrôle",
                MessageBoxButtons.OK);
        }

        /// <summary>
        ///     Fonction qui nous dit si le nom du fichier contient l'extension des fichiers de sauvegarde
        ///     c'est-à-dire *.pkm
        /// </summary>
        /// <param name="nomFichier">Le nom du fichier</param>
        /// <returns>true si l'extension correspond</returns>
        private static bool ValiderExtensionFichier(string nomFichier)
        {
            var myRegex = new Regex(".pkm$");

            return myRegex.IsMatch(nomFichier); // retourne true ou false selon la vérification
        }

        /// <summary>
        ///     Procédure qui met la vue à jour lors d'une restauration
        /// </summary>
        private void RestaurationMiseAJour()
        {
            lblNomText.Text = Noyau.LeJoueur.nomJoueur;
            lblArgent.Text = Noyau.LeJoueur.Argent.ToString();
            lblPokeball.Text = Noyau.LeJoueur.NbPokeballs.ToString();
            lblPotion.Text = Noyau.LeJoueur.NbPotions.ToString();
            map.PosY = Noyau.LeJoueur.posY - 9;
            map.PosX = Noyau.LeJoueur.posX - 11;
            map.Refresh();
        }
    }
}