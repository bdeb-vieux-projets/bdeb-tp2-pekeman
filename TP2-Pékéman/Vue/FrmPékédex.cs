﻿#region

using System;
using System.Linq;
using System.Windows.Forms;
using TP2_Pékéman.Modele;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class FrmPékédex : Form
    {
        // variables qui ont les descriptions de chaque Pekeman.

        private String DescriptionBulba =
            "Au matin de sa vie, la graine sur son dos lui fournit les éléments dont il a besoin pour grandir.";

        private String DescriptionChar = "La flamme de sa queue symbolise sa vitalité quand il est en bonne santé.";

        private String DescriptionChik =
            "Il jauge la température et l’humidité grâce à la feuille sur sa tête. Il raffole des bains de soleil.";

        private String DescriptionPid = "Ce Pekeman docile préfère éviter le combat. Toutefois," +
                                        " il se montre très féroce quand on l’agresse.";

        private String DescriptionPika = "Il lui arrive de remettre d’aplomb un pikachu allié en" +
                                         " lui envoyant une décharge électrique.";

        private String DescriptionSquir =
            "Il se réfugie dans sa carapace et réplique en éclaboussant l’ennemi à la première occasion.";

        /// <summary>
        ///     Constructeur de la classe FrmPekedex
        /// </summary>
        public FrmPékédex(Joueur joueur)
        {
            InitializeComponent();
            InitialiserCmbList(joueur);
        }

        /// <summary>
        ///     methode qui initialise la list du combobox avec les Nom des Pekeman
        /// </summary>
        private void InitialiserCmbList(Joueur joueur)
        {
            // on repete pas les pokemon repetes dans la liste
            foreach (Pekeman val in joueur.pekedexList.Distinct())
            {
                cmbList.Items.Add(val.Nom);
            }
        }

        /// <summary>
        ///     Evenement de la liste de Pekeman
        /// </summary>
        private void cmbList_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbList.Text)
            {
                case "PIKACHU":
                    lblTxtCapacites.Text = "Statik";
                    lblTxtFaib1.Text = "Sol";
                    lblNomPekeman.Text = "Pikachu";
                    lblTxtType.Text = TypePekeman.ELECTRICITE.ToString();
                    picPekeman.BackgroundImage = Resources.pika;
                    rtxtBoxPeke.Text = DescriptionPika;
                    break;
                case "CHARMANDER":
                    lblTxtCapacites.Text = "Brasier";
                    lblTxtFaib1.Text = "Eau, Sol, Roche";
                    lblNomPekeman.Text = "Charmander";
                    lblTxtType.Text = TypePekeman.FEU.ToString();
                    picPekeman.BackgroundImage = Resources.charmanderFront;
                    rtxtBoxPeke.Text = DescriptionChar;
                    break;
                case "BULBASAUR":
                    lblTxtCapacites.Text = "Engrais";
                    lblTxtFaib1.Text = "Feu, Vol, Glace, Psy";
                    lblNomPekeman.Text = "Bulbasaur";
                    lblTxtType.Text = TypePekeman.PLANTE + ", POISON";
                    picPekeman.BackgroundImage = Resources.bulb;
                    rtxtBoxPeke.Text = DescriptionBulba;
                    break;
                case "CHIKORITA":
                    lblTxtCapacites.Text = "Engrais";
                    lblTxtFaib1.Text = "Insecte, Feu, Vol, Glace, Poison";
                    lblNomPekeman.Text = "Chikorita";
                    lblTxtType.Text = TypePekeman.PLANTE.ToString();
                    picPekeman.BackgroundImage = Resources.chik;
                    rtxtBoxPeke.Text = DescriptionChik;
                    break;
                case "SQUIRTLE":
                    lblTxtCapacites.Text = "Torrent";
                    lblTxtFaib1.Text = "Electrik, Plante";
                    lblNomPekeman.Text = "Squirtle";
                    lblTxtType.Text = TypePekeman.EAU.ToString();
                    picPekeman.BackgroundImage = Resources.squir;
                    rtxtBoxPeke.Text = DescriptionSquir;
                    break;
                case "PIDGEY":
                    lblTxtCapacites.Text = "Pieds Confus, Regard Vif";
                    lblTxtFaib1.Text = "Electrik, Glace, Roche";
                    lblNomPekeman.Text = "Pidgey";
                    lblTxtType.Text = TypePekeman.NORMALE + ", VOL";
                    picPekeman.BackgroundImage = Resources.pid;
                    rtxtBoxPeke.Text = DescriptionPid;
                    break;
            }
        }
    }
}