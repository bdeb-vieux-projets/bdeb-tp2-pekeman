﻿#region

using System;
using System.Drawing;

#endregion

namespace TP2_Pékéman.Vue.Tuiles
{
    public class Tuile
    {
        // variable qui sert a savoir si la case est accesible pour le heros
        private readonly Boolean _accessible;
        private readonly Image _image;
        // variable qui sert a savoir si la case correspond a la zone de bataille
        public bool combatPossible;
        // variable qui sert a savoir si la case est une porte de market
        public bool marketTrouve;
        public bool centerTrouve;

        /// <summary>
        ///     Constructeur de la classe Tuile
        /// </summary>
        public Tuile(Boolean accessible, Boolean combatPossible, Boolean marketTrouve,
            Boolean centerTrouve, Image image)
        {
            _accessible = accessible;
            this.combatPossible = combatPossible;
            this.marketTrouve = marketTrouve;
            this.centerTrouve = centerTrouve;
            _image = image;
        }

        public Image image
        {
            get { return _image; }
        }

        public Boolean Accessible
        {
            get { return _accessible; }
        }
    }
}