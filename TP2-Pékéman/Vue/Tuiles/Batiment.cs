﻿#region

using System;
using System.Drawing;

#endregion

namespace TP2_Pékéman.Vue.Tuiles
{
    internal class Batiment : Tuile
    {
        /// <summary>
        ///     Constructeur de la classe Batiment
        /// </summary>
        public Batiment(Image image, Boolean accessible) : base(accessible, false, false, false, image)
        {
        }
    }
}