﻿#region

using System;
using System.Drawing;

#endregion

namespace TP2_Pékéman.Vue.Tuiles
{
    internal class Surface : Tuile
    {
        /// <summary>
        ///     Constructeur de la classe Surface
        /// </summary>
        public Surface(Image image, Boolean accessible) : base(accessible, false, false, false, image)
        {
        }
    }
}