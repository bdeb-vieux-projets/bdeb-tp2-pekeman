﻿#region

using System;
using System.Drawing;

#endregion

namespace TP2_Pékéman.Vue.Tuiles
{
    internal class Pancarte : Tuile
    {
        /// <summary>
        ///     Constructeur de la classe Pancarte
        /// </summary>
        public Pancarte(Image image, Boolean market, Boolean center) : base(false, false, market, center, image)
        {
        }
    }
}