﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TP2_Pékéman.Vue.Tuiles
{
    internal class Ordinateur : Tuile
    {
        public Ordinateur(Image image, Boolean accessible) : base(accessible, false, false, false, image)
        {
        }
    }
}
