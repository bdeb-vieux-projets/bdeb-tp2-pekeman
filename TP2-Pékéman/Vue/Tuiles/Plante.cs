﻿#region

using System;
using System.Drawing;

#endregion

namespace TP2_Pékéman.Vue.Tuiles
{
    internal class Plante : Tuile
    {
        /// <summary>
        ///     Constructeur de la classe Plante
        /// </summary>
        public Plante(Image image, Boolean accessible, Boolean combatPossible)
            : base(accessible, combatPossible, false, false, image)
        {
        }
    }
}