﻿#region

using System.Drawing;

#endregion

namespace TP2_Pékéman.Vue.Tuiles
{
    internal class Arbre : Tuile
    {
        /// <summary>
        ///     Constructeur de la classe Arbre
        /// </summary>
        public Arbre(Image image) : base(false,false, false, false, image)
        {
        }
    }
}