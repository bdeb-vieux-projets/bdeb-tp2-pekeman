﻿namespace TP2_Pékéman.Vue
{
    partial class FrmPékédex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPékédex));
            this.cmbList = new System.Windows.Forms.ComboBox();
            this.lblTxtFaib1 = new System.Windows.Forms.Label();
            this.lblFaiblesses = new System.Windows.Forms.Label();
            this.lblTxtCapacites = new System.Windows.Forms.Label();
            this.lblCapacites = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblTxtType = new System.Windows.Forms.Label();
            this.rtxtBoxPeke = new System.Windows.Forms.RichTextBox();
            this.lblNomPekeman = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.picPekeman = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picBoxPocketball = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxPocketball)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbList
            // 
            this.cmbList.FormattingEnabled = true;
            this.cmbList.Location = new System.Drawing.Point(12, 125);
            this.cmbList.Name = "cmbList";
            this.cmbList.Size = new System.Drawing.Size(165, 21);
            this.cmbList.TabIndex = 0;
            this.cmbList.Text = "CHOISIT TON POKEMON!";
            this.cmbList.SelectedIndexChanged += new System.EventHandler(this.cmbList_SelectedIndexChanged);
            // 
            // lblTxtFaib1
            // 
            this.lblTxtFaib1.AutoSize = true;
            this.lblTxtFaib1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.lblTxtFaib1.Location = new System.Drawing.Point(152, 285);
            this.lblTxtFaib1.Name = "lblTxtFaib1";
            this.lblTxtFaib1.Size = new System.Drawing.Size(0, 26);
            this.lblTxtFaib1.TabIndex = 12;
            // 
            // lblFaiblesses
            // 
            this.lblFaiblesses.AutoSize = true;
            this.lblFaiblesses.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaiblesses.Location = new System.Drawing.Point(24, 286);
            this.lblFaiblesses.Name = "lblFaiblesses";
            this.lblFaiblesses.Size = new System.Drawing.Size(133, 25);
            this.lblFaiblesses.TabIndex = 5;
            this.lblFaiblesses.Text = "Faiblesses:";
            // 
            // lblTxtCapacites
            // 
            this.lblTxtCapacites.AutoSize = true;
            this.lblTxtCapacites.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.lblTxtCapacites.Location = new System.Drawing.Point(152, 243);
            this.lblTxtCapacites.Name = "lblTxtCapacites";
            this.lblTxtCapacites.Size = new System.Drawing.Size(0, 26);
            this.lblTxtCapacites.TabIndex = 10;
            // 
            // lblCapacites
            // 
            this.lblCapacites.AutoSize = true;
            this.lblCapacites.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCapacites.Location = new System.Drawing.Point(25, 244);
            this.lblCapacites.Name = "lblCapacites";
            this.lblCapacites.Size = new System.Drawing.Size(124, 25);
            this.lblCapacites.TabIndex = 8;
            this.lblCapacites.Text = "Capacités:";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.Location = new System.Drawing.Point(25, 203);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(78, 25);
            this.lblType.TabIndex = 7;
            this.lblType.Text = "Type :";
            // 
            // lblTxtType
            // 
            this.lblTxtType.AutoSize = true;
            this.lblTxtType.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.lblTxtType.Location = new System.Drawing.Point(152, 202);
            this.lblTxtType.Name = "lblTxtType";
            this.lblTxtType.Size = new System.Drawing.Size(0, 26);
            this.lblTxtType.TabIndex = 9;
            // 
            // rtxtBoxPeke
            // 
            this.rtxtBoxPeke.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtBoxPeke.Location = new System.Drawing.Point(229, 64);
            this.rtxtBoxPeke.Name = "rtxtBoxPeke";
            this.rtxtBoxPeke.Size = new System.Drawing.Size(199, 116);
            this.rtxtBoxPeke.TabIndex = 15;
            this.rtxtBoxPeke.Text = "";
            // 
            // lblNomPekeman
            // 
            this.lblNomPekeman.AutoSize = true;
            this.lblNomPekeman.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomPekeman.Location = new System.Drawing.Point(246, 17);
            this.lblNomPekeman.Name = "lblNomPekeman";
            this.lblNomPekeman.Size = new System.Drawing.Size(0, 37);
            this.lblNomPekeman.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblNomPekeman);
            this.groupBox1.Controls.Add(this.rtxtBoxPeke);
            this.groupBox1.Controls.Add(this.picPekeman);
            this.groupBox1.Controls.Add(this.lblTxtFaib1);
            this.groupBox1.Controls.Add(this.lblType);
            this.groupBox1.Controls.Add(this.lblTxtCapacites);
            this.groupBox1.Controls.Add(this.lblFaiblesses);
            this.groupBox1.Controls.Add(this.lblCapacites);
            this.groupBox1.Controls.Add(this.lblTxtType);
            this.groupBox1.Location = new System.Drawing.Point(245, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(495, 350);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pekedex";
            // 
            // picPekeman
            // 
            this.picPekeman.Location = new System.Drawing.Point(34, 30);
            this.picPekeman.Name = "picPekeman";
            this.picPekeman.Size = new System.Drawing.Size(150, 150);
            this.picPekeman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picPekeman.TabIndex = 3;
            this.picPekeman.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(208, 78);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // picBoxPocketball
            // 
            this.picBoxPocketball.Image = ((System.Drawing.Image)(resources.GetObject("picBoxPocketball.Image")));
            this.picBoxPocketball.Location = new System.Drawing.Point(68, 250);
            this.picBoxPocketball.Name = "picBoxPocketball";
            this.picBoxPocketball.Size = new System.Drawing.Size(100, 100);
            this.picBoxPocketball.TabIndex = 16;
            this.picBoxPocketball.TabStop = false;
            // 
            // FrmPékédex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(772, 398);
            this.Controls.Add(this.picBoxPocketball);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cmbList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmPékédex";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pékédex";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPekeman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxPocketball)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbList;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblTxtFaib1;
        private System.Windows.Forms.Label lblFaiblesses;
        private System.Windows.Forms.Label lblTxtCapacites;
        private System.Windows.Forms.Label lblCapacites;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblTxtType;
        private System.Windows.Forms.RichTextBox rtxtBoxPeke;
        private System.Windows.Forms.Label lblNomPekeman;
        private System.Windows.Forms.PictureBox picPekeman;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox picBoxPocketball;
    }
}