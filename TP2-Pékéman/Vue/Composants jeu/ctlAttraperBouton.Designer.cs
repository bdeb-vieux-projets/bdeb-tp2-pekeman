﻿namespace TP2_Pékéman.Vue.Composants_jeu
{
    partial class CtlAttraperBouton
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CtlAttraperBouton
            // 
            this.BackgroundImage = global::TP2_Pékéman.Properties.Resources.btnAttraper;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ResumeLayout(false);

        }

        #endregion
    }
}
