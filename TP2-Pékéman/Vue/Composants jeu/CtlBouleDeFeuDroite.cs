﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TP2_Pékéman.Vue.Composants_jeu
{
    class CtlBouleDeFeuDroite:Control
    {
        public CtlBouleDeFeuDroite()
        {
            InitializeComponent();
        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CtlBouleDeFeuGauche
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CtlBouleDeFeu_Paint);
            this.ResumeLayout(false);

        }

        private void CtlBouleDeFeu_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(Properties.Resources.fireball_droite,0,0);
            g.Dispose();
        }
    }
}
