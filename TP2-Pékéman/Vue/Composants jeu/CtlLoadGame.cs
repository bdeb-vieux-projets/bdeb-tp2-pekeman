﻿#region

using System.Windows.Forms;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public class CtlLoadGame : Control
    {
        public CtlLoadGame()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            SuspendLayout();
            // 
            // CtlLoadGame
            // 
            BackgroundImage = Resources.loadGame;
            BackgroundImageLayout = ImageLayout.Stretch;
            ResumeLayout(false);
        }
    }
}