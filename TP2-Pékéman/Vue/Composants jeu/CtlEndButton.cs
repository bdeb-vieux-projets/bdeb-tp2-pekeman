﻿#region

using System.Windows.Forms;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class CtlEndButton : Control
    {
        public CtlEndButton()
        {
            InitializeComponent();
            BackgroundImage = Resources.end_button;
            BackgroundImageLayout = ImageLayout.Stretch;
        }
    }
}