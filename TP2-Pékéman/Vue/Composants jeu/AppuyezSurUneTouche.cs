﻿#region

using System.Drawing;
using System.Windows.Forms;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    internal class AppuyezSurUneTouche : Panel
    {
        public AppuyezSurUneTouche()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            SuspendLayout();
            // 
            // AppuyezSurUneTouche
            // 
            BackColor = Color.Transparent;
            Paint += AppuyezSurUneTouche_Paint;
            ResumeLayout(false);
        }

        private void AppuyezSurUneTouche_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(Resources.appuyezSurUneTouche, 0, 0);
            g.Dispose();
        }
    }
}