﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TP2_Pékéman.Vue.Composants_jeu
{
    public class CtlTypeAttaque:Control
    {
        private String _nomAttaque;
        public CtlTypeAttaque()
        {
            _nomAttaque = "TACKLE";
            InitializeComponent();
        }
        public CtlTypeAttaque(String nomAttaque)
        {
            _nomAttaque = nomAttaque;
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CtlTypeAttaque
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CtlTypeAttaque_Paint);
            this.ResumeLayout(false);

        }

        private void CtlTypeAttaque_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawString(_nomAttaque,new Font("Arial",16), Brushes.Black, 0,0);
            g.Dispose();
        }
    }
}
