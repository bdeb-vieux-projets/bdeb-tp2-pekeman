﻿#region

using System.Windows.Forms;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public class CtlNewGame : Control
    {
        public CtlNewGame()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            SuspendLayout();
            BackgroundImage = Resources.newGameButton;
            BackgroundImageLayout = ImageLayout.Stretch;
            // 
            // ctlNewGame
            // 
            //this.Paint += new System.Windows.Forms.PaintEventHandler(this.ctlNewGame_Paint);
            ResumeLayout(false);
        }

        //private void ctlNewGame_Paint(object sender, PaintEventArgs e)
        //{
        //    Graphics g = e.Graphics;
        //    g.DrawImage(Properties.Resources.newGameButton,0,0);
        //    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
        //    g.Dispose();
        //}
    }
}