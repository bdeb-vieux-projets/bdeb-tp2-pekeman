﻿#region

using System.ComponentModel;
using System.Windows.Forms;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class CtlCursor : Control
    {
        public CtlCursor()
        {
            InitializeComponent();
        }

        public CtlCursor(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            BackgroundImage = Resources.cursor;
            BackgroundImageLayout = ImageLayout.Stretch;
        }
    }
}