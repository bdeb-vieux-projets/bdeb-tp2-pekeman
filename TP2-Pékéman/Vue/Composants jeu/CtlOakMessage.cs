﻿#region

using System.Windows.Forms;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class CtlOakMessage : Panel
    {
        public CtlOakMessage()
        {
            InitializeComponent();
            BackgroundImage = Resources.name_box;
            BackgroundImageLayout = ImageLayout.Stretch;
        }
    }
}