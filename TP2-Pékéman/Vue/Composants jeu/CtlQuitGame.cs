﻿#region

using System.Windows.Forms;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public class CtlQuitGame : Control
    {
        public CtlQuitGame()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            SuspendLayout();
            BackgroundImage = Resources.quitGameButton;
            BackgroundImageLayout = ImageLayout.Stretch;
            // 
            // CtlQuitGame
            // 
            ResumeLayout(false);
        }
    }
}