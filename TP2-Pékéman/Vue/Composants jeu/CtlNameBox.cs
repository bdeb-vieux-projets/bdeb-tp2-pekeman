﻿#region

using System.Windows.Forms;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Vue
{
    public partial class CtlNameBox : Panel
    {
        public CtlNameBox()
        {
            InitializeComponent();
            BackgroundImage = Resources.name_choice_box;
            BackgroundImageLayout = ImageLayout.Stretch;
        }
    }
}