﻿namespace TP2_Pékéman.Vue
{
    partial class FrmPokedex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPokedex));
            this.ctlPokedex = new TP2_Pékéman.Vue.CtlPokedex();
            this.SuspendLayout();
            // 
            // ctlPokedex
            // 
            this.ctlPokedex.BackColor = System.Drawing.Color.White;
            this.ctlPokedex.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ctlPokedex.BackgroundImage")));
            this.ctlPokedex.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ctlPokedex.Location = new System.Drawing.Point(0, 0);
            this.ctlPokedex.Name = "ctlPokedex";
            this.ctlPokedex.Size = new System.Drawing.Size(700, 590);
            this.ctlPokedex.TabIndex = 0;
            // 
            // FrmPokedex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 591);
            this.Controls.Add(this.ctlPokedex);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPokedex";
            this.Text = "Pokédex";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPokedex_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private CtlPokedex ctlPokedex;
    }
}