﻿using TP2_Pékéman.Modele;

namespace TP2_Pékéman
{
    partial class FrmAchatPekemart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pctBPekeball = new System.Windows.Forms.PictureBox();
            this.pctBDreamPotion = new System.Windows.Forms.PictureBox();
            this.lblDescriptionItem = new System.Windows.Forms.Label();
            this.nUDPokeball = new System.Windows.Forms.NumericUpDown();
            this.nUDPotion = new System.Windows.Forms.NumericUpDown();
            this.lblPrixPotion = new System.Windows.Forms.Label();
            this.lblmutliply = new System.Windows.Forms.Label();
            this.lblPrixPokeball = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblMontantArgentJoueur = new System.Windows.Forms.Label();
            this.lblArgentJoueur = new System.Windows.Forms.Label();
            this.lblNombrePotions = new System.Windows.Forms.Label();
            this.lblNombrePokeball = new System.Windows.Forms.Label();
            this.lblPotion = new System.Windows.Forms.Label();
            this.lblPokeball = new System.Windows.Forms.Label();
            this.grbBDescription = new System.Windows.Forms.GroupBox();
            this.btnAcheter = new System.Windows.Forms.Button();
            this.btnVendre = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblMontantTotal = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pctBPekeball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctBDreamPotion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDPokeball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDPotion)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.grbBDescription.SuspendLayout();
            this.SuspendLayout();
            // 
            // pctBPekeball
            // 
            this.pctBPekeball.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctBPekeball.Image = global::TP2_Pékéman.Properties.Resources.pokeball_hd;
            this.pctBPekeball.Location = new System.Drawing.Point(9, 25);
            this.pctBPekeball.Name = "pctBPekeball";
            this.pctBPekeball.Size = new System.Drawing.Size(154, 147);
            this.pctBPekeball.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctBPekeball.TabIndex = 0;
            this.pctBPekeball.TabStop = false;
            this.pctBPekeball.MouseLeave += new System.EventHandler(this.pctBPekeball_MouseLeave);
            this.pctBPekeball.MouseHover += new System.EventHandler(this.pctBPekeball_MouseHover);
            // 
            // pctBDreamPotion
            // 
            this.pctBDreamPotion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctBDreamPotion.Image = global::TP2_Pékéman.Properties.Resources.Dream_Potion_Sprite;
            this.pctBDreamPotion.Location = new System.Drawing.Point(9, 200);
            this.pctBDreamPotion.Name = "pctBDreamPotion";
            this.pctBDreamPotion.Size = new System.Drawing.Size(154, 147);
            this.pctBDreamPotion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctBDreamPotion.TabIndex = 1;
            this.pctBDreamPotion.TabStop = false;
            this.pctBDreamPotion.MouseLeave += new System.EventHandler(this.pctBDreamPotion_MouseLeave);
            this.pctBDreamPotion.MouseHover += new System.EventHandler(this.pctBDreamPotion_MouseHover);
            // 
            // lblDescriptionItem
            // 
            this.lblDescriptionItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionItem.Location = new System.Drawing.Point(6, 16);
            this.lblDescriptionItem.Name = "lblDescriptionItem";
            this.lblDescriptionItem.Size = new System.Drawing.Size(185, 131);
            this.lblDescriptionItem.TabIndex = 2;
            this.lblDescriptionItem.Text = "Les Pokéballs ou Balls sont des objets essentiels à la tâche d\'un dresseur souhai" +
                "tant compléter son Pokédex, lui permettant de capturer les Pokémon sauvages qu\'i" +
                "l rencontre.\r\nValeur : 100 \r\n";
            this.lblDescriptionItem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDescriptionItem.Visible = false;
            // 
            // nUDPokeball
            // 
            this.nUDPokeball.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nUDPokeball.Location = new System.Drawing.Point(169, 99);
            this.nUDPokeball.Name = "nUDPokeball";
            this.nUDPokeball.Size = new System.Drawing.Size(62, 22);
            this.nUDPokeball.TabIndex = 0;
            this.nUDPokeball.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nUDPokeball.ValueChanged += new System.EventHandler(this.nUDPokeball_ValueChanged);
            // 
            // nUDPotion
            // 
            this.nUDPotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nUDPotion.Location = new System.Drawing.Point(169, 272);
            this.nUDPotion.Name = "nUDPotion";
            this.nUDPotion.Size = new System.Drawing.Size(62, 22);
            this.nUDPotion.TabIndex = 1;
            this.nUDPotion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nUDPotion.ValueChanged += new System.EventHandler(this.nUDPotion_ValueChanged);
            // 
            // lblPrixPotion
            // 
            this.lblPrixPotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrixPotion.Location = new System.Drawing.Point(239, 272);
            this.lblPrixPotion.Name = "lblPrixPotion";
            this.lblPrixPotion.Size = new System.Drawing.Size(56, 23);
            this.lblPrixPotion.TabIndex = 7;
            this.lblPrixPotion.Text = "X 150";
            this.lblPrixPotion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmutliply
            // 
            this.lblmutliply.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmutliply.Location = new System.Drawing.Point(237, 100);
            this.lblmutliply.Name = "lblmutliply";
            this.lblmutliply.Size = new System.Drawing.Size(0, 0);
            this.lblmutliply.TabIndex = 8;
            this.lblmutliply.Text = "X";
            this.lblmutliply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPrixPokeball
            // 
            this.lblPrixPokeball.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrixPokeball.Location = new System.Drawing.Point(237, 98);
            this.lblPrixPokeball.Name = "lblPrixPokeball";
            this.lblPrixPokeball.Size = new System.Drawing.Size(65, 23);
            this.lblPrixPokeball.TabIndex = 9;
            this.lblPrixPokeball.Text = "X 100";
            this.lblPrixPokeball.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblMontantArgentJoueur);
            this.groupBox1.Controls.Add(this.lblArgentJoueur);
            this.groupBox1.Controls.Add(this.lblNombrePotions);
            this.groupBox1.Controls.Add(this.lblNombrePokeball);
            this.groupBox1.Controls.Add(this.lblPotion);
            this.groupBox1.Controls.Add(this.lblPokeball);
            this.groupBox1.Location = new System.Drawing.Point(373, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 147);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inventaire";
            // 
            // lblMontantArgentJoueur
            // 
            this.lblMontantArgentJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontantArgentJoueur.Location = new System.Drawing.Point(132, 116);
            this.lblMontantArgentJoueur.Name = "lblMontantArgentJoueur";
            this.lblMontantArgentJoueur.Size = new System.Drawing.Size(59, 23);
            this.lblMontantArgentJoueur.TabIndex = 5;
            this.lblMontantArgentJoueur.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblArgentJoueur
            // 
            this.lblArgentJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArgentJoueur.Location = new System.Drawing.Point(6, 121);
            this.lblArgentJoueur.Name = "lblArgentJoueur";
            this.lblArgentJoueur.Size = new System.Drawing.Size(120, 23);
            this.lblArgentJoueur.TabIndex = 4;
            this.lblArgentJoueur.Text = "Porte-monnaie: ";
            // 
            // lblNombrePotions
            // 
            this.lblNombrePotions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombrePotions.Location = new System.Drawing.Point(132, 74);
            this.lblNombrePotions.Name = "lblNombrePotions";
            this.lblNombrePotions.Size = new System.Drawing.Size(59, 23);
            this.lblNombrePotions.TabIndex = 3;
            this.lblNombrePotions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNombrePokeball
            // 
            this.lblNombrePokeball.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombrePokeball.Location = new System.Drawing.Point(132, 26);
            this.lblNombrePokeball.Name = "lblNombrePokeball";
            this.lblNombrePokeball.Size = new System.Drawing.Size(62, 23);
            this.lblNombrePokeball.TabIndex = 2;
            this.lblNombrePokeball.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPotion
            // 
            this.lblPotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPotion.Location = new System.Drawing.Point(6, 75);
            this.lblPotion.Name = "lblPotion";
            this.lblPotion.Size = new System.Drawing.Size(100, 23);
            this.lblPotion.TabIndex = 1;
            this.lblPotion.Text = "Potion: ";
            // 
            // lblPokeball
            // 
            this.lblPokeball.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPokeball.Location = new System.Drawing.Point(6, 29);
            this.lblPokeball.Name = "lblPokeball";
            this.lblPokeball.Size = new System.Drawing.Size(100, 23);
            this.lblPokeball.TabIndex = 0;
            this.lblPokeball.Text = "Pokeball: ";
            // 
            // grbBDescription
            // 
            this.grbBDescription.Controls.Add(this.lblDescriptionItem);
            this.grbBDescription.Location = new System.Drawing.Point(373, 200);
            this.grbBDescription.Name = "grbBDescription";
            this.grbBDescription.Size = new System.Drawing.Size(200, 147);
            this.grbBDescription.TabIndex = 12;
            this.grbBDescription.TabStop = false;
            this.grbBDescription.Text = "Description";
            // 
            // btnAcheter
            // 
            this.btnAcheter.Location = new System.Drawing.Point(462, 411);
            this.btnAcheter.Name = "btnAcheter";
            this.btnAcheter.Size = new System.Drawing.Size(111, 35);
            this.btnAcheter.TabIndex = 3;
            this.btnAcheter.Text = "Acheter";
            this.btnAcheter.UseVisualStyleBackColor = true;
            this.btnAcheter.Click += new System.EventHandler(this.btnAcheter_Click);
            // 
            // btnVendre
            // 
            this.btnVendre.Location = new System.Drawing.Point(334, 411);
            this.btnVendre.Name = "btnVendre";
            this.btnVendre.Size = new System.Drawing.Size(111, 35);
            this.btnVendre.TabIndex = 2;
            this.btnVendre.Text = "Vendre";
            this.btnVendre.UseVisualStyleBackColor = true;
            this.btnVendre.Click += new System.EventHandler(this.btnVendre_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(12, 411);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(100, 35);
            this.lblTotal.TabIndex = 15;
            this.lblTotal.Text = "Cout total : ";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMontantTotal
            // 
            this.lblMontantTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontantTotal.Location = new System.Drawing.Point(119, 411);
            this.lblMontantTotal.Name = "lblMontantTotal";
            this.lblMontantTotal.Size = new System.Drawing.Size(100, 35);
            this.lblMontantTotal.TabIndex = 16;
            this.lblMontantTotal.Text = "0";
            this.lblMontantTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMontantTotal.TextChanged += new System.EventHandler(this.lblMontantTotal_TextChanged);
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(15, 362);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(549, 42);
            this.lblMessage.TabIndex = 17;
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmAchatPekemart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 458);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblMontantTotal);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.btnVendre);
            this.Controls.Add(this.btnAcheter);
            this.Controls.Add(this.grbBDescription);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblPrixPokeball);
            this.Controls.Add(this.lblmutliply);
            this.Controls.Add(this.lblPrixPotion);
            this.Controls.Add(this.nUDPotion);
            this.Controls.Add(this.nUDPokeball);
            this.Controls.Add(this.pctBDreamPotion);
            this.Controls.Add(this.pctBPekeball);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmAchatPekemart";
            this.Text = "Centre d\'achat du Pekemart";
            this.Load += new System.EventHandler(this.FrmAchatPekemart_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctBPekeball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctBDreamPotion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDPokeball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDPotion)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.grbBDescription.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pctBPekeball;
        private System.Windows.Forms.PictureBox pctBDreamPotion;
        private System.Windows.Forms.Label lblDescriptionItem;
        private System.Windows.Forms.NumericUpDown nUDPokeball;
        private System.Windows.Forms.NumericUpDown nUDPotion;
        private System.Windows.Forms.Label lblPrixPotion;
        private System.Windows.Forms.Label lblmutliply;
        private System.Windows.Forms.Label lblPrixPokeball;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblNombrePotions;
        private System.Windows.Forms.Label lblNombrePokeball;
        private System.Windows.Forms.Label lblPotion;
        private System.Windows.Forms.Label lblPokeball;
        private System.Windows.Forms.GroupBox grbBDescription;
        private System.Windows.Forms.Button btnAcheter;
        private System.Windows.Forms.Button btnVendre;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblMontantTotal;
        private System.Windows.Forms.Label lblMontantArgentJoueur;
        private System.Windows.Forms.Label lblArgentJoueur;
        private System.Windows.Forms.Label lblMessage;
    }
}