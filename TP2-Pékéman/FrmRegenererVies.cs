﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TP2_Pékéman.Modele;

namespace TP2_Pékéman
{
    public partial class FrmRegenererVies : Form
    {
        private string pekeManSelectionne;
        public FrmRegenererVies()
        {
            InitializeComponent();
            InitializeComboBoxPokemons();
        }

        private void InitializeComboBoxPokemons()
        {
            foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
            {
                    this.cBPekemans.Items.Add(pekeman.Nom);
            }
            cBPekemans.SelectedIndex = 0;
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cBPekemans_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cBPekemans.SelectedItem.ToString())
            {
                case "CHARMANDER":
                    pctBPekeman.Image = Properties.Resources.charmanderFront;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;
                    
                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("CHARMANDER"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "SQUIRTLE":
                    pctBPekeman.Image = Properties.Resources.squir;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("SQUIRTLE"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "CHIKORITA":
                    pctBPekeman.Image = Properties.Resources.chik;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("CHIKORITA"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "BULBASAUR":
                    pctBPekeman.Image = Properties.Resources.bulb;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("BULBASAUR"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }

                    break;
                case "PIDGEY":
                    pctBPekeman.Image = Properties.Resources.pid;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("PIDGEY"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "JOLTEON":
                    pctBPekeman.Image = Properties.Resources.jolteon;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("JOLTEON"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "RATTATA":
                    pctBPekeman.Image = Properties.Resources.rattata_front;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("RATTATA"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "HO-OH":
                    pctBPekeman.Image = Properties.Resources.hooh;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("HO-OH"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "MAGIKARP":
                    pctBPekeman.Image = Properties.Resources.magikarp;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("MAGIKARP"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "SHIBE":
                    pctBPekeman.Image = Properties.Resources.shibe;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("SHIBE"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
                case "PIKACHU":
                    pctBPekeman.Image = Properties.Resources.pika;
                    pctBPekeman.SizeMode = PictureBoxSizeMode.StretchImage;

                    foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
                    {
                        if (pekeman.Nom.Equals("PIKACHU"))
                        {
                            pekeManSelectionne = pekeman.Nom;
                            afficherInfosPekeman(pekeman);
                        }
                    }
                    break;
            }
        }

        private void afficherInfosPekeman(Pekeman pekeman)
        {
            lblNombreVies.Text = (pekeman.NbVies).ToString();
            lblNombreAttaques.Text = (pekeman.ListeAttaques.Count).ToString();
        }

        private void btnRegenererVies_Click(object sender, EventArgs e)
        {
            foreach (Pekeman pekeman in Noyau.LeJoueur.Equipe)
            {
                if (pekeman.Nom.Equals(pekeManSelectionne))
                {
                        pekeman.NbVies = pekeman.NbViesMax;

                    for (int i = 0; i < pekeman.ListeAttaques.Count; i++)
                    {
                        pekeman.ListeAttaques[i].ActualPowerPoints = pekeman.ListeAttaques[i].MaxPowerPoints;
                    }

                    lblMessage.Text = "La vie et les attaques du Pekeman " + pekeManSelectionne + 
                        " ont ete regeneree avec succes!";
                }
            }
        }

    }
}
