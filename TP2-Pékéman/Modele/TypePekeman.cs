﻿namespace TP2_Pékéman.Modele
{
    /// <summary>
    ///     Enum du type Pekeman
    /// </summary>
    public enum TypePekeman : int
    {
        FEU,
        EAU,
        PLANTE,
        ELECTRICITE,
        NORMAL
    }
}