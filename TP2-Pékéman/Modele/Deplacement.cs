﻿namespace TP2_Pékéman.Modele
{
    /// <summary>
    ///     Enum du deplacement
    /// </summary>
    public enum Deplacement
    {
        HAUT,
        BAS,
        DROITE,
        GAUCHE
    }
}