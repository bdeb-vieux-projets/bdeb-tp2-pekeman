﻿#region

using System.Drawing;

#endregion

namespace TP2_Pékéman.Modele
{
    internal class Villageois
    {
        private readonly Image _image;

        /// <summary>
        ///     Constructeur de la classe Villageois
        /// </summary>
        public Villageois(Image image)
        {
            _image = image;
        }

        public Image image
        {
            get { return _image; }
        }
    }
}