﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP2_Pékéman.Modele
{
    public class Attaque
    {
        private readonly String _nomAttaque;
        private readonly int _maxPowerPoints;
        private int _actualPowerPoints;
        private int _basePower;
        public Attaque(string nomAttaque, int actualPowerPoints,  int maxPowerPoints, int basePower)
        {
            _nomAttaque = nomAttaque;
            ActualPowerPoints = actualPowerPoints;
            BasePower = basePower;
            _maxPowerPoints = maxPowerPoints;
        }

        public string NomAttaque
        {
            get { return _nomAttaque; }
        }

       

        public int ActualPowerPoints
        {
            get { return _actualPowerPoints; }
            set { _actualPowerPoints = value; }
        }

        public int BasePower
        {
            get { return _basePower; }
            set { _basePower = value; }
        }

        public int MaxPowerPoints
        {
            get { return _maxPowerPoints; }
        }
    }
}
