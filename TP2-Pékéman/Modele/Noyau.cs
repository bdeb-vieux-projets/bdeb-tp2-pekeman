﻿using System;
using System.Collections.Generic;

namespace TP2_Pékéman.Modele
{
    public class Noyau
    {
        public const int VIE_POTION = 10;
        public const String DEFAULT_NAME = "Gold";

        public static Joueur LeJoueur;
        public static Boolean CombatEnCours = false;
        public static Boolean EstDemarre = false;
        

        private List<Attaque> _listAttaquesPokemons = new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            };

        public static readonly List<Pekeman> ListePekemanSauvage = new List<Pekeman>()
        {
            new Pekeman("CHARMANDER", Properties.Resources.charmanderFront,Properties.Resources.charmanderBack, TypePekeman.FEU, 5, 23, 45, 65,
                "18.7 lbs.", "2'00\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 52, 43, "A venir"),
            new Pekeman("SQUIRTLE", Properties.Resources.squir,Properties.Resources.squirtleBack, TypePekeman.EAU, 5, 24, 44, 43, "19.8 lbs.",
                "1'08\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 48, 65, "A venir"),
            new Pekeman("CHIKORITA", Properties.Resources.chik,Properties.Resources.chikoritaBack, TypePekeman.PLANTE, 5, 24, 45, 45, "14.1 lbs.",
                "2'11\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 49, 65, "A venir"),
            new Pekeman("BULBASAUR", Properties.Resources.bulb,Properties.Resources.bulbasaurBack, TypePekeman.PLANTE, 5, 24, 45, 45, "15.2 lbs.",
                "2'04\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 49, 49, "A venir"),
            new Pekeman("PIDGEY", Properties.Resources.pid,Properties.Resources.pidgeyBack, TypePekeman.NORMAL, 5, 23, 255, 56, "4.0 lbs.",
                "1'00\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 45, 40, "A venir"),
            new Pekeman("JOLTEON", Properties.Resources.jolteon, Properties.Resources.jolteonBack, TypePekeman.ELECTRICITE, 5, 26, 45, 22, "54.0 lbs.",
                "2'07\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 16, 15, "A venir"),
            new Pekeman("RATTATA", Properties.Resources.rattata_front,Properties.Resources.rattataBack, TypePekeman.NORMAL, 5, 22, 255, 16,
                "7.7 lbs.", "1'00\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 15, 13, "A venir"),
            new Pekeman("HO-OH", Properties.Resources.hooh,Properties.Resources.hoohBack, TypePekeman.FEU, 5, 30, 3, 18, "438.7 lbs.", "12'06\"",
                new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 22, 18, "A venir"),
            new Pekeman("MAGIKARP", Properties.Resources.magikarp,Properties.Resources.magikarp, TypePekeman.EAU, 5, 21,
                255, 17, "22.0 lbs.", "2'11\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 10, 15, "A venir"),
               new Pekeman("SHIBE", Properties.Resources.shibe, Properties.Resources.shibe, TypePekeman.NORMAL, 5, 23, 190,
                18, "13.2 lbs.", "1'04\"", new List<Attaque>
            {
                new Attaque("TACKLE", 35, 35, 50),
                new Attaque("SPLASH", 40, 40, 0)
            }, 15, 12,
                "Wow. Such pekeman much wild so catch.")
        };

        public Noyau()
        {
            EstDemarre = true;
            List<Pekeman> equipe = InitialiserPekemansJoueur();
            LeJoueur = new Joueur(DEFAULT_NAME, equipe, 10, 10);
        }

        private List<Pekeman> InitialiserPekemansJoueur()
        {
            List<Pekeman> pekemansJoueur = new List<Pekeman>(6);
            
            pekemansJoueur.Add( new Pekeman("PIKACHU", Properties.Resources.pika, Properties.Resources.pikaback, TypePekeman.ELECTRICITE, 5, 23, 190,
                18, "13.2 lbs.", "1'04\"", _listAttaquesPokemons, 15, 12,
                "Vit dans les forêts l'écart du monde. Il stocke l'électricité dans ses joues pour électrocuter un ennemi s'il est attaqué."));
            return pekemansJoueur;
        }
    }
}
