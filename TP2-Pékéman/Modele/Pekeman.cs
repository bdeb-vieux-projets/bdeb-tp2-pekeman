﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;

#endregion

namespace TP2_Pékéman.Modele
{
    public class Pekeman
    {
        private readonly Image _image;
        private readonly Image _imageBack;
        private readonly String _description;
        private readonly int _catchRate;
        private readonly int _nbViesMax;
        private readonly String _poids;
        private readonly String _taille;
        private readonly String _nom;
        private readonly int _attackPower;
        private readonly int _defensePower;
        private readonly int _vitesse;
        private int _exp;
        private List<Attaque> _listeAttaques;
        private int _level;

        private int _nbVies;
        private readonly TypePekeman _type;

        /// <summary>
        ///     Constructeur de la classe Pekeman
        /// </summary>
        public Pekeman(String nom, Image image,Image imageBack, TypePekeman type, int level, int nbVies, int catchRate, int vitesse,
            String poids, String taille, List<Attaque> listeAttaques, int attackPower, int defensePower,
            String description)
        {
            _image = image;
            _imageBack = imageBack;
            _nom = nom;
            _poids = poids;
            _taille = taille;
            _nbVies = nbVies;
            _nbViesMax = nbVies;
            _type = type;
            _level = level;
            _attackPower = attackPower;
            _defensePower = defensePower;
            _description = description;
            _image = Image;
            _catchRate = catchRate;
            _vitesse = vitesse;
            _exp = 0;
            ListeAttaques = listeAttaques;
        }

        public String Nom
        {
            get { return _nom; }
        }

        public int NbViesMax
        {
            get { return _nbViesMax; }
        }

        public int CatchRate
        {
            get { return _catchRate; }
        }
        public int Exp
        {
            get { return _exp; }
            set { _exp = value; }
        }
        public int Vitesse
        {
            get { return _vitesse; }
        }

        public Image Image
        {
            get { return _image; }
        }
        public Image ImageBack
        {
            get { return _imageBack; }
        }
        public int Level
        {
            get { return _level; }
            set
            {
                if (value == 1)
                {
                    _level = value;
                }
            }
        }

        // la valeur d'attaque du Pekeman

        public int AttackPower
        {
            get { return _attackPower; }
        }

        // la valeur de defense du Pekeman

        public int DefensePower
        {
            get { return _defensePower; }
        }

        public string Description
        {
            get { return _description; }
        }

        public int NbVies
        {
            get { return _nbVies; }
            set { _nbVies = value; }
        }

        public List<Attaque> ListeAttaques
        {
            get { return _listeAttaques; }
            set { _listeAttaques = value; }
        }

        public int Type
        {
            get { return (int) _type; }
        }

        public string Poids
        {
            get { return _poids; }
        }

        public string Taille
        {
            get { return _taille; }
        }
    }
}