﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TP2_Pékéman.Vue;

namespace TP2_Pékéman.Modele
{
    public class SauvegardeDuJoueur
    {
        public String NomJoueur;
        public int PosX;
        public int PosY;
        public int Argent;
        public int NbPotions;
        public int NbPokeballs;
        // public Pekeman[] Equipe;
    }
    
    class SauvegardeRestauration
    {
        /// <summary>
        /// Procédure qui permet de sauvegarder l'état d'une partie en XML
        /// </summary>
        /// <param name="leNoyau">L'objet Noyau de la partie en cours</param>
        /// <param name="fs">Un flux d'écriture</param>
        public void Sauvegarde(Noyau leNoyau, FileStream fs)
        {
            SauvegardeDuJoueur save = new SauvegardeDuJoueur();
            
            save.NomJoueur = Noyau.LeJoueur.nomJoueur;
            save.PosX = Noyau.LeJoueur.posX;
            save.PosY = Noyau.LeJoueur.posY;
            save.Argent = Noyau.LeJoueur.Argent;
            save.NbPotions = Noyau.LeJoueur.NbPotions;
            save.NbPokeballs = Noyau.LeJoueur.NbPokeballs;
            
            // TODO sauvegarder le Pékéball et l'équipe
            //save.Equipe = joueurASauvegarder.Equipe;

            XmlSerializer x = new XmlSerializer(save.GetType());
            x.Serialize(fs, save);

            fs.Close(); // Ferme le flux d'écriture du fichier de sauvegarde reçu en paramètre
        }

        /// <summary>
        /// Procédure qui permet de restaurer le Noyau du jeu 
        /// à partir d'un fichier de sauvegarde
        /// </summary>
        /// <param name="leNoyau">Le noyau du jeu à modifier</param>
        /// <param name="fs">Un flux de lecture du fichier de restauration</param>
        public void Restauration(Noyau leNoyau, FileStream fs)
        {
            SauvegardeDuJoueur load = new SauvegardeDuJoueur();
            XmlSerializer x = new XmlSerializer(load.GetType());

            load = x.Deserialize(fs) as SauvegardeDuJoueur;

            Noyau.LeJoueur.nomJoueur = load.NomJoueur;            
            Noyau.LeJoueur.posX = load.PosX;          
            Noyau.LeJoueur.posY = load.PosY;
            Noyau.LeJoueur.Argent = load.Argent;
            Noyau.LeJoueur.NbPotions = load.NbPotions;
            Noyau.LeJoueur.NbPokeballs = load.NbPokeballs;

            // TODO restaurer le Pékéball et l'équipe

            fs.Close(); // Ferme le flux de lecture reçu en paramètre
        }
    }
}
