﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using TP2_Pékéman.Properties;

#endregion

namespace TP2_Pékéman.Modele
{
    public class Joueur
    {
        public static Image image;
        public int PremierPekeman;//indique quel est le premier pekeman si le premier par defaut faint
       // public Pekeman compagnon;
        public String nomJoueur;
        public List<Pekeman> pekedexList;
        private List<Pekeman> _pekemansDuJoueur;
        public List<Pekeman> Equipe = new List<Pekeman>(6); 
        // position x du Joueur
        public int posX;
        private int _argent;
        // position y du Joueur
        public int posY;
        private int _nbPotions;
        private int _nbPokeballs;
        public static Boolean[] TypeDePekemanAttrape = new bool[5];

        /// <summary>
        ///     Constructeur de la classe Joueur et qui a comme paramettre le Nom du Joueur
        /// </summary>
        public Joueur(String nomJoueur, List<Pekeman> equipe, int nbPotions, int nbPokeballs)
        {
            PremierPekeman = 0;
            this.nomJoueur = nomJoueur;
            PekemansDuJoueur = new List<Pekeman>();
            image = Resources.bas1;
            posX = 11;
            posY = 9;
            pekedexList = new List<Pekeman>();
            Equipe = equipe;
            NbPotions = nbPotions;
            NbPokeballs = nbPokeballs;
            _argent = 1000;
            pekedexList.Add(equipe[PremierPekeman]);

            for (int i = 0; i < TypeDePekemanAttrape.Length; i++)
            {
                TypeDePekemanAttrape[i] = false;
            }
        }

        public List<Pekeman> PekemansDuJoueur
        {
            get { return _pekemansDuJoueur; }
            set { _pekemansDuJoueur = value; }
        }

        //public List<Pekeman> Equipe
        //{
        //    get { return _equipe; }
        //    set { _equipe = value; }
        //}

        public int NbPokeballs
        {
            get { return _nbPokeballs; }
            set { _nbPokeballs = value; }
        }

        public int NbPotions
        {
            get { return _nbPotions; }
            set { _nbPotions = value; }
        }
        public int Argent
        {
            get { return _argent; }
            set { _argent = value; }
        }
        /// <summary>
        /// Methode qui ajoute le nouveau pokemon a la liste ou l'equipe dependamment des places libres
        /// </summary>
        public void AjouterNouveauPekeman(Pekeman pekeman)
        {
            pekeman.NbVies = pekeman.NbViesMax;
            TypeDePekemanAttrape[(int)pekeman.Type] = true;

            if (Equipe.Count < 6)
            {
                Equipe.Add(pekeman);
            }
            else
            {
                PekemansDuJoueur.Add(pekeman);
            }
        }

        public bool FinDuJeu()
        {
            if (TypeDePekemanAttrape[(int)TypePekeman.EAU].Equals(true) &&
                TypeDePekemanAttrape[(int)TypePekeman.ELECTRICITE].Equals(true) &&
                TypeDePekemanAttrape[(int)TypePekeman.FEU].Equals(true) &&
                TypeDePekemanAttrape[(int)TypePekeman.NORMAL].Equals(true) &&
                TypeDePekemanAttrape[(int)TypePekeman.PLANTE].Equals(true))
            {
                return true;
            }
            
            return false;
        }
    }
}