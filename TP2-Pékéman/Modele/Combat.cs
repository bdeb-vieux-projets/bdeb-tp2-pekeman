﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TP2_Pékéman.Properties;
using TP2_Pékéman.Vue;

namespace TP2_Pékéman.Modele
{
    public class Combat
    {
        private static readonly Random Random = new Random();
        private readonly Pekeman _ennemi;
        private int _indicePekemanChoisi;
        private Boolean _plusDePokeBalls;
        private int _tentavivesFuites;
        private Boolean _tourHero;
        private String _nomPekemanChoisi;
        public Combat()
        {
            Noyau.CombatEnCours = true;
            int indicePekemanEnnemi;
            indicePekemanEnnemi = Random.Next(Noyau.ListePekemanSauvage.Count);
            _ennemi = Noyau.ListePekemanSauvage[indicePekemanEnnemi];
            
        }


        public bool Fuir { get; set; }

        public bool EstEchange { get; set; }

        public Boolean PlusDePokeballs
        {
            get { return _plusDePokeBalls; }
        }

        public Pekeman Ennemi
        {
            get { return _ennemi; }
        }

        public int IndicePekemanChoisi
        {
            get { return _indicePekemanChoisi; }
            set { _indicePekemanChoisi = value; }
        }

        public string NomPekemanChoisi { get; set; }

        /// <summary>
        ///     Methode qui se charge d'attaquer un pokemon dependamment de l'attaque choisit
        /// </summary>
        /// <param name="attaquant"></param>
        /// <param name="defenseur"></param>
        /// <param name="attaque"></param>
        public void CombattrePokemon(Pekeman attaquant, Pekeman defenseur, Attaque attaque)
        {
            var c = InitialiserCoupCritique();
            var r = GenerateRandomNumber();
            const int m = 1;
            var b = attaque.BasePower;
            var a = attaquant.AttackPower;
            var d = attaquant.DefensePower;
            int damage = (int) ((((attaquant.Level*0.4*c) + 2)*a*b/50/d) + 2)*m*r/255;
            defenseur.NbVies -= damage;
            VerifierViePekeman();
        }

        /// <summary>
        ///     Methode qui trouve le chances d'un coup critique
        /// </summary>
        private int InitialiserCoupCritique()
        {
            int coupCritique = 1;

            int chance = Random.Next(1, 16);
            if (chance == 1)
            {
                coupCritique = 2;
            }
            return coupCritique;
        }

        private int GenerateRandomNumber()
        {
            return Random.Next(217, 256);
        }

        /// <summary>
        ///     Methode qui se charge d'attraper un pokemon
        /// </summary>
        /// <returns></returns>
        public Boolean AttraperPekeman()
        {
            Boolean estAttrape = false;
            if (Noyau.LeJoueur.NbPokeballs > 0)
            {
                _plusDePokeBalls = true;
                double x;
                const int d = 0;
                double a = Ennemi.NbVies*2;
                double b = Ennemi.NbViesMax*3;
                var c = Ennemi.CatchRate;

                if (a == 0)
                {
                    a = 1;
                }
                if (b > 255)
                {
                    a = Math.Floor(Math.Floor(a/2)/2);
                    b = Math.Floor(Math.Floor(b/2)/2);
                }
                x = Math.Floor((b - a)*c/b + d);
                int randomNumber;
                if (x > 255)
                {
                    estAttrape = true;
                    Noyau.LeJoueur.pekedexList.Add(Ennemi);
                    Noyau.LeJoueur.AjouterNouveauPekeman(_ennemi);

                    if (VerifierQuantiteList())
                        AfficherGagnant();
                    else
                        Noyau.CombatEnCours = false;
                }
                else
                {
                    randomNumber = Random.Next(0, 256);
                    if (randomNumber > x)
                    {
                        estAttrape = true;
                        Noyau.LeJoueur.pekedexList.Add(Ennemi);
                        Noyau.LeJoueur.AjouterNouveauPekeman(_ennemi);

                        if (VerifierQuantiteList())
                            AfficherGagnant();
                        else
                            Noyau.CombatEnCours = false;
                    }
                }
                --Noyau.LeJoueur.NbPokeballs;
            }
            else
                _plusDePokeBalls = false;
            return estAttrape;
        }

        /// <summary>
        private void AfficherGagnant()
        {
            MessageBox.Show(Resources.Msg_felicitation, Resources.msg_partie_gagnee);
            DemanderVoirPekeDex();
            Noyau.CombatEnCours = false;
            Application.Exit();
        }

        /// <summary>
        ///     methode qui demande d'affiche le pekedex
        /// </summary>
        private void DemanderVoirPekeDex()
        {
            DialogResult result = MessageBox.Show(Resources.msg_pekedex_avant_quitter, "Voir pékédex", buttons: MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                var frmPokedex = new FrmPokedex();
                frmPokedex.ChargerPokedex();
                frmPokedex.ShowDialog();
            }
        }

        /// <summary>
        ///     methode qui verifie le nb de types de Pekeman
        /// </summary>
        private Boolean VerifierQuantiteList()
        {
            int nb = CompterPekemansUniques();
            if (nb == 10)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     methode qui fait une liste de pekemans non repetés
        /// </summary>
        private int CompterPekemansUniques()
        {
            var pekePasRepetes = new List<Pekeman>();

            foreach (Pekeman pek in Noyau.LeJoueur.pekedexList.Distinct())
            {
                pekePasRepetes.Add(pek);
            }
            return pekePasRepetes.Count;
        }

        /// <summary>
        ///     methode qui trouve la chance de fuir le combat
        /// </summary>
        public void ChanceFuirAttaque()
        {
            Fuir = false;
            int a = Noyau.LeJoueur.Equipe[Noyau.LeJoueur.PremierPekeman].Vitesse;
            int b = (_ennemi.Vitesse/4)%256;
            int c = _tentavivesFuites;

            int f = a*32/b + 30*c;
            if (f > 255)
            {
                //Pekeman fuit le combat
                Noyau.CombatEnCours = false;
                Fuir = true;
            }
            else
            {
                int randomNumber = Random.Next(0, 256);
                if (randomNumber > f)
                {
                    //Pekeman fuit le combat
                    Noyau.CombatEnCours = false;
                    Fuir = true;
                }
            }
        }

        private void VerifierViePekeman()
        {
            if (Noyau.LeJoueur.Equipe[_indicePekemanChoisi].NbVies <= 0)
            {
                //AfficherMessageDefaite();
            }
            else if (_ennemi.NbVies <= 0)
            {
                Noyau.CombatEnCours = false;
            }
        }

        /// <summary>
        ///     methode qui soigne un pekeman
        /// </summary>
        /// <returns></returns>
        public Boolean SoignerPekeman()
        {
            Boolean estSoigne;
            if (estSoigne = (Noyau.LeJoueur.NbPotions > 0))
            {
                Noyau.LeJoueur.Equipe[IndicePekemanChoisi].NbVies += Noyau.VIE_POTION;
                --Noyau.LeJoueur.NbPotions;
            }
            return estSoigne;
        }
        

        /// <summary>
        ///     Methode qui remet a la vie un pokemon sauvage s'il meurt
        /// </summary>
        public void RemettreEnVieEnnemi()
        {
            Ennemi.NbVies = Ennemi.NbViesMax;
        }

        /// <summary>
        ///     Methode qui se charge d'echanger les pokemons au cours d'un combat
        /// </summary>
        /// <param name="indiceNouveauPokemon"></param>
        public void EchangerPokemon(int indiceNouveauPokemon)
        {
            if (Noyau.LeJoueur.Equipe[indiceNouveauPokemon].NbVies > 0)
            {
                EstEchange = true;
                IndicePekemanChoisi = indiceNouveauPokemon;
            }
            else
            {
                MessageBox.Show(Resources.msg_pokemonNonUtilisable, "Pékéman", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                EstEchange = false;
            }
        }
    }
}