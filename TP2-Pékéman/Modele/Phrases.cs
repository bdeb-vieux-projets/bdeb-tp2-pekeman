﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TP2_Pékéman.Modele
{
    public enum IDPHRASES
    {
        [Description("BUY SOMETHING ALREADY")]
        ZELDA,
        [Description("Thank you Mario! But our Princess is in another castle!")]
        MARIO,
        [Description("Oh, hi. So, how are you holding up? BECAUSE I'M A POTATO!")]
        GLADOS
        
    }

    public class Phrases
    {
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }
}
