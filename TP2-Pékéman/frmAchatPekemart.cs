﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TP2_Pékéman.Modele;

namespace TP2_Pékéman
{
    public partial class FrmAchatPekemart : Form
    {
        private int nbPekeball = 0;
        private int nbPotions = 0;
        private int montantTotal;
        private const int VALEUR_POKEBALL = 100;
        private const int VALEUR_POTION = 150;

        public FrmAchatPekemart()
        {
            InitializeComponent();
            AfficherInfosDeBase();
        }

        private void AfficherInfosDeBase()
        {
            this.lblNombrePokeball.Text = Noyau.LeJoueur.NbPokeballs.ToString();
            this.lblNombrePotions.Text = Noyau.LeJoueur.NbPotions.ToString();
            this.lblMontantArgentJoueur.Text = Noyau.LeJoueur.Argent.ToString();
        }

        private void pctBPekeball_MouseHover(object sender, EventArgs e)
        {
            this.lblDescriptionItem.Visible = true;
            this.lblDescriptionItem.Text = "Les Pokéballs ou Balls sont des objets essentiels à la tâche d'un dresseur souhaitant compléter son Pokédex, lui permettant" +
                                           " de capturer les Pokémon sauvages qu'il rencontre.";
        }

        private void pctBPekeball_MouseLeave(object sender, EventArgs e)
        {
            this.lblDescriptionItem.Visible = false;
        }

        private void pctBDreamPotion_MouseHover(object sender, EventArgs e)
        {
            this.lblDescriptionItem.Visible = true;
            this.lblDescriptionItem.Text = "La Potion est un objet apparu dans la première génération. Cet objet permet de rendre jusqu'à 20 PV à un Pokémon" +
                                           " (à utiliser en combat ou bien en dehors).";
        }

        private void pctBDreamPotion_MouseLeave(object sender, EventArgs e)
        {
            this.lblDescriptionItem.Visible = false;
        }

        private void lblMontantTotal_TextChanged(object sender, EventArgs e)
        {
            this.lblMontantTotal.Text = montantTotal.ToString();
        }

        private void nUDPokeball_ValueChanged(object sender, EventArgs e)
        {
            nbPekeball = Convert.ToInt32(this.nUDPokeball.Value);
            Console.WriteLine(nbPekeball);
            MettreAJourMontantTotal();
        }

        private void nUDPotion_ValueChanged(object sender, EventArgs e)
        {
            nbPotions = Convert.ToInt32(this.nUDPotion.Value);
            MettreAJourMontantTotal();
        }

        private void MettreAJourMontantTotal()
        {
            montantTotal = (nbPotions*VALEUR_POTION) +
                               (nbPekeball*VALEUR_POKEBALL);
            this.lblMontantTotal.Text = montantTotal.ToString();
            
        }

        private void btnVendre_Click(object sender, EventArgs e)
        {
            if (nbPotions > Noyau.LeJoueur.NbPotions || nbPekeball > Noyau.LeJoueur.NbPokeballs)
            {
                lblMessage.Text = "Vous ne possedez pas assez d'items pour en vendre autant!";
            }
            else
            {
                mettreAJourVenteJoueur();
            }
        }

        private void mettreAJourVenteJoueur()
        {
            Noyau.LeJoueur.NbPokeballs -= nbPekeball;
            Noyau.LeJoueur.NbPotions -= nbPotions;
            Noyau.LeJoueur.Argent += montantTotal;
            lblMessage.Text = "Vente completee avec succes !";
            //this.Hide();
        }

        private void btnAcheter_Click(object sender, EventArgs e)
        {
            if (montantTotal > Noyau.LeJoueur.Argent)
            {
                lblMessage.Text = "Vous ne possedez pas assez d'argent pour poursuivre cet achat!";
            }
            else
            {
                mettreAJourAchatJoueur();
            }
        }

        private void mettreAJourAchatJoueur()
        {
            Noyau.LeJoueur.NbPokeballs += nbPekeball;
            Noyau.LeJoueur.NbPotions += nbPotions;
            Noyau.LeJoueur.Argent -= montantTotal;
            lblMessage.Text = "Achat complete avec succes !";
            this.Hide();
        }

        private void FrmAchatPekemart_Load(object sender, EventArgs e)
        {
            lblDescriptionItem.Visible = true;
            lblDescriptionItem.Text = "Placer votre curseur au dessus de l'item pour afficher sa description";
        }
    }
}
