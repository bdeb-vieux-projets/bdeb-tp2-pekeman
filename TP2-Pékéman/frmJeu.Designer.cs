﻿namespace TP2_Pékéman
{
    partial class FrmJeu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmJeu));
            this.tmrDemarrer = new System.Windows.Forms.Timer(this.components);
            this.btnPekedex = new System.Windows.Forms.Button();
            this.lblNomJoueur = new System.Windows.Forms.Label();
            this.lblNomText = new System.Windows.Forms.Label();
            this.tmrMenu = new System.Windows.Forms.Timer(this.components);
            this.btnDown = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.tmrNomJoueur = new System.Windows.Forms.Timer(this.components);
            this.ctlNomJoueur = new TP2_Pékéman.Controle_Utilisateur.CtlNomJoueur();
            this.logoNentindo1 = new TP2_Pékéman.Controle_Utilisateur.LogoNentindo();
            this.ctlAccueil = new TP2_Pékéman.Controle_Utilisateur.CtlAccueil();
            this.map = new TP2_Pékéman.Map();
            this.ctlMenu = new TP2_Pékéman.Controle_Utilisateur.CtlMenu();
            this.SuspendLayout();
            // 
            // tmrDemarrer
            // 
            this.tmrDemarrer.Tick += new System.EventHandler(this.tmrDemarrer_Tick);
            // 
            // btnPekedex
            // 
            this.btnPekedex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPekedex.Location = new System.Drawing.Point(396, 634);
            this.btnPekedex.Name = "btnPekedex";
            this.btnPekedex.Size = new System.Drawing.Size(95, 42);
            this.btnPekedex.Text = "PEKEDEX";
            this.btnPekedex.UseVisualStyleBackColor = true;
            this.btnPekedex.Click += new System.EventHandler(this.btnPekedex_Click);
            // 
            // lblNomJoueur
            // 
            this.lblNomJoueur.AutoSize = true;
            this.lblNomJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomJoueur.Location = new System.Drawing.Point(36, 644);
            this.lblNomJoueur.Name = "lblNomJoueur";
            this.lblNomJoueur.Size = new System.Drawing.Size(140, 20);
            this.lblNomJoueur.TabIndex = 10;
            this.lblNomJoueur.Text = "Nom du _joueur:";
            // 
            // lblNomText
            // 
            this.lblNomText.AutoSize = true;
            this.lblNomText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomText.Location = new System.Drawing.Point(193, 644);
            this.lblNomText.Name = "lblNomText";
            this.lblNomText.Size = new System.Drawing.Size(0, 20);
            this.lblNomText.TabIndex = 11;
            // 
            // tmrMenu
            // 
            this.tmrMenu.Tick += new System.EventHandler(this.tmrMenu_Tick);
            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.Color.White;
            this.btnDown.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDown.BackgroundImage")));
            this.btnDown.Location = new System.Drawing.Point(581, 657);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(42, 42);
            this.btnDown.UseVisualStyleBackColor = false;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnRight
            // 
            this.btnRight.BackColor = System.Drawing.Color.White;
            this.btnRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRight.BackgroundImage")));
            this.btnRight.Location = new System.Drawing.Point(622, 634);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(42, 42);
            this.btnRight.UseVisualStyleBackColor = false;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.BackColor = System.Drawing.Color.White;
            this.btnLeft.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLeft.BackgroundImage")));
            this.btnLeft.Location = new System.Drawing.Point(540, 634);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(42, 42);
            this.btnLeft.UseVisualStyleBackColor = false;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnUp
            // 
            this.btnUp.BackColor = System.Drawing.Color.White;
            this.btnUp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUp.BackgroundImage")));
            this.btnUp.Location = new System.Drawing.Point(581, 611);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(42, 42);
           this.btnUp.UseVisualStyleBackColor = false;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // tmrNomJoueur
            // 
            this.tmrNomJoueur.Tick += new System.EventHandler(this.tmrNomJoueur_Tick);
            // 
            // ctlNomJoueur
            // 
            this.ctlNomJoueur.Location = new System.Drawing.Point(-1, 2);
            this.ctlNomJoueur.Name = "ctlNomJoueur";
            this.ctlNomJoueur.Size = new System.Drawing.Size(700, 700);
            this.ctlNomJoueur.Visible = false;
            // 
            // logoNentindo1
            // 
            this.logoNentindo1.BackColor = System.Drawing.Color.Transparent;
            this.logoNentindo1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("logoNentindo1.BackgroundImage")));
            this.logoNentindo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoNentindo1.Location = new System.Drawing.Point(0, 2);
            this.logoNentindo1.Name = "logoNentindo1";
            this.logoNentindo1.Size = new System.Drawing.Size(700, 700);
            // 
            // ctlAccueil
            // 
            this.ctlAccueil.BackColor = System.Drawing.Color.White;
            this.ctlAccueil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ctlAccueil.Location = new System.Drawing.Point(0, 0);
            this.ctlAccueil.Name = "ctlAccueil";
            this.ctlAccueil.Size = new System.Drawing.Size(700, 700);
            this.ctlAccueil.Visible = false;
            // 
            // map
            // 
           // this.map.Joueur = null;
            this.map.listePekeman = null;
            this.map.Location = new System.Drawing.Point(0, 0);
            this.map.Name = "map";
            this.map.Size = new System.Drawing.Size(700, 700);
            this.map.Visible = false;
            // 
            // ctlMenu
            // 
            this.ctlMenu.BackColor = System.Drawing.Color.Silver;
            this.ctlMenu.Location = new System.Drawing.Point(0, 2);
            this.ctlMenu.Name = "ctlMenu";
            this.ctlMenu.Size = new System.Drawing.Size(700, 569);
            this.ctlMenu.Visible = false;
            // 
            // FrmJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(699, 704);
            this.Controls.Add(this.lblNomText);
            this.Controls.Add(this.lblNomJoueur);
            this.Controls.Add(this.btnPekedex);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.ctlNomJoueur);
            this.Controls.Add(this.logoNentindo1);
            this.Controls.Add(this.ctlAccueil);
            this.Controls.Add(this.map);
            this.Controls.Add(this.ctlMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FrmJeu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pékéman";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmJeu_FormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmJeu_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Map map;
        private Controle_Utilisateur.CtlAccueil ctlAccueil;
        private Controle_Utilisateur.LogoNentindo logoNentindo1;
        private System.Windows.Forms.Timer tmrDemarrer;
        internal System.Windows.Forms.Button btnDown;
        internal System.Windows.Forms.Button btnRight;
        internal System.Windows.Forms.Button btnLeft;
        internal System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnPekedex;
        private System.Windows.Forms.Label lblNomJoueur;
        private System.Windows.Forms.Label lblNomText;
        private System.Windows.Forms.Timer tmrMenu;
        private Controle_Utilisateur.CtlMenu ctlMenu;
        private System.Windows.Forms.Timer tmrNomJoueur;
        public Controle_Utilisateur.CtlNomJoueur ctlNomJoueur;
    }
}

