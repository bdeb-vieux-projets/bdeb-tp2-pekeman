﻿namespace TP2_Pékéman
{
    partial class FrmRegenererVies
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRegenererVies = new System.Windows.Forms.Label();
            this.cBPekemans = new System.Windows.Forms.ComboBox();
            this.pctBPekeman = new System.Windows.Forms.PictureBox();
            this.btnRegenererVies = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.gbInformation = new System.Windows.Forms.GroupBox();
            this.lblNombreAttaques = new System.Windows.Forms.Label();
            this.lblNombreVies = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblViePekeman = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pctBPekeman)).BeginInit();
            this.gbInformation.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblRegenererVies
            // 
            this.lblRegenererVies.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegenererVies.Location = new System.Drawing.Point(26, 9);
            this.lblRegenererVies.Name = "lblRegenererVies";
            this.lblRegenererVies.Size = new System.Drawing.Size(451, 40);
            this.lblRegenererVies.TabIndex = 0;
            this.lblRegenererVies.Text = "Choisissez le Pokeman dont vous voulez regenerer\r\n la vie et les attaques";
            this.lblRegenererVies.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cBPekemans
            // 
            this.cBPekemans.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBPekemans.FormattingEnabled = true;
            this.cBPekemans.Location = new System.Drawing.Point(193, 65);
            this.cBPekemans.Name = "cBPekemans";
            this.cBPekemans.Size = new System.Drawing.Size(146, 21);
            this.cBPekemans.TabIndex = 0;
            this.cBPekemans.SelectedIndexChanged += new System.EventHandler(this.cBPekemans_SelectedIndexChanged);
            // 
            // pctBPekeman
            // 
            this.pctBPekeman.Image = global::TP2_Pékéman.Properties.Resources.pika;
            this.pctBPekeman.Location = new System.Drawing.Point(29, 110);
            this.pctBPekeman.Name = "pctBPekeman";
            this.pctBPekeman.Size = new System.Drawing.Size(247, 182);
            this.pctBPekeman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctBPekeman.TabIndex = 2;
            this.pctBPekeman.TabStop = false;
            // 
            // btnRegenererVies
            // 
            this.btnRegenererVies.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegenererVies.Location = new System.Drawing.Point(193, 345);
            this.btnRegenererVies.Name = "btnRegenererVies";
            this.btnRegenererVies.Size = new System.Drawing.Size(146, 36);
            this.btnRegenererVies.TabIndex = 1;
            this.btnRegenererVies.Text = "Regenerer";
            this.btnRegenererVies.UseVisualStyleBackColor = true;
            this.btnRegenererVies.Click += new System.EventHandler(this.btnRegenererVies_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnuler.Location = new System.Drawing.Point(345, 345);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(146, 36);
            this.btnAnnuler.TabIndex = 3;
            this.btnAnnuler.Text = "Quitter";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(12, 295);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(465, 46);
            this.lblMessage.TabIndex = 5;
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbInformation
            // 
            this.gbInformation.Controls.Add(this.lblNombreAttaques);
            this.gbInformation.Controls.Add(this.lblNombreVies);
            this.gbInformation.Controls.Add(this.label1);
            this.gbInformation.Controls.Add(this.lblViePekeman);
            this.gbInformation.Location = new System.Drawing.Point(301, 110);
            this.gbInformation.Name = "gbInformation";
            this.gbInformation.Size = new System.Drawing.Size(190, 124);
            this.gbInformation.TabIndex = 6;
            this.gbInformation.TabStop = false;
            this.gbInformation.Text = "Informations Pokeman";
            // 
            // lblNombreAttaques
            // 
            this.lblNombreAttaques.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreAttaques.Location = new System.Drawing.Point(130, 85);
            this.lblNombreAttaques.Name = "lblNombreAttaques";
            this.lblNombreAttaques.Size = new System.Drawing.Size(49, 23);
            this.lblNombreAttaques.TabIndex = 3;
            this.lblNombreAttaques.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNombreVies
            // 
            this.lblNombreVies.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreVies.Location = new System.Drawing.Point(130, 25);
            this.lblNombreVies.Name = "lblNombreVies";
            this.lblNombreVies.Size = new System.Drawing.Size(49, 23);
            this.lblNombreVies.TabIndex = 2;
            this.lblNombreVies.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre attaques:";
            // 
            // lblViePekeman
            // 
            this.lblViePekeman.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViePekeman.Location = new System.Drawing.Point(6, 29);
            this.lblViePekeman.Name = "lblViePekeman";
            this.lblViePekeman.Size = new System.Drawing.Size(118, 23);
            this.lblViePekeman.TabIndex = 0;
            this.lblViePekeman.Text = "Nombre de vie: ";
            // 
            // FrmRegenererVies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 393);
            this.Controls.Add(this.gbInformation);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnRegenererVies);
            this.Controls.Add(this.pctBPekeman);
            this.Controls.Add(this.cBPekemans);
            this.Controls.Add(this.lblRegenererVies);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmRegenererVies";
            this.Text = "Bienvenue au Pekecenter";
            ((System.ComponentModel.ISupportInitialize)(this.pctBPekeman)).EndInit();
            this.gbInformation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblRegenererVies;
        private System.Windows.Forms.ComboBox cBPekemans;
        private System.Windows.Forms.PictureBox pctBPekeman;
        private System.Windows.Forms.Button btnRegenererVies;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.GroupBox gbInformation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblViePekeman;
        private System.Windows.Forms.Label lblNombreAttaques;
        private System.Windows.Forms.Label lblNombreVies;
    }
}